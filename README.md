# SQLAnt #


*"A small but hard worker"* | Build Status
--|---
  | [![Build status](https://ci.appveyor.com/api/projects/status/pfvjuvl2pnt79j3b?svg=true)](https://ci.appveyor.com/project/wesleywerner/sqlant) 
![ant-image.png](https://bitbucket.org/repo/aq65Ed/images/3658507457-ant-image.png) | ![system-installer.png](https://bitbucket.org/repo/aq65Ed/images/1370793940-system-installer.png) [Download](https://bitbucket.org/wesleywerner/sqlant/downloads/)

SQLAnt is a light-weight T-SQL query editor. It's goal is to get you up-and-writing those queries, and it keeps you moving.

## Highlighted Features ##

|
--------|--------
![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Build Pipeline Unit Tests** | ![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Transactional Rollbacks**
![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Syntax Highlighting** | ![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Column Selections**
![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Multi-line Editing** | ![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Tabbed editors**
![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Auto Complete** | ![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Code Snippets**
![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Query Auditing** | ![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Connection Colouring**
![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Navigational Tree** | ![application-certificate.png](https://bitbucket.org/repo/aq65Ed/images/1502872152-application-certificate.png) **Small Size**

## Quality Assurance ##

The build pipeline includes a range of unit tests which ensures various aspects of SQLAnt work as intented, including:

* Data Integrity: Ensures the query request processor returns the data you asked for.
* Auditing: Ensures that every query is audited to an archive, and that you can search historical audits with accuracy.
* Transactions: Ensures data changes are rolled back when the query errors.

[See the latest build test suite results here](https://ci.appveyor.com/project/wesleywerner/sqlant/build/tests).

## Cost ##

SQLAnt is both Gratis and Free.

## License ##

    SQLAnt query editor
    Copyright (C) 2016 Wesley Werner
    https://bitbucket.org/wesleywerner/sqlant

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

## What SQLAnt is not ##

* It is not a database management solution. If SQL Management studio is for administering databases, SQLAnt is for writing the queries.