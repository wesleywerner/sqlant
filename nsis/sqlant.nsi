############################################################################################
#      NSIS Installation Script created by NSIS Quick Setup Script Generator v1.09.18
#               Entirely Edited with NullSoft Scriptable Installation System                
#              by Vlasis K. Barkas aka Red Wine red_wine@freemail.gr Sep 2006               
############################################################################################

!define APP_NAME "SQL Ant"
!define COMP_NAME "Wesley Werner"
!define WEB_SITE "https://bitbucket.org/wesleywerner/sqlant/wiki"
#!define VERSION "1.00.00.00"
!define COPYRIGHT "Wesley Werner � 2016"
!define DESCRIPTION "Light-weight T-SQL query editor"
!define LICENSE_TXT "COPYING.txt"
!define INSTALLER_NAME "SQLAnt_${VERSION}_setup.exe"
!define MAIN_APP_EXE "SQLAnt.exe"
!define INSTALL_TYPE "SetShellVarContext current"
!define REG_ROOT "HKCU"
!define REG_APP_PATH "Software\Microsoft\Windows\CurrentVersion\App Paths\${MAIN_APP_EXE}"
!define UNINSTALL_PATH "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}"

######################################################################

VIProductVersion  "${VERSION}.0"
VIAddVersionKey "ProductName"  "${APP_NAME}"
VIAddVersionKey "CompanyName"  "${COMP_NAME}"
VIAddVersionKey "LegalCopyright"  "${COPYRIGHT}"
VIAddVersionKey "FileDescription"  "${DESCRIPTION}"
VIAddVersionKey "FileVersion"  "${VERSION}.0"

######################################################################

SetCompressor ZLIB
Name "${APP_NAME}"
Caption "${APP_NAME}"
OutFile "${INSTALLER_NAME}"
BrandingText "${APP_NAME}"
XPStyle on
InstallDirRegKey "${REG_ROOT}" "${REG_APP_PATH}" ""
InstallDir "$APPDATA\SQL Ant"

######################################################################

!include "MUI.nsh"

!define MUI_ABORTWARNING
!define MUI_UNABORTWARNING

!define MUI_WELCOMEPAGE_TITLE "SQL Ant Setup"
!define MUI_WELCOMEPAGE_TITLE_3LINES
!define MUI_WELCOMEPAGE_TEXT "Please close SQL Ant if it is running"
!insertmacro MUI_PAGE_WELCOME

!ifdef LICENSE_TXT
!insertmacro MUI_PAGE_LICENSE "..\${LICENSE_TXT}"
!endif

!ifdef REG_START_MENU
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "SQL Ant"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "${REG_ROOT}"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${UNINSTALL_PATH}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${REG_START_MENU}"
!insertmacro MUI_PAGE_STARTMENU Application $SM_Folder
!endif

!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_RUN "$INSTDIR\${MAIN_APP_EXE}"
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM

!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

######################################################################

Section -MainProgram
${INSTALL_TYPE}
SetOverwrite ifnewer
SetOutPath "$INSTDIR"
File "..\SQLAnt\bin\Debug\*.dll"
File "..\SQLAnt\bin\Debug\SQLAnt.exe"
File "..\SQLAnt\bin\Debug\SQLAnt.exe.config"
SectionEnd

######################################################################

Section -Additional
SetOutPath "$INSTDIR\snippets\Ant"
File "snippets\Ant\*.sql"
SectionEnd

Section -Additional
SetOutPath "$INSTDIR\autosaved"
File "welcome\*.sql"
SectionEnd

######################################################################

Section -Icons_Reg
SetOutPath "$INSTDIR"
WriteUninstaller "$INSTDIR\uninstall.exe"

!ifdef REG_START_MENU
!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
CreateDirectory "$SMPROGRAMS\$SM_Folder"
CreateShortCut "$SMPROGRAMS\$SM_Folder\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$DESKTOP\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$SMPROGRAMS\$SM_Folder\Uninstall ${APP_NAME}.lnk" "$INSTDIR\uninstall.exe"

!ifdef WEB_SITE
WriteIniStr "$INSTDIR\${APP_NAME} website.url" "InternetShortcut" "URL" "${WEB_SITE}"
CreateShortCut "$SMPROGRAMS\$SM_Folder\${APP_NAME} Website.lnk" "$INSTDIR\${APP_NAME} website.url"
!endif
!insertmacro MUI_STARTMENU_WRITE_END
!endif

!ifndef REG_START_MENU
CreateDirectory "$SMPROGRAMS\SQL Ant"
CreateShortCut "$SMPROGRAMS\SQL Ant\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$DESKTOP\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$SMPROGRAMS\SQL Ant\Uninstall ${APP_NAME}.lnk" "$INSTDIR\uninstall.exe"

!ifdef WEB_SITE
WriteIniStr "$INSTDIR\${APP_NAME} website.url" "InternetShortcut" "URL" "${WEB_SITE}"
CreateShortCut "$SMPROGRAMS\SQL Ant\${APP_NAME} Website.lnk" "$INSTDIR\${APP_NAME} website.url"
!endif
!endif

WriteRegStr ${REG_ROOT} "${REG_APP_PATH}" "" "$INSTDIR\${MAIN_APP_EXE}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayName" "${APP_NAME}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "UninstallString" "$INSTDIR\uninstall.exe"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayIcon" "$INSTDIR\${MAIN_APP_EXE}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayVersion" "${VERSION}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "Publisher" "${COMP_NAME}"

!ifdef WEB_SITE
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "URLInfoAbout" "${WEB_SITE}"
!endif
SectionEnd

######################################################################

Section Uninstall
${INSTALL_TYPE}
Delete "$INSTDIR\Microsoft.Data.ConnectionUI.Dialog.dll"
Delete "$INSTDIR\Microsoft.Data.ConnectionUI.dll"
Delete "$INSTDIR\Microsoft.SqlServer.ConnectionInfo.dll"
Delete "$INSTDIR\Microsoft.SqlServer.ConnectionInfoExtended.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Dmf.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.Sdk.Sfc.dll"
Delete "$INSTDIR\Microsoft.SqlServer.RegSvrEnum.dll"
Delete "$INSTDIR\Microsoft.SqlServer.ServiceBrokerEnum.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Smo.dll"
Delete "$INSTDIR\Microsoft.SqlServer.SmoExtended.dll"
Delete "$INSTDIR\Microsoft.SqlServer.SqlClrProvider.dll"
Delete "$INSTDIR\Microsoft.SqlServer.SqlEnum.dll"
Delete "$INSTDIR\ScintillaNET.dll"
Delete "$INSTDIR\Microsoft.SqlServer.AzureStorageEnum.dll"
Delete "$INSTDIR\Microsoft.SqlServer.BatchParser.dll"
Delete "$INSTDIR\Microsoft.SqlServer.BatchParserClient.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Diagnostics.Strace.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Dmf.Common.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.Collector.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.CollectorEnum.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.Common.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.RegisteredServers.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.SmoMetadataProvider.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.SqlParser.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.Utility.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.UtilityEnum.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.XEvent.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.XEventDbScoped.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.XEventDbScopedEnum.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Management.XEventEnum.dll"
Delete "$INSTDIR\Microsoft.SqlServer.PolicyEnum.dll"
Delete "$INSTDIR\Microsoft.SqlServer.SqlTDiagm.dll"
Delete "$INSTDIR\Microsoft.SqlServer.SqlWmiManagement.dll"
Delete "$INSTDIR\Microsoft.SqlServer.SString.dll"
Delete "$INSTDIR\Microsoft.SqlServer.Types.dll"
Delete "$INSTDIR\Microsoft.SqlServer.WmiEnum.dll"
Delete "$INSTDIR\Microsoft.SqlServer.XE.Core.dll"
Delete "$INSTDIR\Microsoft.SqlServer.XEvent.Linq.dll"
Delete "$INSTDIR\ScintillaNET FindReplaceDialog.dll"
Delete "$INSTDIR\SQLAnt.exe"
Delete "$INSTDIR\SQLAnt.exe.config"
Delete "$INSTDIR\uninstall.exe"
!ifdef WEB_SITE
Delete "$INSTDIR\${APP_NAME} website.url"
!endif

RmDir "$INSTDIR"

!ifdef REG_START_MENU
!insertmacro MUI_STARTMENU_GETFOLDER "Application" $SM_Folder
Delete "$SMPROGRAMS\$SM_Folder\${APP_NAME}.lnk"
Delete "$SMPROGRAMS\$SM_Folder\Uninstall ${APP_NAME}.lnk"
!ifdef WEB_SITE
Delete "$SMPROGRAMS\$SM_Folder\${APP_NAME} Website.lnk"
!endif
Delete "$DESKTOP\${APP_NAME}.lnk"

RmDir "$SMPROGRAMS\$SM_Folder"
!endif

!ifndef REG_START_MENU
Delete "$SMPROGRAMS\SQL Ant\${APP_NAME}.lnk"
Delete "$SMPROGRAMS\SQL Ant\Uninstall ${APP_NAME}.lnk"
!ifdef WEB_SITE
Delete "$SMPROGRAMS\SQL Ant\${APP_NAME} Website.lnk"
!endif
Delete "$DESKTOP\${APP_NAME}.lnk"

RmDir "$SMPROGRAMS\SQL Ant"
!endif

DeleteRegKey ${REG_ROOT} "${REG_APP_PATH}"
DeleteRegKey ${REG_ROOT} "${UNINSTALL_PATH}"
SectionEnd

######################################################################

