﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SQLAnt.UpdateChecker;
using System.Globalization;

namespace UnitTests
{
    [TestFixture]
    public class UpdateCheckerTests
    {
        string infos = @"STABLE VERSION: 1.1.0
STABLE URL:stable-url
BETA VERSION: 1.1.101
BETA URL: beta-url";

        [TestCase("30 Jun 2017 13:00", "30 Jun 2017 12:30", 1, false)]
        [TestCase("30 Jun 2017 13:00", "30 Jun 2017 12:00", 1, true)]
        [TestCase("30 Jun 2017 13:00", "30 Jun 2017 12:01", 1, false)]
        [TestCase("30 Jun 2017 01:00", "29 Jun 2017 13:00", 12, true)]
        [TestCase("25 Jun 2017 05:00", "24 Jun 2017 05:42", 24, false)]
        [TestCase("25 Jun 2017 05:00", "24 Jun 2017 05:00", 24, true)]
        [TestCase("25 Jun 2017 04:30", "24 Jun 2017 05:00", 24, false)]
        [TestCase("20 Jun 2017 06:00", "20 Jun 2017 06:00", 0, false)]
        public void UpdateCheckerLogic(string current, string lastChecked, int hoursBeforeCheck, bool expected)
        {
            var checker = new UpdateChecker();
            bool result = false;
            var currentValue = DateTime.ParseExact(current, "dd MMM yyyy HH:mm", CultureInfo.InvariantCulture);
            var lastCheckedValue = DateTime.ParseExact(lastChecked, "dd MMM yyyy HH:mm", CultureInfo.InvariantCulture);
            result = checker.ShouldCheckForUpdates(currentValue, lastCheckedValue, hoursBeforeCheck);
            Assert.AreEqual(expected, result);
        }

        [Test]
        public void ExtractVersionsInfo()
        {
            var checker = new UpdateChecker();
            var stableVersion = checker.ExtractInfoLine("STABLE VERSION", infos);
            Assert.AreEqual("1.1.0", stableVersion);
            var stableUrl = checker.ExtractInfoLine("STABLE URL", infos);
            Assert.AreEqual("stable-url", stableUrl);
            var betaVersion = checker.ExtractInfoLine("BETA VERSION", infos);
            Assert.AreEqual("1.1.101", betaVersion);
            var betaUrl = checker.ExtractInfoLine("BETA URL", infos);
            Assert.AreEqual("beta-url", betaUrl);
            var emptyInfo = checker.ExtractInfoLine(string.Empty, string.Empty);
            Assert.AreEqual(string.Empty, emptyInfo);
        }

        [TestCase("1.1.0", "1.1.0", "1.1.101", true, false, "no change on stable rule")]
        [TestCase("2.1.0", "1.1.0", "1.1.101", true, true, "major change on stable rule")]
        [TestCase("1.3.0", "1.1.0", "1.1.101", true, true, "minor change on stable rule")]
        [TestCase("1.1.10", "1.1.0", "1.1.101", true, false, "build change on stable rule")]
        [TestCase("1.1.99", "1.1.0", "1.1.101", true, false, "revision change on stable rule")]
        [TestCase("1.1.101", "1.1.0", "1.1.101", false, false, "no change on beta rule")]
        [TestCase("2.1.101", "1.1.0", "1.1.101", false, true, "major change on beta rule")]
        [TestCase("1.2.101", "1.1.0", "1.1.101", false, true, "minor change on beta rule")]
        [TestCase("1.1.100", "1.1.0", "1.1.101", false, true, "build change on beta rule")]
        [TestCase("1.1.101", "1.1.0", "1.1.101", false, false, "revision change on beta rule")]
        public void UpdateAvailable(string appVersion, string stableVersion, string betaVersion, bool stableOnly, bool expected, string rule)
        {
            var checker = new UpdateCheckResult()
            {
                CurrentVersion = appVersion,
                AvailableStableVersion = stableVersion,
                AvailableBetaVersion = betaVersion,
                CheckStableBuilds = stableOnly
            };
            var result = checker.UpdateAvailable();
            Assert.AreEqual(expected, result, message: rule);
        }

        [TestCase(true, "1.1.0.0", "1.1.0.99", "1.1.0.0", "stable-url", "beta-url", "stable-url")]
        [TestCase(false, "1.1.0.0", "1.1.0.0", "1.1.99.0", "stable-url", "beta-url", "beta-url")]
        public void RecommendedUrl(bool checkStable, string appVersion, string stableVersion, string betaVersion, string stableUrl, string betaUrl, string expected)
        {
            var checker = new UpdateCheckResult()
            {
                CurrentVersion = appVersion,
                AvailableStableVersion = stableVersion,
                AvailableBetaVersion = betaVersion,
                CheckStableBuilds = checkStable,
                StableUpdateURL = stableUrl,
                BetaUpdateURL = betaUrl
            };
            var result = checker.SuggestedUpdateUrl();
            Assert.AreEqual(expected, result);
        }

    }
}
