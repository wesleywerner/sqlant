﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.IO;
using System.Configuration;
using System.Collections.Specialized;
using SQLAnt;
using System.Data;
using System.Data.SqlClient;
using System.IO.Compression;

namespace UnitTests
{
    [TestFixture]
    public class SmoTests
    {
        string auditPath = Path.Combine(Path.GetTempPath(), "SQLAnt-Tests", "audits");
        string connectionstring = string.Empty;
        string database = string.Empty;
        SmoController smo = null;

        [SetUp]
        public void Setup()
        {
            // Load default configuration
            connectionstring = ConfigurationManager.AppSettings["connectionstring"];

            // Extract the database name for use in so request objects
            var builder = new SqlConnectionStringBuilder(connectionstring);
            database = builder.InitialCatalog;

            // Load user defined configuration
            string assemblyPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            if (File.Exists(Path.Combine(assemblyPath, "Custom.config")))
            {
                NameValueCollection customSection = (NameValueCollection)ConfigurationManager.GetSection("CustomAppSettings");
                connectionstring = customSection["connectionstring"];
            }

            smo = new SmoController(connectionstring, "SQLAntTestData");
            smo.ServerVersion();

            // Reset the audit path for each test
            if (Directory.Exists(auditPath)) Directory.Delete(auditPath, true);
            System.Threading.Thread.Sleep(250);
            Directory.CreateDirectory(auditPath);
        }


        [Test]
        public void DataIntegrity()
        {
            var request = new SmoQueryRequest() { Database = database, Query = "SELECT * FROM Users" };
            smo.ProcessRequest(request);
            var result = (DataSet)request.Result;
            Assert.AreEqual(1, result.Tables.Count, "Test data table not found");
            Assert.Greater(result.Tables[0].Rows.Count, 0, "No test data rows found");
        }

        [Test]
        public void GetDatabaseList()
        {
            var list = smo.GetDatabaseList();
            bool b = list.Contains(database);
            Assert.AreEqual(true, b, string.Format("No database named {0} found", database));
        }


        /// <summary>
        /// Tests deleting and inserting rows within a transaction, and verifies these are rolled back.
        /// </summary>
        [Test]
        public void Transactions()
        {
            // INSERT Test
            var request = new SmoQueryRequest() { Database = database, Query = "select max(id) from Users"};
            smo.ProcessRequest(request);
            var maxUserDs = (DataSet)request.Result;
            HasRow(maxUserDs);
            int userid = 1 + int.Parse(maxUserDs.Tables[0].Rows[0][0].ToString());
            string insertRow = string.Format("select max(id) from Users INSERT INTO Users (id,first_name) VALUES ({0}, 'Transaction User')", userid);
            smo.BeginTransaction();
            request = new SmoQueryRequest() { Database = database, Query = insertRow };
            smo.ProcessRequest(request);
            smo.RollbackTransaction();

            request = new SmoQueryRequest() { Database = database, Query = string.Format("select count(*) from Users where id = {0}", userid) };
            smo.ProcessRequest(request);
            var maxCountDs = (DataSet)request.Result;
            HasRow(maxCountDs);
            int maxCount = int.Parse(maxCountDs.Tables[0].Rows[0][0].ToString());
            Assert.AreEqual(maxCount, 0, "Transaction did not roll back INSERT ROW");

            // DELETE Test
            request = new SmoQueryRequest() { Database = database, Query = "select min(id) from Users" };
            smo.ProcessRequest(request);
            var minUserDs = (DataSet)request.Result;
            HasRow(maxUserDs);
            int minUserid = int.Parse(minUserDs.Tables[0].Rows[0][0].ToString());
            smo.BeginTransaction();
            request = new SmoQueryRequest() { Database = database, Query = string.Format("delete from Users where id = {0}", minUserid) };
            smo.ProcessRequest(request);
            smo.RollbackTransaction();

            request = new SmoQueryRequest() { Database = database, Query = string.Format("select count(*) from Users where id = {0}", minUserid) };
            smo.ProcessRequest(request);
            var minCountDs = (DataSet)request.Result;
            HasRow(minCountDs);
            int minCount = int.Parse(minCountDs.Tables[0].Rows[0][0].ToString());
            Assert.AreEqual(minCount, 1, "Transaction did not roll back DELETE ROW");

        }

        public void HasRow(DataSet ds)
        {
            Assert.Greater(ds.Tables.Count, 0, "No table in dataset");
            Assert.AreEqual(ds.Tables[0].Rows.Count, 1, "No row in dataset");
        }

        [Test]
        public void AuditingCSV()
        {
            var options = new SmoAuditOptions();
            options.Enabled = true;
            options.Format = "csv";
            options.Path = auditPath;
            options.Filename = "MyQuery.sql";
            
            var request = new SmoQueryRequest();
            request.AuditOptions = options;
            request.Database = database;
            request.Query = "SELECT * FROM Users; SELECT * FROM Payments";

            smo.ProcessRequest(request);

            bool exists = File.Exists(request.AuditOptions.GeneratedArchiveFilename);

            Assert.AreEqual(true, exists, "Audit archive does not exist");

            var info = new FileInfo(request.AuditOptions.GeneratedArchiveFilename);

            Assert.Greater(info.Length, 0, "Audit archive is zero length");

            // Test that the archive contains our query, and our results
            using (ZipArchive zip = ZipFile.Open(request.AuditOptions.GeneratedArchiveFilename, ZipArchiveMode.Read))
            {
                var queryEntry = zip.GetEntry("MyQuery.sql");
                Assert.NotNull(queryEntry, "Audit query entry is null");
                var reader = new StreamReader(queryEntry.Open());
                var queryText = reader.ReadToEnd();
                reader.Close();
                Assert.AreEqual(request.Query, queryText, "Audit query text not saved correctly inside archive");

                // Count 100 rows + header
                var result1Entry = zip.GetEntry("Result 0.csv");
                Assert.NotNull(result1Entry, "Audit CSV result file #1 not in the archive");

                var result2Entry = zip.GetEntry("Result 1.csv");
                Assert.NotNull(result2Entry, "Audit CSV result file #1 not in the archive");

                // count lines in archived result #1
                reader = new StreamReader(result1Entry.Open());
                int lines = 0;
                while (reader.ReadLine() != null) {
                    lines++;
                }
                Assert.AreEqual(101, lines, "Audit CSV result #1 row count");

                // count lines in archived result #2
                reader = new StreamReader(result2Entry.Open());
                lines = 0;
                while (reader.ReadLine() != null) {
                    lines++;
                }
                Assert.AreEqual(501, lines, "Audit CSV result #2 row count");

            }
            
        }

        [Test]
        public void AuditingXML()
        {
            var options = new SmoAuditOptions();
            options.Enabled = true;
            options.Format = "xml";
            options.Path = auditPath;
            options.Filename = "MyQuery.sql";
            
            var request = new SmoQueryRequest();
            request.AuditOptions = options;
            request.Database = database;
            request.Query = "SELECT * FROM Users; SELECT * FROM Payments";

            smo.ProcessRequest(request);

            bool exists = File.Exists(request.AuditOptions.GeneratedArchiveFilename);

            Assert.AreEqual(true, exists, "Audit archive does not exist");

            var info = new FileInfo(request.AuditOptions.GeneratedArchiveFilename);

            Assert.Greater(info.Length, 0, "Audit archive is zero length");

            // Test that the archive contains our query, and our results
            using (ZipArchive zip = ZipFile.Open(request.AuditOptions.GeneratedArchiveFilename, ZipArchiveMode.Read))
            {
                var queryEntry = zip.GetEntry("MyQuery.sql");
                Assert.NotNull(queryEntry, "Audit query entry is null");
                var reader = new StreamReader(queryEntry.Open());
                var queryText = reader.ReadToEnd();
                reader.Close();
                Assert.AreEqual(request.Query, queryText, "Audit query text not saved correctly inside archive");

                // Count 100 rows + header
                var result1Entry = zip.GetEntry("Result 0.xml");
                Assert.NotNull(result1Entry, "Audit XML result file #1 not in the archive");

                var result2Entry = zip.GetEntry("Result 1.xml");
                Assert.NotNull(result2Entry, "Audit XML result file #1 not in the archive");

                // count lines in datasets
                var dataset1 = new DataSet();
                dataset1.ReadXml(result1Entry.Open());
                Assert.AreEqual(1, dataset1.Tables.Count, "Audit XML result #1 expected to have 1 table");
                Assert.AreEqual(100, dataset1.Tables[0].Rows.Count, "Audit XML result #1 row count");
                dataset1.Dispose();

                // count lines in datasets
                var dataset2 = new DataSet();
                dataset2.ReadXml(result2Entry.Open());
                Assert.AreEqual(1, dataset2.Tables.Count, "Audit XML result #2 expected to have 1 table");
                Assert.AreEqual(500, dataset2.Tables[0].Rows.Count, "Audit XML result #2 row count");
                dataset2.Dispose();
            }
            
        }

        [TestCase("select * from foobar")]
        public void AuditingEmptyResults(string query)
        {
            // Insert queries return no result rows
            var options = new SmoAuditOptions();
            options.Enabled = true;
            options.Format = "csv";
            options.Path = auditPath;
            options.Filename = "MyQuery.sql";
            
            var request = new SmoQueryRequest();
            request.AuditOptions = options;
            request.Database = database;
            request.Query = query;
            smo.ProcessRequest(request);

            Assert.IsNull(request.AuditOptions.GeneratedArchiveFilename, "Audit archive should not exist, but it has a filename value");
        }

        [TestCase("insert into counters (counter) values (42);", 1)]
        [TestCase("insert into counters (counter) values (1), (2), (3);", 3)]
        public void ReturnRowsAffected(string query, int expectedRows)
        {
            // Insert queries return no result rows
            var options = new SmoAuditOptions();
            options.Enabled = true;
            options.Format = "csv";
            options.Path = auditPath;
            options.Filename = "MyQuery.sql";

            var request = new SmoQueryRequest();
            request.AuditOptions = options;
            request.Database = database;
            request.Query = query;
            smo.ProcessRequest(request);

            Assert.IsInstanceOf<DataSet>(request.Result, "A DataSet result is expected");
            var ds = (DataSet)request.Result;
            Assert.AreEqual(1, ds.Tables.Count, "One data table is expected in the result");
            Assert.AreEqual(1, ds.Tables[0].Rows.Count, "One data row is expected in the result");
            var actualRows = int.Parse(ds.Tables[0].Rows[0][0].ToString());
            Assert.AreEqual(expectedRows, actualRows, "Rows affected does not match");
        }

        [Test]
        public void AuditingSearching()
        {
            var options = new SmoAuditOptions();
            options.Enabled = true;
            options.Format = "csv";
            options.Path = auditPath;
            options.Filename = string.Empty;
            
            var request = new SmoQueryRequest();
            request.AuditOptions = options;
            request.Database = database;
            request.Query = "SELECT * FROM Payments";

            smo.ProcessRequest(request);

            bool exists = File.Exists(request.AuditOptions.GeneratedArchiveFilename);

            Assert.AreEqual(true, exists, "Audit archive does not exist");

            var info = new FileInfo(request.AuditOptions.GeneratedArchiveFilename);

            Assert.Greater(info.Length, 0, "Audit archive is zero length");

            // Search the audit query only
            var audit = new SmoAuditing();
            var auditOptions = new AuditSearchOptions();
            auditOptions.Path = options.Path;
            auditOptions.SearchData = false;
            auditOptions.Filter = "payments";

            var oneResult = audit.InitializeSearch(auditOptions);
            oneResult = audit.SearchNext(oneResult);
            Assert.AreEqual(1, oneResult.Matches.Count, "One search result expected");
            Assert.IsNull(oneResult.Matches[0].Data, "");
            Assert.AreEqual(options.GeneratedArchiveFilename, oneResult.Matches[0].Filename, "Audit archive filename expected");

            auditOptions.Filter = "879818753-8";
            var noResult = audit.InitializeSearch(auditOptions);
            noResult = audit.SearchNext(noResult);
            Assert.AreEqual(0, noResult.Matches.Count, "Expected no search results");

            auditOptions.Filter = "879818753-8";
            auditOptions.SearchData = true;
            var dataResult = audit.InitializeSearch(auditOptions);
            dataResult = audit.SearchNext(dataResult);
            Assert.AreEqual(1, dataResult.Matches.Count, "Expected a result from searching archive data.");
            Assert.IsNotNull(dataResult.Matches[0].Data, "Expected data result to have a value of the matched line");
            Assert.AreEqual(options.GeneratedArchiveFilename, dataResult.Matches[0].Filename, "Audit archive filename expected");

            // Clean up the generated archive for future (local) tests
            File.Delete(request.AuditOptions.GeneratedArchiveFilename);
        }

        /// <summary>
        /// Tests that comments are removed from a query prior to execution.
        /// Specifically if a batch statement resides within a comment, it would split the batch inside a comment
        /// breaking a query illogically.
        /// </summary>
        [Test]
        public void ExcludeCommentsOnExecute()
        {
            var request = new SmoQueryRequest() {
                Database = database,
                Query = @"/*
drop TABLE [Other-Table]
go
CREATE TABLE MyTable (id int, [person-name] varchar(max))
*/
SELECT * FROM Users"
            };
            try
            {
                smo.ProcessRequest(request);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(false, "Query comments bamboozled the batch splitting");
            }
        }

        /// <summary>
        /// Tests that the rowcount limit is honored.
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="expected"></param>
        [TestCase(21, 21)]
        [TestCase(3, 3)]
        [TestCase(0, 100)]
        public void RowCountLimit(int limit, int expected)
        {
            var request = new SmoQueryRequest() {
                Database = database,
                Query = "SELECT * FROM Users",
                RowCountLimit = limit
            };
            smo.ProcessRequest(request);
            Assert.NotNull(request.Result);
            Assert.IsInstanceOf<DataSet>(request.Result);
            var result = (DataSet)request.Result;
            Assert.AreEqual(1, result.Tables.Count);
            Assert.AreEqual(expected, result.Tables[0].Rows.Count);
        }

        [Test]
        public void QueryDatabaseMetadata()
        {
            var metadata = smo.GetDatabaseMetadata(null);
            var tables = metadata.GetTableList().Count;
            var procedures = metadata.GetProcedureList().Count;
            var functions = metadata.GetFunctionList().Count;
            var views = metadata.GetViewList().Count;
            Assert.AreEqual(4, tables, "Number of tables does not match");
            Assert.AreEqual(1, views, "Number of views does not match");
            Assert.AreEqual(1, procedures, "Number of procedures does not match");
            Assert.AreEqual(1, functions, "Number of functions does not match");
        }

        [Test]
        public void QueryTableColumns()
        {
            var metadata = smo.GetDatabaseMetadata(null);
            var columns = metadata.GetColumnList("Users");
            Assert.AreEqual(6, columns.Count, "Number of table columns does not match");
            Assert.Contains("email", columns, "Users table expected to have the email column");
        }

        [Test]
        public void QueryViewColumns()
        {
            var metadata = smo.GetDatabaseMetadata(null);
            var columns = metadata.GetColumnList("UserAccountsList");
            Assert.AreEqual(4, columns.Count, "Number of view columns does not match");
            Assert.Contains("email", columns, "View expected to have the email column");
        }

        [Test]
        public void QueryProcedureParameters()
        {
            var metadata = smo.GetDatabaseMetadata(null);
            var parameters = metadata.GetProcedureParameterList("UserAccountsReport");
            Assert.AreEqual(1, parameters.Count, "Number of procedure parameters does not match");
            Assert.AreEqual("@NameFilter", parameters[0], "Procedure parameter name does not match");
        }

        [Test]
        public void QueryFunctionParameters()
        {
            var metadata = smo.GetDatabaseMetadata(null);
            var parameters = metadata.GetFunctionParameterList("GetAccountBalance");
            Assert.AreEqual(1, parameters.Count, "Number of function parameters does not match");
            Assert.AreEqual("@UserID", parameters[0], "Function parameter name does not match");
        }

    }
}
