﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SQLAnt.Cleaner;
using System.IO;

namespace UnitTests
{
    [TestFixture]
    public class CleanerTests
    {
        CleanerOptions options;

        [SetUp]
        public void Setup()
        {
            // configure default cleaner options
            var temp = Path.Combine(Path.GetTempPath(), "SQLAnt-Tests", "cleaner");
            options = new CleanerOptions();
            options.Age = 14;
            options.Paths = new string[] {
                Path.Combine(temp, "audits"),
                Path.Combine(temp, "cache"),
                Path.Combine(temp, "errors"),
                Path.Combine(temp, "logs")
                };

            // Create some test files
            foreach (string path in options.Paths)
            {
                Directory.CreateDirectory(path);
                for (int i = 0; i < 10; i++)
                {
                    var fileCreatedDate = DateTime.Today.AddDays(-i);
                    var filename = Path.Combine(path, string.Format("Test{0}", i));
                    var stream = File.Create(filename);
                    stream.Close();
                    File.SetCreationTime(filename, fileCreatedDate);
                }
            }
        }

        [TestCase(1, 32)]
        [TestCase(5, 16)]
        [TestCase(8, 4)]
        [TestCase(9, 0)]
        public void CleanerAgeDetection(int age, int result)
        {
            options.Age = age;
            var cleaner = new CleanerAnalysis(options);
            cleaner.StartAnalysis();
            // Expects to find 16 files to clean: 4 from each of the 4 paths
            Assert.AreEqual(result, cleaner.Results.Count);
        }

        [Test]
        public void CleanerFileRemoval()
        {
            options.Age = 5;
            var cleaner = new CleanerAnalysis(options);
            cleaner.StartAnalysis();
            while (true)
            {
                if (!cleaner.CleanNext()) break;
            }
            System.Threading.Thread.Sleep(500);
            foreach (var path in options.Paths)
            {
                var files = Directory.GetFiles(path);
                Assert.AreEqual(6, files.Count());
            }
        }
    }
}
