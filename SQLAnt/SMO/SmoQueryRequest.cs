﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SQLAnt
{
    public class SmoQueryRequest
    {

        /// <summary>
        /// The server to query
        /// </summary>
        public string Server { get; set; }

        /// <summary>
        /// The database from which to collect all object names
        /// </summary>
        public string Database { get; set; }

        /// <summary>
        /// By default object names are cached for speed. Set this to forcibly interrogate the database and refresh the cached list.
        /// </summary>
        public bool Forced { get; set; }

        /// <summary>
        /// The editor window performing this request
        /// </summary>
        public CodeEditor Target { get; set; }

        /// <summary>
        /// The result of the query
        /// </summary>
        public object Result { get; set; }

        /// <summary>
        /// Time the request started
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Time taken to complete the request
        /// </summary>
        public double Duration { get; set; }

        /// <summary>
        /// Query to execute
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// Stored options for query auditing
        /// </summary>
        public SmoAuditOptions AuditOptions { get; set; }

        /// <summary>
        /// List of query errors
        /// </summary>
        public List<SmoQueryError> QueryErrors { get; set; }

        /// <summary>
        /// Limit the number of rows returned. 0 disables this limit.
        /// </summary>
        public int RowCountLimit { get; set; }

    }
}
