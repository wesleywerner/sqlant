﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAnt
{
    public class AuditSearchOptions
    {
        /// <summary>
        /// The path where the archives live
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Search within the archived results data
        /// </summary>
        public bool SearchData { get; set; }

        /// <summary>
        /// The text to look for
        /// </summary>
        public string Filter { get; set; }
    }
}
