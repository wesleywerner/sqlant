﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace SQLAnt
{
    /// <summary>
    /// Provides database metadata management
    /// </summary>
    public class DatabaseMetadata
    {
        internal DataSet Tables;
        internal DataSet Procedures;
        internal DataSet Views;
        internal DataSet Functions;

        internal DatabaseMetadata()
        {
            this.Tables = new DataSet("tables");
            this.Procedures = new DataSet("procedures");
            this.Views = new DataSet("views");
            this.Functions = new DataSet("functions");
        }

        internal void AddTable(string schema, string name, IEnumerable<DataColumn> columns)
        {
            if (!this.Tables.Tables.Contains(name))
            {
                var newtable = this.Tables.Tables.Add(name);
                newtable.ExtendedProperties.Add("schema", schema);
                newtable.Columns.AddRange(columns.ToArray());
            }
        }

        internal void AddView(string schema, string name, IEnumerable<DataColumn> columns)
        {
            if (!this.Views.Tables.Contains(name))
            {
                var newtable = this.Views.Tables.Add(name);
                newtable.ExtendedProperties.Add("schema", schema);
                newtable.Columns.AddRange(columns.ToArray());
            }
        }

        internal void AddProcedure(string schema, string name, IEnumerable<DataColumn> columns)
        {
            if (!this.Procedures.Tables.Contains(name))
            {
                var newtable = this.Procedures.Tables.Add(name);
                newtable.ExtendedProperties.Add("schema", schema);
                newtable.Columns.AddRange(columns.ToArray());
            }
        }

        internal void AddFunction(string schema, string name, IEnumerable<DataColumn> columns)
        {
            if (!this.Functions.Tables.Contains(name))
            {
                var newtable = this.Functions.Tables.Add(name);
                newtable.ExtendedProperties.Add("schema", schema);
                newtable.Columns.AddRange(columns.ToArray());
            }
        }

        internal void Save(string filename)
        {
            string cacheFile;
            cacheFile = filename + " tables.xml";
            this.Tables.WriteXmlSchema(cacheFile);
            File.SetCreationTime(cacheFile, DateTime.Now);

            cacheFile = filename + " views.xml";
            this.Views.WriteXmlSchema(cacheFile);
            File.SetCreationTime(cacheFile, DateTime.Now);

            cacheFile = filename + " procedures.xml";
            this.Procedures.WriteXmlSchema(cacheFile);
            File.SetCreationTime(cacheFile, DateTime.Now);

            cacheFile = filename + " functions.xml";
            this.Functions.WriteXmlSchema(cacheFile);
            File.SetCreationTime(cacheFile, DateTime.Now);
        }

        internal void Read(string filename)
        {
            string cacheFile;
            cacheFile = filename + " tables.xml";
            if (File.Exists(cacheFile))
                this.Tables.ReadXmlSchema(cacheFile);

            cacheFile = filename + " views.xml";
            if (File.Exists(cacheFile))
                this.Views.ReadXmlSchema(cacheFile);

            cacheFile = filename + " procedures.xml";
            if (File.Exists(cacheFile))
                this.Procedures.ReadXmlSchema(cacheFile);

            cacheFile = filename + " functions.xml";
            if (File.Exists(cacheFile))
                this.Functions.ReadXmlSchema(cacheFile);
        }

        public bool IsEmpty()
        {
            return (this.Tables.Tables.Count == 0 && this.Procedures.Tables.Count == 0 && this.Views.Tables.Count == 0 && this.Functions.Tables.Count == 0);
        }

        public List<string> GetTableList()
        {
            return (from t in this.Tables.Tables.Cast<DataTable>() select t.TableName).ToList();
        }

        public List<string> GetViewList()
        {
            return (from t in this.Views.Tables.Cast<DataTable>() select t.TableName).ToList();
        }

        public List<string> GetColumnList(string tablename)
        {
            var table = (from t in this.Tables.Tables.Cast<DataTable>()
                         where t.TableName == tablename
                         select t).FirstOrDefault();
            var view = (from t in this.Views.Tables.Cast<DataTable>()
                         where t.TableName == tablename
                         select t).FirstOrDefault();

            if (table != null)
            {
                return (from col in table.Columns.Cast<DataColumn>()
                                         select col.ColumnName).ToList();
            }
            if (view != null)
            {
                return (from col in view.Columns.Cast<DataColumn>()
                                         select col.ColumnName).ToList();
            }
            return new List<string>();
        }

        public List<string> GetProcedureList()
        {
            return (from t in this.Procedures.Tables.Cast<DataTable>() select t.TableName).ToList();
        }

        public List<string> GetFunctionList()
        {
            return (from t in this.Functions.Tables.Cast<DataTable>() select t.TableName).ToList();
        }

        public List<string> GetProcedureParameterList(string procedure)
        {
            var table = (from t in this.Procedures.Tables.Cast<DataTable>()
                         where t.TableName == procedure
                         select t).FirstOrDefault();

            if (table != null)
            {
                return (from col in table.Columns.Cast<DataColumn>()
                        select col.ColumnName).ToList();
            }
            return new List<string>();
        }

        public List<string> GetFunctionParameterList(string function)
        {
            var table = (from t in this.Functions.Tables.Cast<DataTable>()
                         where t.TableName == function
                         select t).FirstOrDefault();

            if (table != null)
            {
                return (from col in table.Columns.Cast<DataColumn>()
                        select col.ColumnName).ToList();
            }
            return new List<string>();
        }

        internal bool ContainsTableOrView(string name)
        {
            return this.Tables.Tables.Contains(name) || this.Views.Tables.Contains(name);
        }

        /// <summary>
        /// Gets a table or view name (case insensitive)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        internal string GetTableOrViewInsentitiveLookup(string name)
        {
            name = name.ToLower();
            var table = (from t in this.Tables.Tables.Cast<DataTable>()
                         where t.TableName.ToLower() == name
                         select t).FirstOrDefault();
            var view = (from t in this.Views.Tables.Cast<DataTable>()
                         where t.TableName.ToLower() == name
                         select t).FirstOrDefault();

            if (table != null)
            {
                return table.TableName;
            }
            if (view != null)
            {
                return view.TableName;
            }
            return string.Empty;
        }

        internal string GetSchemaName(string name)
        {
            if (this.Tables.Tables.Contains(name))
            {
                if (this.Tables.Tables[name].ExtendedProperties.ContainsKey("schema"))
                {
                    return this.Tables.Tables[name].ExtendedProperties["schema"].ToString();
                }
            }
            if (this.Views.Tables.Contains(name))
            {
                if (this.Views.Tables[name].ExtendedProperties.ContainsKey("schema"))
                {
                    return this.Views.Tables[name].ExtendedProperties["schema"].ToString();
                }
            }
            if (this.Procedures.Tables.Contains(name))
            {
                if (this.Procedures.Tables[name].ExtendedProperties.ContainsKey("schema"))
                {
                    return this.Procedures.Tables[name].ExtendedProperties["schema"].ToString();
                }
            }
            if (this.Functions.Tables.Contains(name))
            {
                if (this.Functions.Tables[name].ExtendedProperties.ContainsKey("schema"))
                {
                    return this.Functions.Tables[name].ExtendedProperties["schema"].ToString();
                }
            }
            return string.Empty;
        }


        internal bool TableContainsColumn(string tableName, string columnName)
        {
            tableName = tableName.ToLower();
            columnName = columnName.ToLower();
            var table = (from t in this.Tables.Tables.Cast<DataTable>()
                         where t.TableName.ToLower() == tableName
                         select t).FirstOrDefault();
            var view = (from t in this.Views.Tables.Cast<DataTable>()
                         where t.TableName.ToLower() == tableName
                         select t).FirstOrDefault();

            if (table != null)
            {
                return (from col in table.Columns.Cast<DataColumn>()
                            where col.ColumnName.ToLower() == columnName
                            select col).Any();
            }
            if (view != null)
            {
                return (from col in view.Columns.Cast<DataColumn>()
                            where col.ColumnName.ToLower() == columnName
                            select col).Any();
            }
            return false;
        }


        /// <summary>
        /// Qualify an object name only when necessary, ie when the name contains characters that require qualifying.
        /// </summary>
        /// <param name="objectName"></param>
        /// <returns></returns>
        internal string QualifyObjectName(string objectName)
        {
            if (objectName.Contains(" ") || objectName.Contains("-"))
            {
                objectName = string.Format("[{0}]", objectName);
            }

            var schema = GetSchemaName(objectName);
            if (schema != string.Empty && schema != "dbo")
            {
                return string.Format("{0}.{1}", schema, objectName);
            }
            else
            {
                return objectName;
            }
        }

        
        /// <summary>
        /// Fully qualify an object name.
        /// </summary>
        /// <param name="objectName"></param>
        /// <returns></returns>
        internal string QualifyFullName(string objectName)
        {
            var schema = GetSchemaName(objectName);
            return string.Format("[{0}].[{1}]", schema, objectName);
        }

    }
}
