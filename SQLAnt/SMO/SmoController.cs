﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;
using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.SqlServer.Management.Common;

namespace SQLAnt
{
    /// <summary>
    /// Provides a high level SMO access object.
    /// </summary>
    public class SmoController
    {
        // Fix: must reference the BatchParserClient DLL so reflection picks it up and copies it to our unit test output directory.
        Microsoft.SqlServer.Management.Common.ExecuteBatch dummyReferenceEnsuresDLLCopyToUnitTestProject;

        Server server { get; set; }
        private string Database { get; set; }

        /// <summary>
        /// Initialize a new smo controller.
        /// The connection string should contain the server and authentication details, the database is given seperately.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="database"></param>
        public SmoController(string connection, string database)
        {
            this.Database = database;
            this.server = new Server();
            this.server.SetDefaultInitFields(true);
            var builder = new SqlConnectionStringBuilder(connection);
            builder.InitialCatalog = database ?? string.Empty;
            this.server.ConnectionContext.ConnectionString = builder.ToString();
        }

        public Version ServerVersion()
        {
            this.server.ConnectionContext.Connect();
            return this.server.Information.Version;
        }

        Database db
        {
            get
            {
                if (!this.server.Databases.Contains(this.Database))
                {
                    throw new Exception(string.Format("Database {0} does not exist", this.Database));
                }
                return this.server.Databases[this.Database];
            }
        }

        public List<string> GetDatabaseList()
        {
            // Enumerating server.Databases takes up to a minute long
            // Get database list as a query instead.
            var data = this.server.ConnectionContext.ExecuteWithResults("SELECT name FROM master.dbo.sysdatabases ORDER BY name");
            if (data.Tables.Count == 1)
            {
                return data.Tables[0].Rows.Cast<DataRow>().Select(n => n[0]).Cast<string>().ToList();
            }
            else
            {
                return new List<string>();
            }
        }

        public void ProcessRequest(SmoQueryRequest request)
        {
            bool multibatch = false;
            int CurrentLinePos = 0;
            int batchPos = 0;
            try
            {
                // Issue 56: We cannot use GO batches with smo execution, it only works when script objects.
                // Tried many versions of RegEx to split or replace GO batches, except it usually ends up
                // matching blank line breaks, breaking the reported error lines if the query has an error.
                var builder = new StringBuilder(request.Query.Length);
                var stream = new System.IO.StringReader(request.Query);
                string line = string.Empty;
                var ds = new DataSet();
                int resultNumber = 0;

                if (request.RowCountLimit > 0)
                {
                    this.server.ConnectionContext.ExecuteNonQuery(string.Format("SET ROWCOUNT {0}", request.RowCountLimit));
                }

                do
                {
                    line = stream.ReadLine();

                    bool isempty = line == null;
                    bool isbatch = isempty ? false : line.ToUpper().Trim() == "GO";

                    // batch delimiter or end of stream
                    if (isempty || isbatch)
                    {
                        // #58 Display number of rows affected for executed queries
                        var buildQueryLower = builder.ToString().ToLower();
                        var isNonQuery = buildQueryLower.StartsWith("update") || buildQueryLower.StartsWith("insert") || buildQueryLower.StartsWith("delete");

                        if (isNonQuery)
                        {
                            // this query expects a rows affected result
                            var rows = this.server.ConnectionContext.ExecuteNonQuery(builder.ToString());
                            var dt = new DataTable();
                            dt.Columns.Add("Rows Affected");
                            dt.Rows.Add(new object[] { rows });
                            ds.Tables.Add(dt);
                        }
                        else
                        {
                            // this query expects a dataset result
                            var batchdata = this.server.ConnectionContext.ExecuteWithResults(builder.ToString());

                            while (batchdata.Tables.Count > 0)
                            {
                                // Count the number of results for all batches
                                resultNumber++;
                                // Move each table to the result dataset. Each must have a unique name
                                var dt = batchdata.Tables[0];
                                batchdata.Tables.RemoveAt(0);
                                dt.TableName = string.Format("Result {0}", resultNumber);
                                ds.Tables.Add(dt);
                            }
                        }

                        // Build the next batch query
                        builder.Clear();

                        // Remember the current batch start position for the next query
                        if (isbatch)
                        {
                            multibatch = true;
                            batchPos = CurrentLinePos + 1;
                        }

                    }
                    else
                    {
                        // Add this line to the batch query
                        CurrentLinePos++;
                        builder.AppendLine(line);
                    }
                } while (line != null);

                // Assign final result set
                request.Result = ds;

                var audit = new SmoAuditing();
                audit.Start(request);
            }
            catch (Exception ex)
            {
                var o = ExtractSqlError(ex);
                if (o == null)
                {
                    // bubble any non query errors up the stack
                    throw ex;
                }
                else
                {
                    request.QueryErrors = new List<SmoQueryError>();
                    request.QueryErrors.Add(new SmoQueryError(o.LineNumber + (multibatch ? batchPos : 0), o.Message));
                }
            }
            
        }


        private SqlException ExtractSqlError(Exception ex)
        {
            if (ex.InnerException != null)
            {
                return ExtractSqlError(ex.InnerException);
            }
            else if ((object)ex is SqlException)
            {
                return (SqlException)(object)ex;
            }
            else
            {
                return null;
            }
        }

        public void BeginTransaction()
        {
            if (this.HasTransaction())
            {
                throw new Exception("There is already a Transaction in progress. Commit or Reject that one first.");
            }
            this.server.ConnectionContext.BeginTransaction();
        }

        private bool HasTransaction()
        {
            return this.server.ConnectionContext.TransactionDepth > 0;
        }

        public void CommitTransaction()
        {
            if (!this.HasTransaction())
            {
                // a query error (esp a DDL operation that fails) auto rolls back the transaction
                // so let the error bubble up instead of replacing it with our own.
                //throw new Exception("There are no transactions to commit.");
            }
            else
            {
                this.server.ConnectionContext.CommitTransaction();
            }
        }

        public bool RollbackTransaction()
        {
            if (this.HasTransaction())
            {
                this.server.ConnectionContext.RollBackTransaction();
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Query the database for its metadata
        /// </summary>
        /// <returns></returns>
        public DatabaseMetadata GetDatabaseMetadata(SmoQueryRequest request)
        {
            var meta = new DatabaseMetadata();

            foreach (Table table in this.db.Tables)
            {
                if (!table.IsSystemObject)
                {
                    var columns = new List<DataColumn>();
                    foreach (Microsoft.SqlServer.Management.Smo.Column c in table.Columns)
                    {
                        columns.Add(new DataColumn(c.Name, c.DataType.GetType()));
                    }
                    meta.AddTable(table.Schema, table.Name, columns);
                }
            }

            foreach (View view in this.db.Views)
            {
                if (!view.IsSystemObject)
                {
                    var columns = new List<DataColumn>();
                    foreach (Microsoft.SqlServer.Management.Smo.Column c in view.Columns)
                    {
                        columns.Add(new DataColumn(c.Name, c.DataType.GetType()));
                    }
                    meta.AddView(view.Schema, view.Name, columns);
                }
            }

            foreach (StoredProcedure sproc in this.db.StoredProcedures)
            {
                if (!sproc.IsSystemObject)
                {
                    var parameters = (from param in sproc.Parameters.Cast<StoredProcedureParameter>()
                                      select new DataColumn(param.Name));
                    meta.AddProcedure(sproc.Schema, sproc.Name, parameters);
                }
            }

            foreach (UserDefinedFunction func in this.db.UserDefinedFunctions)
            {
                if (!func.IsSystemObject)
                {
                    var parameters = (from param in func.Parameters.Cast<UserDefinedFunctionParameter>()
                                      select new DataColumn(param.Name));
                    meta.AddFunction(func.Schema, func.Name, parameters);
                }
            }

            return meta;
        }

        public StringBuilder ScriptTable(string tablename, ScriptingOptions options)
        {
            var output = new StringBuilder();
            Table table = null;

            foreach (Table n in this.db.Tables)
            {
                if (string.Format("[{0}].[{1}]", n.Schema, n.Name) == tablename)
                {
                    table = n;
                }
            }

            if (table != null)
            {
                output.AppendLine("GO");
                var scripter = new Scripter(server) { Options = options };
                var script = scripter.EnumScript(new SqlSmoObject[] { table });
                foreach (var line in script)
                {
                    output.AppendLine(line);
                }
            }
            return output;
        }

        public StringBuilder ScriptView(string viewname, ScriptingOptions options)
        {
            var output = new StringBuilder();
            View vw = null;

            foreach (View n in this.db.Views)
            {
                if (string.Format("[{0}].[{1}]", n.Schema, n.Name) == viewname)
                {
                    vw = n;
                }
            }

            if (vw != null)
            {
                output.AppendLine("GO");
                var scripter = new Scripter(server) { Options = options };
                var script = scripter.EnumScript(new SqlSmoObject[] { vw });
                foreach (var line in script)
                {
                    output.AppendLine(line);
                }
            }
            return output;
        }

        public StringBuilder ScriptProcedure(string procname, ScriptingOptions options)
        {
            var output = new StringBuilder();
            StoredProcedure sp = null;

            foreach (StoredProcedure n in this.db.StoredProcedures)
            {
                if (string.Format("[{0}].[{1}]", n.Schema, n.Name) == procname)
                {
                    sp = n;
                }
            }

            if (sp != null)
            {
                output.AppendLine("GO");
                var scripter = new Scripter(server) { Options = options };
                var script = scripter.EnumScript(new SqlSmoObject[] { sp });
                foreach (var line in script)
                {
                    output.AppendLine(line);
                }
            }
            return output;
        }

        public DateTime LastSchemaChangedDate(DateTime LastChange)
        {
            string object_modified_query = @"SELECT TOP 1 o.create_date dt
FROM sys.all_objects o
LEFT OUTER JOIN sys.schemas s
ON ( o.schema_id = s.schema_id)
WHERE type in ('P', 'U')
ORDER BY create_date DESC

SELECT TOP 1 o.modify_date dt
FROM sys.all_objects o
LEFT OUTER JOIN sys.schemas s
ON ( o.schema_id = s.schema_id)
WHERE type in ('P', 'U')
ORDER BY o.modify_date DESC";

            DateTime NewestChange = LastChange;
            var ds = this.db.ExecuteWithResults(object_modified_query);
            if (ds == null) return LastChange;
            if (ds.Tables.Count == 0) return LastChange;

            foreach (DataTable t in ds.Tables)
            {
                if (t.Rows.Count == 1)
                {
                    DateTime apparentDate;
                    if (DateTime.TryParse(t.Rows[0][0].ToString(), out apparentDate))
                    {
                        if (apparentDate > NewestChange)
                        {
                            NewestChange = apparentDate;
                        }
                    }
                }
            }
            return NewestChange;
        }

    }
}
