﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SQLAnt
{
    class ScriptRequest
    {
        internal List<string> Tables { get; set; }
        internal List<string> Views { get; set; }
        internal List<string> Procedures { get; set; }
        internal List<StringBuilder> Scripts { get; set; }
        internal ScriptingOptions options { get; set; }

        internal ScriptRequest()
        {
            this.Tables = new List<string>();
            this.Views = new List<string>();
            this.Procedures = new List<string>();
            this.Scripts = new List<StringBuilder>();
        }

    }
}
