﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAnt
{
    public class SmoQueryError
    {
        public int Line { get; set; }
        public string Message { get; set; }
        public SmoQueryError() { }
        public SmoQueryError(int line, string message)
        {
            Line = line;
            Message = message;
        }
    }
}
