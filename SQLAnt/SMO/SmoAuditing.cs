﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Data;

namespace SQLAnt
{
    public class SmoAuditing
    {

        /// <summary>
        /// New constructor
        /// </summary>
        public SmoAuditing()
        { }

        /// <summary>
        /// Performs a history audit on the given request object.
        /// </summary>
        /// <param name="request"></param>
        public void Start(SmoQueryRequest request)
        {
            if (request.AuditOptions == null)
            {
                Utilities.LogAction("Query auditing options not given");
                return;
            }

            if (!request.AuditOptions.Enabled)
            {
                Utilities.LogAction("Query auditing disabled");
                return;
            }

            var datasets = (DataSet)request.Result;

            // Generate the archive filename.
            // If it exists, try another.
            string filenameTemplate = "AUDIT {0} ({1}) [{2:yyyy-MM-dd}] {2:HH}h {2:mm}m {2:ss}s.zip";
            string archiveFilename = string.Empty;
            string archivePath = string.Empty;
            do
            {
                archiveFilename = string.Format(filenameTemplate, request.Server, request.Database, DateTime.Now);
                archiveFilename = Utilities.CleanPath(archiveFilename);
                archivePath = Path.Combine(request.AuditOptions.Path, archiveFilename);
            } while (File.Exists(archivePath));

            using (ZipArchive zip = ZipFile.Open(archivePath, ZipArchiveMode.Create))
            {
                var queryFile = zip.CreateEntry(request.AuditOptions.Filename == string.Empty ? "query.sql" : Path.GetFileName(request.AuditOptions.Filename));
                var queryStream = new StreamWriter(queryFile.Open());
                queryStream.Write(request.Query);
                queryStream.Flush();
                queryStream.Close();

                if (datasets != null)
                {
                    for (int i = 0; i < datasets.Tables.Count; i++)
                    {
                        var table = datasets.Tables[i];

                        if (request.AuditOptions.Format == "xml")
                        {
                            var resultFile = zip.CreateEntry(string.Format("Result {0}.xml", i));
                            var resultStream = resultFile.Open();
                            table.WriteXml(resultStream);
                            resultStream.Flush();
                            resultStream.Close();
                        }
                        else
                        {
                            // CSV format
                            var resultFile = zip.CreateEntry(string.Format("Result {0}.csv", i));
                            var resultStream = resultFile.Open();
                            var resultWriter = new StreamWriter(resultStream);

                            // Header
                            var headerNames = (from c in table.Columns.Cast<DataColumn>() select c.ColumnName.CsvEscape());
                            resultWriter.WriteLine(string.Join(",", headerNames));

                            // Data
                            foreach (DataRow row in table.Rows)
                            {
                                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString().CsvEscape());
                                resultWriter.WriteLine(string.Join(",", fields));
                            }

                            resultWriter.Flush();
                            resultStream.Close();
                        }

                    }
                }


            }

            request.AuditOptions.GeneratedArchiveFilename = archivePath;

        }


        public AuditSearchState InitializeSearch(AuditSearchOptions options)
        {
            var state = new AuditSearchState();
            state.Options = options;
            state.Files = Directory.GetFiles(options.Path, "*.zip");
            return state;
        }



        /// <summary>
        /// Searches through archived audit records.
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public AuditSearchState SearchNext(AuditSearchState state)
        {
            state.Position++;

            // Get audit archive file info
            var archivefile = state.Files[state.Position];
            var info = new AuditSearchInfo();
            var fileinfo = new FileInfo(archivefile);
            info.Date = fileinfo.CreationTime;
            info.Size = fileinfo.Length;

            // Search inside the archive
            using (ZipArchive zip = ZipFile.OpenRead(archivefile))
            {
                foreach (var entry in zip.Entries)
                {
                    string extension = Path.GetExtension(entry.Name).ToLower();
                    if (extension != ".sql" && state.Options.SearchData)
                    {
                        // Data searches only return the context lines that match the filter, not the entire result
                        var context = MatchContextByLine(state.Options.Filter, entry);
                        if (!string.IsNullOrEmpty(context))
                        {
                            info.Filename = archivefile;
                            info.Data = context;
                        }
                    }
                }
                foreach (var entry in zip.Entries)
                {
                    string extension = Path.GetExtension(entry.Name).ToLower();
                    if (extension == ".sql")
                    {
                        string context = string.Empty;
                        // Always include the query if the data matched to a result
                        if (string.IsNullOrEmpty(info.Data))
                        {
                            context = MatchContext(state.Options.Filter, entry);
                        }
                        else
                        {
                            context = ReadFile(entry);
                        }
                        if (!string.IsNullOrEmpty(context))
                        {
                            info.Filename = archivefile;
                            info.Query = context;
                        }
                    }
                }
                if (!string.IsNullOrEmpty(info.Filename))
                {
                    state.Matches.Add(info);
                }
            }

            return state;
        }

        /// <summary>
        /// Read the contents of a zip entry
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        private string ReadFile(ZipArchiveEntry entry)
        {
            var reader = new StreamReader(entry.Open());
            var queryText = reader.ReadToEnd();
            reader.Close();
            return queryText;
        }

        /// <summary>
        /// Get the contents of a zip entry if it contains a match
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="entry"></param>
        /// <returns></returns>
        private string MatchContext(string filter, ZipArchiveEntry entry)
        {
            var reader = new StreamReader(entry.Open());
            var queryText = reader.ReadToEnd();
            reader.Close();
            if (queryText.ToLower().Contains(filter))
            {
                return queryText;
            }
            return string.Empty;
        }

        /// <summary>
        /// Get the lines from a zip entry that match
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="entry"></param>
        /// <returns></returns>
        private string MatchContextByLine(string filter, ZipArchiveEntry entry)
        {
            var contexts = new List<string>();
            var reader = new StreamReader(entry.Open());
            var line = reader.ReadLine();
            var header = line;
            while (line != null)
            {
                if (line.ToLower().Contains(filter))
                {
                    contexts.Add(line);
                }
                line = reader.ReadLine();
            }
            reader.Close();
            if (contexts.Count > 0)
            {
                contexts.Insert(0, header);
                return string.Join(Environment.NewLine, contexts.ToArray());
            }
            return string.Empty;
        }

    }
}
