﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAnt
{
    public class AuditSearchState
    {
        /// <summary>
        /// The search options applicable to this search
        /// </summary>
        public AuditSearchOptions Options { get; set; }

        /// <summary>
        /// Stores the list of files being searched
        /// </summary>
        public string[] Files { get; set; }

        /// <summary>
        /// List of search matches
        /// </summary>
        public List<AuditSearchInfo> Matches { get; set; }

        /// <summary>
        /// The last position searched in the file list
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// The totals number of files that can be searched
        /// </summary>
        public int Total
        {
            get
            {
                return Files.Length;
            }
        }

        /// <summary>
        /// Indicates if the search has completed
        /// </summary>
        public bool Complete
        {
            get
            {
                return Position == Total - 1;
            }
        }

        public AuditSearchState()
        {
            Matches = new List<AuditSearchInfo>();
            Position = -1;
        }
    }
}
