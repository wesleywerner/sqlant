﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SQLAnt.UpdateChecker
{
    public class UpdateChecker
    {
        /// <summary>
        /// Should we check for an update based on the given values.
        /// </summary>
        /// <param name="lastChecked"></param>
        /// <param name="hoursBeforeCheck"></param>
        /// <returns></returns>
        public bool ShouldCheckForUpdates(DateTime current, DateTime lastChecked, int hoursBeforeCheck)
        {
            // Disable update checks
            if (hoursBeforeCheck == 0)
            {
                return false;
            }
            // Use current time if none is saved
            if (lastChecked == DateTime.MinValue)
            {
                lastChecked = current;
                Program.rc["update last checked"] = lastChecked.ToString();
                return false;
            }
            var hoursSinceLastCheck = (current - lastChecked).TotalHours;
            return (hoursSinceLastCheck >= hoursBeforeCheck);
        }

        /// <summary>
        /// Should we check for an update based on saved configuration values.
        /// </summary>
        /// <returns></returns>
        public bool ShouldCheckForUpdates()
        {
            var lastChecked = Program.rc.GetDate("update last checked", DateTime.MinValue);
            var hoursBeforeCheck = Program.rc.GetInt("update check interval", 24);
            return ShouldCheckForUpdates(DateTime.Now, lastChecked, hoursBeforeCheck);
        }

        /// <summary>
        /// Download the online version information.
        /// </summary>
        /// <returns></returns>
        public UpdateCheckResult GetOnlineVersionInfo()
        {
            var versionFile = string.Empty;
            var webRequest = WebRequest.Create(@"https://bitbucket.org/wesleywerner/sqlant/downloads/Versions.txt");
            using (var response = webRequest.GetResponse())
            using (var content = response.GetResponseStream())
            using (var reader = new StreamReader(content))
            {
                versionFile = reader.ReadToEnd();
            }

            var result = new UpdateCheckResult()
            {
                AvailableBetaVersion = ExtractInfoLine("BETA VERSION", versionFile),
                AvailableStableVersion = ExtractInfoLine("STABLE VERSION", versionFile),
                BetaUpdateURL = ExtractInfoLine("BETA URL", versionFile),
                StableUpdateURL = ExtractInfoLine("STABLE URL", versionFile),
                CurrentVersion = Program.SQLAntVersion(),
                CheckStableBuilds = Program.rc.GetBool("stable updates", true)
            };

            return result;
        }

        /// <summary>
        /// Extract version info by item key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="versionsFile"></param>
        /// <returns></returns>
        public string ExtractInfoLine(string key, string versionsFile)
        {
            var result = string.Empty;
            var io = new StringReader(versionsFile);
            var line = io.ReadLine();
            do
            {
                if (line != null)
                {
                    if (line.StartsWith(key))
                    {
                        result = line.Substring(key.Length + 1).Trim();
                    }
                }
                line = io.ReadLine();
            } while (line != null);
            return result;
        }

    }
}
