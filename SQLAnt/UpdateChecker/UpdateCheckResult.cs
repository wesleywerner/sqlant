﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAnt.UpdateChecker
{
    public class UpdateCheckResult
    {
        public string CurrentVersion { get; set; }
        public string AvailableStableVersion { get; set; }
        public string AvailableBetaVersion { get; set; }
        public string StableUpdateURL { get; set; }
        public string BetaUpdateURL { get; set; }
        public bool CheckStableBuilds { get; set; }

        /// <summary>
        /// Determine if an update is available.
        /// </summary>
        /// <param name="versionsFile"></param>
        /// <returns></returns>
        public bool UpdateAvailable()
        {
            var C = new Version(CurrentVersion);
            var S = new Version(AvailableStableVersion);
            var B = new Version(AvailableBetaVersion);

            if (CheckStableBuilds)
            {
                return (C.Major != S.Major) || (C.Minor != S.Minor);
            }
            else
            {
                return (C.Build != B.Build) || (C.Major != S.Major) || (C.Minor != S.Minor);
            }
        }


        /// <summary>
        /// Recommends the update url based on stable or beta.
        /// </summary>
        /// <returns></returns>
        public string SuggestedUpdateUrl()
        {
            if (CheckStableBuilds)
            {
                return StableUpdateURL;
            }
            else
            {
                return BetaUpdateURL;
            }
        }

    }
}
