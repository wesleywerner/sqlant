﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAnt.Cleaner
{
    public class CleanerAnalysis
    {

        /// <summary>
        /// Clean options
        /// </summary>
        public CleanerOptions Options { get; set; }

        /// <summary>
        /// Results of files gained from the analysis that match the criteria
        /// </summary>
        public List<FileInfo> Results { get; set; }

        /// <summary>
        /// Creates a new cleaner instance
        /// </summary>
        /// <param name="options"></param>
        public CleanerAnalysis(CleanerOptions options)
        {
            Options = options;
        }

        /// <summary>
        /// Analyse the preconfigured paths against the criteria in the options
        /// </summary>
        public void StartAnalysis()
        {
            // Default the file remove method
            if (Options.Mode == 0) Options.Mode = Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin;
            Results = new List<FileInfo>();
            DateTime minimumDate = DateTime.Today.Subtract(new TimeSpan(Options.Age, 0, 0, 0));
            foreach (var path in Options.Paths)
            {
                var dirfiles = Directory.GetFiles(Utilities.Storage(path, ""));
                foreach (var filename in dirfiles)
                {
                    var info = new FileInfo(filename);
                    if (info.CreationTime < minimumDate)
                    {
                        Results.Add(info);
                    }
                }
            }
        }

        /// <summary>
        /// Cleans the next file in the Results list
        /// </summary>
        /// <returns>true while there are more files to clean</returns>
        public bool CleanNext()
        {
            if (Results == null) throw new Exception("No analysis results to action on. Did you run the analysis first?");
            if (Results.Count == 0) return false;
            Microsoft.VisualBasic.FileIO.FileSystem.DeleteFile(Results[0].FullName,
                    Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs,
                    Options.Mode);
            Results.RemoveAt(0);
            return Results.Count > 0;
        }

    }
}
