﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLAnt
{
    static class Program
    {
        /// <summary>
        /// Provides runtime configurations
        /// </summary>
        public static Config rc;
        public static Config connections;
        public static Config connectionColors;
        public static Config codeSnippets;
        public static List<string> recentFiles;
        public static int autosavecounter = 0;


        /// <summary>
        /// Read all configuration files from storage.
        /// </summary>
        public static void ReadConfigurationFiles()
        {
            // Initialize the storage area
            Utilities.InitStoragePath();

            // Read the configuration file
            rc = new Config(Utilities.ReadINIFile(Utilities.Storage("", "SQLAntrc")));

            if (!rc.ContainsKey("recentpath"))
            {
                rc["recentpath"] = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            }

            Utilities.LogAction("Loading database connections");

            // Read the connections file
            connections = new Config(Utilities.ReadINIFile(Utilities.Storage("", "connections")));

            //// create a default file
            //if (connections.Keys.Count == 0)
            //{
            //    connections["# SERVER CONFIGURATIONS"] = "";
            //    connections["Localhost"] = "server=localhost;Integrated Security=SSPI;Connection Timeout=5;";
            //    Utilities.WriteINIFile(connections, Utilities.Storage("", "connections"));
            //}

            // Read connection colors configuration
            connectionColors = new Config(Utilities.ReadINIFile(Utilities.Storage("", "connectioncolors")));

            // Read recent files list
            recentFiles = Utilities.ReadListFile(Utilities.Storage("", "recentfiles"));

        }


        /// <summary>
        /// Writes the runtime configuration to file
        /// </summary>
        public static void WriteConfiguration()
        {
            Utilities.WriteINIFile(rc, Utilities.Storage("", "SQLAntrc"));
        }


        /// <summary>
        /// Read saved snippets
        /// </summary>
        public static  void ReadSnippets()
        {
            try
            {
                Utilities.LogAction("Reading snippets");
                string snippath = Utilities.Storage("snippets", "");
                var snippetFiles = System.IO.Directory.GetFiles(snippath, "*.sql", System.IO.SearchOption.AllDirectories);
                codeSnippets = new Config(new Dictionary<string, string>());
                foreach (string snipfile in snippetFiles)
                {
                    string key = System.IO.Path.GetFileNameWithoutExtension(snipfile).Replace(" ", "-");
                    codeSnippets[key] = System.IO.File.ReadAllText(snipfile);
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Write snippets to file
        /// </summary>
        public static void SaveSnippets()
        {
            try
            {
                Utilities.WriteINIFile(codeSnippets, Utilities.Storage("", "snippets"));
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        public static string SQLAntVersion()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            return fvi.ProductVersion;
        }


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                ReadConfigurationFiles();
                ReadSnippets();
                Application.Run(new StudioWindow());
                Utilities.LogAction("Shutdown initiated, writing configuration to file");
                WriteConfiguration();
                Utilities.LogAction("Goodbye");
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
                MessageBox.Show(ExceptionHelper.Clean(ex));
            }
        }

        public static void DoubleBuffered(this DataGridView dgv, bool setting)
        {
            Type dgvType = dgv.GetType();
            PropertyInfo pi = dgvType.GetProperty("DoubleBuffered",
                  BindingFlags.Instance | BindingFlags.NonPublic);
            pi.SetValue(dgv, setting, null);
        }

    }
}
