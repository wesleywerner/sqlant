﻿namespace SQLAnt
{
    partial class ConnectionsEditorWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ConnectionList = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ConnectionColorButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.PasswordText = new System.Windows.Forms.TextBox();
            this.UserText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SQLSecurityOption = new System.Windows.Forms.RadioButton();
            this.IntegratedOption = new System.Windows.Forms.RadioButton();
            this.ServerText = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.NewConnectionButton = new System.Windows.Forms.ToolStripButton();
            this.RemoveConnectionButton = new System.Windows.Forms.ToolStripButton();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ConnectionList
            // 
            this.ConnectionList.FullRowSelect = true;
            this.ConnectionList.HideSelection = false;
            this.ConnectionList.Location = new System.Drawing.Point(12, 28);
            this.ConnectionList.MultiSelect = false;
            this.ConnectionList.Name = "ConnectionList";
            this.ConnectionList.Size = new System.Drawing.Size(167, 269);
            this.ConnectionList.TabIndex = 0;
            this.ConnectionList.UseCompatibleStateImageBehavior = false;
            this.ConnectionList.View = System.Windows.Forms.View.List;
            this.ConnectionList.SelectedIndexChanged += new System.EventHandler(this.ConnectionList_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ConnectionColorButton);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.SQLSecurityOption);
            this.groupBox1.Controls.Add(this.IntegratedOption);
            this.groupBox1.Controls.Add(this.ServerText);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(185, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(342, 269);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection Details";
            // 
            // ConnectionColorButton
            // 
            this.ConnectionColorButton.BackColor = System.Drawing.Color.LightGreen;
            this.ConnectionColorButton.Location = new System.Drawing.Point(118, 215);
            this.ConnectionColorButton.Name = "ConnectionColorButton";
            this.ConnectionColorButton.Size = new System.Drawing.Size(75, 23);
            this.ConnectionColorButton.TabIndex = 11;
            this.ConnectionColorButton.UseVisualStyleBackColor = false;
            this.ConnectionColorButton.Click += new System.EventHandler(this.ConnectionColorButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Connection Color";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.PasswordText);
            this.panel1.Controls.Add(this.UserText);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Enabled = false;
            this.panel1.Location = new System.Drawing.Point(48, 123);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(288, 80);
            this.panel1.TabIndex = 8;
            // 
            // PasswordText
            // 
            this.PasswordText.Location = new System.Drawing.Point(78, 38);
            this.PasswordText.MaxLength = 200;
            this.PasswordText.Name = "PasswordText";
            this.PasswordText.Size = new System.Drawing.Size(181, 20);
            this.PasswordText.TabIndex = 5;
            this.PasswordText.TextChanged += new System.EventHandler(this.PasswordText_TextChanged);
            // 
            // UserText
            // 
            this.UserText.Location = new System.Drawing.Point(78, 12);
            this.UserText.MaxLength = 200;
            this.UserText.Name = "UserText";
            this.UserText.Size = new System.Drawing.Size(181, 20);
            this.UserText.TabIndex = 3;
            this.UserText.TextChanged += new System.EventHandler(this.UserText_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Password";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "User";
            // 
            // SQLSecurityOption
            // 
            this.SQLSecurityOption.AutoSize = true;
            this.SQLSecurityOption.Location = new System.Drawing.Point(27, 100);
            this.SQLSecurityOption.Name = "SQLSecurityOption";
            this.SQLSecurityOption.Size = new System.Drawing.Size(87, 17);
            this.SQLSecurityOption.TabIndex = 7;
            this.SQLSecurityOption.Text = "SQL Security";
            this.SQLSecurityOption.UseVisualStyleBackColor = true;
            this.SQLSecurityOption.CheckedChanged += new System.EventHandler(this.SQLSecurityOption_CheckedChanged);
            // 
            // IntegratedOption
            // 
            this.IntegratedOption.AutoSize = true;
            this.IntegratedOption.Checked = true;
            this.IntegratedOption.Location = new System.Drawing.Point(27, 77);
            this.IntegratedOption.Name = "IntegratedOption";
            this.IntegratedOption.Size = new System.Drawing.Size(114, 17);
            this.IntegratedOption.TabIndex = 6;
            this.IntegratedOption.TabStop = true;
            this.IntegratedOption.Text = "Integrated Security";
            this.IntegratedOption.UseVisualStyleBackColor = true;
            this.IntegratedOption.CheckedChanged += new System.EventHandler(this.IntegratedOption_CheckedChanged);
            // 
            // ServerText
            // 
            this.ServerText.Location = new System.Drawing.Point(106, 37);
            this.ServerText.MaxLength = 255;
            this.ServerText.Name = "ServerText";
            this.ServerText.ReadOnly = true;
            this.ServerText.Size = new System.Drawing.Size(230, 20);
            this.ServerText.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "SERVER";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewConnectionButton,
            this.RemoveConnectionButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(539, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // NewConnectionButton
            // 
            this.NewConnectionButton.Image = global::SQLAnt.Properties.Resources.document_new;
            this.NewConnectionButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewConnectionButton.Name = "NewConnectionButton";
            this.NewConnectionButton.Size = new System.Drawing.Size(51, 22);
            this.NewConnectionButton.Text = "New";
            this.NewConnectionButton.Click += new System.EventHandler(this.NewConnectionButton_Click);
            // 
            // RemoveConnectionButton
            // 
            this.RemoveConnectionButton.Image = global::SQLAnt.Properties.Resources.edit_clear;
            this.RemoveConnectionButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.RemoveConnectionButton.Name = "RemoveConnectionButton";
            this.RemoveConnectionButton.Size = new System.Drawing.Size(70, 22);
            this.RemoveConnectionButton.Text = "Remove";
            this.RemoveConnectionButton.Click += new System.EventHandler(this.RemoveConnectionButton_Click);
            // 
            // ConnectionsEditorWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 308);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ConnectionList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ConnectionsEditorWindow";
            this.Text = "Connections";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ConnectionsEditorWindow_FormClosing);
            this.Load += new System.EventHandler(this.ConnectionsEditorWindow_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView ConnectionList;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox PasswordText;
        private System.Windows.Forms.TextBox UserText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton SQLSecurityOption;
        private System.Windows.Forms.RadioButton IntegratedOption;
        private System.Windows.Forms.TextBox ServerText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton NewConnectionButton;
        private System.Windows.Forms.ToolStripButton RemoveConnectionButton;
        private System.Windows.Forms.Button ConnectionColorButton;
        private System.Windows.Forms.Label label4;
    }
}