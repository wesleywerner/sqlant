﻿namespace SQLAnt
{
    partial class ScriptObjectsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScriptObjectsWindow));
            this.ScriptThread = new System.ComponentModel.BackgroundWorker();
            this.ScriptSplitContainer = new System.Windows.Forms.SplitContainer();
            this.ObjectTree = new System.Windows.Forms.TreeView();
            this.ObjectTreeIcons = new System.Windows.Forms.ImageList(this.components);
            this.ObjectFilterPanel = new System.Windows.Forms.Panel();
            this.ObjectTreeFilterInput = new System.Windows.Forms.TextBox();
            this.ObjectFilterLabel = new System.Windows.Forms.Label();
            this.SelectedItemsList = new System.Windows.Forms.ListView();
            this.ObjectTreeFilterTimer = new System.Windows.Forms.Timer(this.components);
            this.SelectedObjectsDisplayTimer = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.GenerateScriptButton = new System.Windows.Forms.ToolStripButton();
            this.ScriptSchemaCheck = new System.Windows.Forms.ToolStripButton();
            this.ScriptDataCheck = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.BusyProgress = new System.Windows.Forms.ToolStripProgressBar();
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.ScriptSplitContainer)).BeginInit();
            this.ScriptSplitContainer.Panel1.SuspendLayout();
            this.ScriptSplitContainer.Panel2.SuspendLayout();
            this.ScriptSplitContainer.SuspendLayout();
            this.ObjectFilterPanel.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(6, 28);
            // 
            // ScriptThread
            // 
            this.ScriptThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ScriptThread_DoWork);
            this.ScriptThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ScriptThread_RunWorkerCompleted);
            // 
            // ScriptSplitContainer
            // 
            this.ScriptSplitContainer.Location = new System.Drawing.Point(26, 41);
            this.ScriptSplitContainer.Name = "ScriptSplitContainer";
            // 
            // ScriptSplitContainer.Panel1
            // 
            this.ScriptSplitContainer.Panel1.Controls.Add(this.ObjectTree);
            this.ScriptSplitContainer.Panel1.Controls.Add(this.ObjectFilterPanel);
            // 
            // ScriptSplitContainer.Panel2
            // 
            this.ScriptSplitContainer.Panel2.Controls.Add(this.SelectedItemsList);
            this.ScriptSplitContainer.Size = new System.Drawing.Size(500, 169);
            this.ScriptSplitContainer.SplitterDistance = 260;
            this.ScriptSplitContainer.TabIndex = 4;
            // 
            // ObjectTree
            // 
            this.ObjectTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ObjectTree.CheckBoxes = true;
            this.ObjectTree.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ObjectTree.ImageIndex = 0;
            this.ObjectTree.ImageList = this.ObjectTreeIcons;
            this.ObjectTree.Location = new System.Drawing.Point(17, 52);
            this.ObjectTree.Name = "ObjectTree";
            this.ObjectTree.SelectedImageIndex = 0;
            this.ObjectTree.Size = new System.Drawing.Size(192, 89);
            this.ObjectTree.TabIndex = 2;
            this.ObjectTree.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.ObjectTree_AfterCheck);
            // 
            // ObjectTreeIcons
            // 
            this.ObjectTreeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ObjectTreeIcons.ImageStream")));
            this.ObjectTreeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ObjectTreeIcons.Images.SetKeyName(0, "treedefault.png");
            this.ObjectTreeIcons.Images.SetKeyName(1, "table.png");
            this.ObjectTreeIcons.Images.SetKeyName(2, "view.png");
            this.ObjectTreeIcons.Images.SetKeyName(3, "procedure.png");
            // 
            // ObjectFilterPanel
            // 
            this.ObjectFilterPanel.Controls.Add(this.ObjectTreeFilterInput);
            this.ObjectFilterPanel.Controls.Add(this.ObjectFilterLabel);
            this.ObjectFilterPanel.Location = new System.Drawing.Point(17, 16);
            this.ObjectFilterPanel.Name = "ObjectFilterPanel";
            this.ObjectFilterPanel.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.ObjectFilterPanel.Size = new System.Drawing.Size(192, 30);
            this.ObjectFilterPanel.TabIndex = 1;
            // 
            // ObjectTreeFilterInput
            // 
            this.ObjectTreeFilterInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ObjectTreeFilterInput.ForeColor = System.Drawing.Color.Blue;
            this.ObjectTreeFilterInput.Location = new System.Drawing.Point(86, 0);
            this.ObjectTreeFilterInput.Name = "ObjectTreeFilterInput";
            this.ObjectTreeFilterInput.Size = new System.Drawing.Size(106, 22);
            this.ObjectTreeFilterInput.TabIndex = 1;
            this.ObjectTreeFilterInput.TextChanged += new System.EventHandler(this.ObjectTreeFilterInput_TextChanged);
            // 
            // ObjectFilterLabel
            // 
            this.ObjectFilterLabel.AutoSize = true;
            this.ObjectFilterLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.ObjectFilterLabel.Location = new System.Drawing.Point(0, 0);
            this.ObjectFilterLabel.Name = "ObjectFilterLabel";
            this.ObjectFilterLabel.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.ObjectFilterLabel.Size = new System.Drawing.Size(35, 16);
            this.ObjectFilterLabel.TabIndex = 0;
            this.ObjectFilterLabel.Text = "Filter: ";
            // 
            // SelectedItemsList
            // 
            this.SelectedItemsList.CheckBoxes = true;
            this.SelectedItemsList.Location = new System.Drawing.Point(55, 60);
            this.SelectedItemsList.Name = "SelectedItemsList";
            this.SelectedItemsList.Size = new System.Drawing.Size(121, 97);
            this.SelectedItemsList.TabIndex = 0;
            this.SelectedItemsList.UseCompatibleStateImageBehavior = false;
            this.SelectedItemsList.View = System.Windows.Forms.View.Details;
            this.SelectedItemsList.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.SelectedItemsList_ItemChecked);
            // 
            // ObjectTreeFilterTimer
            // 
            this.ObjectTreeFilterTimer.Interval = 450;
            this.ObjectTreeFilterTimer.Tick += new System.EventHandler(this.ObjectTreeFilterTimer_Tick);
            // 
            // SelectedObjectsDisplayTimer
            // 
            this.SelectedObjectsDisplayTimer.Interval = 250;
            this.SelectedObjectsDisplayTimer.Tick += new System.EventHandler(this.SelectedObjectsDisplayTimer_Tick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.AutoSize = false;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GenerateScriptButton,
            toolStripSeparator1,
            this.ScriptSchemaCheck,
            this.ScriptDataCheck});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 1, 4);
            this.toolStrip1.Size = new System.Drawing.Size(758, 32);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // GenerateScriptButton
            // 
            this.GenerateScriptButton.Image = ((System.Drawing.Image)(resources.GetObject("GenerateScriptButton.Image")));
            this.GenerateScriptButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.GenerateScriptButton.Name = "GenerateScriptButton";
            this.GenerateScriptButton.Size = new System.Drawing.Size(107, 25);
            this.GenerateScriptButton.Text = "Generate Script";
            this.GenerateScriptButton.Click += new System.EventHandler(this.GenerateScriptButton_Click);
            // 
            // ScriptSchemaCheck
            // 
            this.ScriptSchemaCheck.Checked = true;
            this.ScriptSchemaCheck.CheckOnClick = true;
            this.ScriptSchemaCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ScriptSchemaCheck.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ScriptSchemaCheck.Image = ((System.Drawing.Image)(resources.GetObject("ScriptSchemaCheck.Image")));
            this.ScriptSchemaCheck.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ScriptSchemaCheck.Name = "ScriptSchemaCheck";
            this.ScriptSchemaCheck.Size = new System.Drawing.Size(86, 25);
            this.ScriptSchemaCheck.Text = "Script Schema";
            // 
            // ScriptDataCheck
            // 
            this.ScriptDataCheck.Checked = true;
            this.ScriptDataCheck.CheckOnClick = true;
            this.ScriptDataCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ScriptDataCheck.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ScriptDataCheck.Image = ((System.Drawing.Image)(resources.GetObject("ScriptDataCheck.Image")));
            this.ScriptDataCheck.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ScriptDataCheck.Name = "ScriptDataCheck";
            this.ScriptDataCheck.Size = new System.Drawing.Size(68, 25);
            this.ScriptDataCheck.Text = "Script Data";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BusyProgress});
            this.statusStrip1.Location = new System.Drawing.Point(0, 411);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(758, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // BusyProgress
            // 
            this.BusyProgress.Name = "BusyProgress";
            this.BusyProgress.Size = new System.Drawing.Size(100, 16);
            // 
            // ScriptObjectsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(758, 433);
            this.Controls.Add(this.ScriptSplitContainer);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "ScriptObjectsWindow";
            this.Text = "Script Objects";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScriptTableWindow_FormClosing);
            this.Load += new System.EventHandler(this.ScriptTableWindow_Load);
            this.ScriptSplitContainer.Panel1.ResumeLayout(false);
            this.ScriptSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ScriptSplitContainer)).EndInit();
            this.ScriptSplitContainer.ResumeLayout(false);
            this.ObjectFilterPanel.ResumeLayout(false);
            this.ObjectFilterPanel.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.ComponentModel.BackgroundWorker ScriptThread;
        private System.Windows.Forms.SplitContainer ScriptSplitContainer;
        private System.Windows.Forms.Panel ObjectFilterPanel;
        private System.Windows.Forms.TextBox ObjectTreeFilterInput;
        private System.Windows.Forms.Label ObjectFilterLabel;
        private System.Windows.Forms.TreeView ObjectTree;
        private System.Windows.Forms.Timer ObjectTreeFilterTimer;
        private System.Windows.Forms.ImageList ObjectTreeIcons;
        private System.Windows.Forms.ListView SelectedItemsList;
        private System.Windows.Forms.Timer SelectedObjectsDisplayTimer;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton GenerateScriptButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar BusyProgress;
        private System.Windows.Forms.ToolStripButton ScriptSchemaCheck;
        private System.Windows.Forms.ToolStripButton ScriptDataCheck;
    }
}