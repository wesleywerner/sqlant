﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLAnt.Windows
{
    public partial class AutoCleanerWindow : Form
    {
        Cleaner.CleanerAnalysis cleaner { get; set; }
        public AutoCleanerWindow(Cleaner.CleanerAnalysis cleaner)
        {
            InitializeComponent();
            this.cleaner = cleaner;
            this.Icon = SQLAnt.Properties.Resources.ant;
        }

        /// <summary>
        /// User must confirm before cleaning
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmCheck_CheckedChanged(object sender, EventArgs e)
        {
            DeleteButton.Enabled = ConfirmCheck.Checked;
        }

        /// <summary>
        /// Start the cleaning
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteButton_Click(object sender, EventArgs e)
        {
            // Remove unchecked categories
            foreach (ListViewItem row in FilesList.Items)
            {
                if (!row.Checked)
                {
                    string rowpath = row.Tag.ToString();
                    cleaner.Results.RemoveAll(m => m.DirectoryName == rowpath);
                }
            }

            if (TrashCheck.Checked)
            {
                cleaner.Options.Mode = Microsoft.VisualBasic.FileIO.RecycleOption.SendToRecycleBin;
            }
            else
            {
                cleaner.Options.Mode = Microsoft.VisualBasic.FileIO.RecycleOption.DeletePermanently;
            }

            this.UseWaitCursor = true;
            FilesList.Enabled = false;
            BusyIndicator.Visible = true;
            ConfirmCheck.Visible = false;
            TrashCheck.Visible = false;
            DeleteButton.Visible = false;
            BusyIndicator.Value = 0;
            CleanupWorker.RunWorkerAsync();
        }

        /// <summary>
        /// Display a summary of the files to clean
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoCleanerWindow_Load(object sender, EventArgs e)
        {
            FilesList.Columns.Add("Category");
            FilesList.Columns.Add("Files");
            FilesList.Columns.Add("Size");

            foreach (var path in cleaner.Options.Paths)
            {
                var pathitems = cleaner.Results.Where(n => n.DirectoryName == path);
                long pathsize = pathitems.Select(n => n.Length).Sum();
                var row = new ListViewItem(Path.GetFileName(path));
                row.SubItems.Add(pathitems.Count().ToString());
                row.SubItems.Add(Formatters.FormatBytes(pathsize));
                row.Checked = true;
                row.Tag = path;
                FilesList.Items.Add(row);
            }
            FilesList.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
        }

        /// <summary>
        /// Clean the files from the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CleanupWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int max = cleaner.Results.Count;
            while (true)
            {
                if (CleanupWorker.CancellationPending) break;
                if (!cleaner.CleanNext()) break;
                int value = max - cleaner.Results.Count;
                decimal frac = value / (decimal)max;
                CleanupWorker.ReportProgress((int)(frac * 100));
            }
        }

        /// <summary>
        /// Cleaner completed or cancelled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CleanupWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled)
            {
                Program.rc["clean checked"] = DateTime.Today.ToLongDateString();
            }
            this.Close();
        }

        /// <summary>
        /// Prompt to cancel on close while busy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoCleanerWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing && CleanupWorker.IsBusy)
            {
                e.Cancel = true;
                var res = MessageBox.Show("Abort the cleanup?", "Cleanup", MessageBoxButtons.YesNo);
                if (res == DialogResult.Yes) CleanupWorker.CancelAsync();
            }
        }

        /// <summary>
        /// Report cleanup progress
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CleanupWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BusyIndicator.Value = e.ProgressPercentage;
        }
    }
}
