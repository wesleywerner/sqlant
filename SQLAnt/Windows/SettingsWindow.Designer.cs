﻿namespace SQLAnt
{
    partial class SettingsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.PictureBox pictureBox1;
            System.Windows.Forms.PictureBox pictureBox2;
            System.Windows.Forms.PictureBox pictureBox3;
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.EditorTabPage = new System.Windows.Forms.TabPage();
            this.RememberOpenFilesCheck = new System.Windows.Forms.CheckBox();
            this.AutoCapitalizeKeywordsCheck = new System.Windows.Forms.CheckBox();
            this.AutoDetectSchemaChangesCheck = new System.Windows.Forms.CheckBox();
            this.AutoSaveOnCloseCheck = new System.Windows.Forms.CheckBox();
            this.AutoQualifySchemaNamesCheck = new System.Windows.Forms.CheckBox();
            this.AuditTabPage = new System.Windows.Forms.TabPage();
            this.AuditBrowse = new System.Windows.Forms.Button();
            this.AuditFormat = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AuditingEnabled = new System.Windows.Forms.CheckBox();
            this.CleanTabPage = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.RunCleanerCheckButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.CleanAgeInput = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.CleanCheckInput = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.UpdatesTabPage = new System.Windows.Forms.TabPage();
            this.StableUpdatesCheck = new System.Windows.Forms.CheckBox();
            this.UpdatesDisabledLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.UpdateIntervalInput = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CancelButton = new System.Windows.Forms.Button();
            this.SaveButton = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.RowcountLimitInput = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            pictureBox1 = new System.Windows.Forms.PictureBox();
            pictureBox2 = new System.Windows.Forms.PictureBox();
            pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pictureBox2)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.EditorTabPage.SuspendLayout();
            this.AuditTabPage.SuspendLayout();
            this.CleanTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CleanAgeInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CleanCheckInput)).BeginInit();
            this.UpdatesTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateIntervalInput)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RowcountLimitInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            pictureBox1.Image = global::SQLAnt.Properties.Resources.dialog_information;
            pictureBox1.Location = new System.Drawing.Point(27, 26);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new System.Drawing.Size(16, 16);
            pictureBox1.TabIndex = 4;
            pictureBox1.TabStop = false;
            this.toolTip1.SetToolTip(pictureBox1, "Automatically insert the SCHEMA part of [schema].[object]");
            // 
            // pictureBox2
            // 
            pictureBox2.Image = global::SQLAnt.Properties.Resources.dialog_information;
            pictureBox2.Location = new System.Drawing.Point(27, 49);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new System.Drawing.Size(16, 16);
            pictureBox2.TabIndex = 5;
            pictureBox2.TabStop = false;
            this.toolTip1.SetToolTip(pictureBox2, "Queries with known filenames are saved when you exit, and those without filenames" +
        " are auto-saved and re-opened on next program start.");
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.EditorTabPage);
            this.tabControl1.Controls.Add(this.AuditTabPage);
            this.tabControl1.Controls.Add(this.CleanTabPage);
            this.tabControl1.Controls.Add(this.UpdatesTabPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(558, 254);
            this.tabControl1.TabIndex = 0;
            // 
            // EditorTabPage
            // 
            this.EditorTabPage.Controls.Add(pictureBox3);
            this.EditorTabPage.Controls.Add(this.RowcountLimitInput);
            this.EditorTabPage.Controls.Add(this.label11);
            this.EditorTabPage.Controls.Add(this.RememberOpenFilesCheck);
            this.EditorTabPage.Controls.Add(pictureBox2);
            this.EditorTabPage.Controls.Add(pictureBox1);
            this.EditorTabPage.Controls.Add(this.AutoCapitalizeKeywordsCheck);
            this.EditorTabPage.Controls.Add(this.AutoDetectSchemaChangesCheck);
            this.EditorTabPage.Controls.Add(this.AutoSaveOnCloseCheck);
            this.EditorTabPage.Controls.Add(this.AutoQualifySchemaNamesCheck);
            this.EditorTabPage.Location = new System.Drawing.Point(4, 22);
            this.EditorTabPage.Name = "EditorTabPage";
            this.EditorTabPage.Size = new System.Drawing.Size(550, 228);
            this.EditorTabPage.TabIndex = 1;
            this.EditorTabPage.Text = "Editor";
            this.EditorTabPage.ToolTipText = "Inserts the schema part of [schema].[object] if it is not the default [dbo] name." +
    "";
            this.EditorTabPage.UseVisualStyleBackColor = true;
            // 
            // RememberOpenFilesCheck
            // 
            this.RememberOpenFilesCheck.AutoSize = true;
            this.RememberOpenFilesCheck.Location = new System.Drawing.Point(49, 118);
            this.RememberOpenFilesCheck.Name = "RememberOpenFilesCheck";
            this.RememberOpenFilesCheck.Size = new System.Drawing.Size(175, 17);
            this.RememberOpenFilesCheck.TabIndex = 6;
            this.RememberOpenFilesCheck.Text = "Remember my open files on exit";
            this.RememberOpenFilesCheck.UseVisualStyleBackColor = true;
            // 
            // AutoCapitalizeKeywordsCheck
            // 
            this.AutoCapitalizeKeywordsCheck.AutoSize = true;
            this.AutoCapitalizeKeywordsCheck.Location = new System.Drawing.Point(49, 95);
            this.AutoCapitalizeKeywordsCheck.Name = "AutoCapitalizeKeywordsCheck";
            this.AutoCapitalizeKeywordsCheck.Size = new System.Drawing.Size(167, 17);
            this.AutoCapitalizeKeywordsCheck.TabIndex = 3;
            this.AutoCapitalizeKeywordsCheck.Text = "Auto capitalize SQL keywords";
            this.AutoCapitalizeKeywordsCheck.UseVisualStyleBackColor = true;
            // 
            // AutoDetectSchemaChangesCheck
            // 
            this.AutoDetectSchemaChangesCheck.AutoSize = true;
            this.AutoDetectSchemaChangesCheck.Location = new System.Drawing.Point(49, 72);
            this.AutoDetectSchemaChangesCheck.Name = "AutoDetectSchemaChangesCheck";
            this.AutoDetectSchemaChangesCheck.Size = new System.Drawing.Size(298, 17);
            this.AutoDetectSchemaChangesCheck.TabIndex = 2;
            this.AutoDetectSchemaChangesCheck.Text = "Auto refresh database objects if I detect schema changes";
            this.AutoDetectSchemaChangesCheck.UseVisualStyleBackColor = true;
            // 
            // AutoSaveOnCloseCheck
            // 
            this.AutoSaveOnCloseCheck.AutoSize = true;
            this.AutoSaveOnCloseCheck.Location = new System.Drawing.Point(49, 49);
            this.AutoSaveOnCloseCheck.Name = "AutoSaveOnCloseCheck";
            this.AutoSaveOnCloseCheck.Size = new System.Drawing.Size(145, 17);
            this.AutoSaveOnCloseCheck.TabIndex = 1;
            this.AutoSaveOnCloseCheck.Text = "Auto save queries on exit";
            this.AutoSaveOnCloseCheck.UseVisualStyleBackColor = true;
            // 
            // AutoQualifySchemaNamesCheck
            // 
            this.AutoQualifySchemaNamesCheck.AutoSize = true;
            this.AutoQualifySchemaNamesCheck.Location = new System.Drawing.Point(49, 26);
            this.AutoQualifySchemaNamesCheck.Name = "AutoQualifySchemaNamesCheck";
            this.AutoQualifySchemaNamesCheck.Size = new System.Drawing.Size(160, 17);
            this.AutoQualifySchemaNamesCheck.TabIndex = 0;
            this.AutoQualifySchemaNamesCheck.Text = "Auto insert schema qualifiers";
            this.AutoQualifySchemaNamesCheck.UseVisualStyleBackColor = true;
            // 
            // AuditTabPage
            // 
            this.AuditTabPage.Controls.Add(this.AuditBrowse);
            this.AuditTabPage.Controls.Add(this.AuditFormat);
            this.AuditTabPage.Controls.Add(this.label2);
            this.AuditTabPage.Controls.Add(this.label1);
            this.AuditTabPage.Controls.Add(this.AuditingEnabled);
            this.AuditTabPage.Location = new System.Drawing.Point(4, 22);
            this.AuditTabPage.Name = "AuditTabPage";
            this.AuditTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.AuditTabPage.Size = new System.Drawing.Size(550, 228);
            this.AuditTabPage.TabIndex = 0;
            this.AuditTabPage.Text = "Query Auditing";
            this.AuditTabPage.UseVisualStyleBackColor = true;
            // 
            // AuditBrowse
            // 
            this.AuditBrowse.BackColor = System.Drawing.Color.White;
            this.AuditBrowse.Location = new System.Drawing.Point(24, 161);
            this.AuditBrowse.Name = "AuditBrowse";
            this.AuditBrowse.Size = new System.Drawing.Size(121, 32);
            this.AuditBrowse.TabIndex = 4;
            this.AuditBrowse.Text = "Browse Audits";
            this.AuditBrowse.UseVisualStyleBackColor = false;
            this.AuditBrowse.Click += new System.EventHandler(this.AuditBrowse_Click);
            // 
            // AuditFormat
            // 
            this.AuditFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AuditFormat.FormattingEnabled = true;
            this.AuditFormat.Items.AddRange(new object[] {
            "CSV",
            "XML"});
            this.AuditFormat.Location = new System.Drawing.Point(24, 120);
            this.AuditFormat.Name = "AuditFormat";
            this.AuditFormat.Size = new System.Drawing.Size(121, 21);
            this.AuditFormat.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Format:";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.Info;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(544, 46);
            this.label1.TabIndex = 1;
            this.label1.Text = "Auditing archives every query and execution result to a zip file. It provides an " +
    "audit trail of what you executed.\r\nUse the Database > \"Audit Trail\" menu to sear" +
    "ch.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // AuditingEnabled
            // 
            this.AuditingEnabled.AutoSize = true;
            this.AuditingEnabled.Location = new System.Drawing.Point(24, 70);
            this.AuditingEnabled.Name = "AuditingEnabled";
            this.AuditingEnabled.Size = new System.Drawing.Size(65, 17);
            this.AuditingEnabled.TabIndex = 0;
            this.AuditingEnabled.Text = "Enabled";
            this.AuditingEnabled.UseVisualStyleBackColor = true;
            // 
            // CleanTabPage
            // 
            this.CleanTabPage.Controls.Add(this.label7);
            this.CleanTabPage.Controls.Add(this.RunCleanerCheckButton);
            this.CleanTabPage.Controls.Add(this.label6);
            this.CleanTabPage.Controls.Add(this.label5);
            this.CleanTabPage.Controls.Add(this.CleanAgeInput);
            this.CleanTabPage.Controls.Add(this.label4);
            this.CleanTabPage.Controls.Add(this.CleanCheckInput);
            this.CleanTabPage.Controls.Add(this.label3);
            this.CleanTabPage.Location = new System.Drawing.Point(4, 22);
            this.CleanTabPage.Name = "CleanTabPage";
            this.CleanTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.CleanTabPage.Size = new System.Drawing.Size(550, 228);
            this.CleanTabPage.TabIndex = 2;
            this.CleanTabPage.Text = "Auto Cleaner";
            this.CleanTabPage.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.SystemColors.Info;
            this.label7.Dock = System.Windows.Forms.DockStyle.Top;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(544, 46);
            this.label7.TabIndex = 7;
            this.label7.Text = "Periodically ask to delete cached files (logs, audits, db schemas)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RunCleanerCheckButton
            // 
            this.RunCleanerCheckButton.BackColor = System.Drawing.Color.White;
            this.RunCleanerCheckButton.Location = new System.Drawing.Point(23, 145);
            this.RunCleanerCheckButton.Name = "RunCleanerCheckButton";
            this.RunCleanerCheckButton.Size = new System.Drawing.Size(137, 32);
            this.RunCleanerCheckButton.TabIndex = 6;
            this.RunCleanerCheckButton.Text = "Check Now";
            this.RunCleanerCheckButton.UseVisualStyleBackColor = false;
            this.RunCleanerCheckButton.Click += new System.EventHandler(this.RunCleanerCheckButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(212, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "days";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(212, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "days";
            // 
            // CleanAgeInput
            // 
            this.CleanAgeInput.Location = new System.Drawing.Point(134, 101);
            this.CleanAgeInput.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.CleanAgeInput.Name = "CleanAgeInput";
            this.CleanAgeInput.Size = new System.Drawing.Size(72, 20);
            this.CleanAgeInput.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Limit to files older than";
            // 
            // CleanCheckInput
            // 
            this.CleanCheckInput.Location = new System.Drawing.Point(134, 75);
            this.CleanCheckInput.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.CleanCheckInput.Name = "CleanCheckInput";
            this.CleanCheckInput.Size = new System.Drawing.Size(72, 20);
            this.CleanCheckInput.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ask every";
            // 
            // UpdatesTabPage
            // 
            this.UpdatesTabPage.Controls.Add(this.StableUpdatesCheck);
            this.UpdatesTabPage.Controls.Add(this.UpdatesDisabledLabel);
            this.UpdatesTabPage.Controls.Add(this.label10);
            this.UpdatesTabPage.Controls.Add(this.label9);
            this.UpdatesTabPage.Controls.Add(this.UpdateIntervalInput);
            this.UpdatesTabPage.Controls.Add(this.label8);
            this.UpdatesTabPage.Location = new System.Drawing.Point(4, 22);
            this.UpdatesTabPage.Name = "UpdatesTabPage";
            this.UpdatesTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.UpdatesTabPage.Size = new System.Drawing.Size(550, 228);
            this.UpdatesTabPage.TabIndex = 3;
            this.UpdatesTabPage.Text = "Updates";
            this.UpdatesTabPage.UseVisualStyleBackColor = true;
            // 
            // StableUpdatesCheck
            // 
            this.StableUpdatesCheck.AutoSize = true;
            this.StableUpdatesCheck.Location = new System.Drawing.Point(28, 103);
            this.StableUpdatesCheck.Name = "StableUpdatesCheck";
            this.StableUpdatesCheck.Size = new System.Drawing.Size(119, 17);
            this.StableUpdatesCheck.TabIndex = 10;
            this.StableUpdatesCheck.Text = "Stable updates only";
            this.StableUpdatesCheck.UseVisualStyleBackColor = true;
            // 
            // UpdatesDisabledLabel
            // 
            this.UpdatesDisabledLabel.AutoSize = true;
            this.UpdatesDisabledLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdatesDisabledLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.UpdatesDisabledLabel.Location = new System.Drawing.Point(427, 69);
            this.UpdatesDisabledLabel.Name = "UpdatesDisabledLabel";
            this.UpdatesDisabledLabel.Size = new System.Drawing.Size(105, 13);
            this.UpdatesDisabledLabel.TabIndex = 9;
            this.UpdatesDisabledLabel.Text = "updates are disabled";
            this.UpdatesDisabledLabel.Visible = false;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.SystemColors.Info;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(544, 46);
            this.label10.TabIndex = 8;
            this.label10.Text = "If updates are available, a button on the main toolbar will take you to the downl" +
    "oad page.";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(247, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "hours";
            // 
            // UpdateIntervalInput
            // 
            this.UpdateIntervalInput.Location = new System.Drawing.Point(169, 69);
            this.UpdateIntervalInput.Maximum = new decimal(new int[] {
            336,
            0,
            0,
            0});
            this.UpdateIntervalInput.Name = "UpdateIntervalInput";
            this.UpdateIntervalInput.Size = new System.Drawing.Size(72, 20);
            this.UpdateIntervalInput.TabIndex = 5;
            this.UpdateIntervalInput.ValueChanged += new System.EventHandler(this.UpdateIntervalInput_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Check for updates every";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CancelButton);
            this.panel1.Controls.Add(this.SaveButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 254);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(558, 67);
            this.panel1.TabIndex = 1;
            // 
            // CancelButton
            // 
            this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CancelButton.BackColor = System.Drawing.Color.White;
            this.CancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelButton.Location = new System.Drawing.Point(443, 17);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(93, 33);
            this.CancelButton.TabIndex = 1;
            this.CancelButton.Text = "&Cancel";
            this.CancelButton.UseVisualStyleBackColor = false;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SaveButton.BackColor = System.Drawing.Color.White;
            this.SaveButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.SaveButton.Location = new System.Drawing.Point(344, 17);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(93, 33);
            this.SaveButton.TabIndex = 0;
            this.SaveButton.Text = "&Save";
            this.SaveButton.UseVisualStyleBackColor = false;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 200;
            this.toolTip1.AutoPopDelay = 10000;
            this.toolTip1.InitialDelay = 200;
            this.toolTip1.ReshowDelay = 40;
            // 
            // RowcountLimitInput
            // 
            this.RowcountLimitInput.Increment = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.RowcountLimitInput.Location = new System.Drawing.Point(49, 165);
            this.RowcountLimitInput.Maximum = new decimal(new int[] {
            5000000,
            0,
            0,
            0});
            this.RowcountLimitInput.Name = "RowcountLimitInput";
            this.RowcountLimitInput.Size = new System.Drawing.Size(131, 20);
            this.RowcountLimitInput.TabIndex = 8;
            this.RowcountLimitInput.ThousandsSeparator = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(46, 149);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "SET ROWCOUNT";
            // 
            // pictureBox3
            // 
            pictureBox3.Image = global::SQLAnt.Properties.Resources.dialog_information;
            pictureBox3.Location = new System.Drawing.Point(27, 147);
            pictureBox3.Name = "pictureBox3";
            pictureBox3.Size = new System.Drawing.Size(16, 16);
            pictureBox3.TabIndex = 9;
            pictureBox3.TabStop = false;
            this.toolTip1.SetToolTip(pictureBox3, "SET ROWCOUNT implicitly to limit the number of rows returned. Zero disables this " +
        "limit.");
            // 
            // SettingsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(558, 321);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Name = "SettingsWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Settings";
            ((System.ComponentModel.ISupportInitialize)(pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pictureBox2)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.EditorTabPage.ResumeLayout(false);
            this.EditorTabPage.PerformLayout();
            this.AuditTabPage.ResumeLayout(false);
            this.AuditTabPage.PerformLayout();
            this.CleanTabPage.ResumeLayout(false);
            this.CleanTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CleanAgeInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CleanCheckInput)).EndInit();
            this.UpdatesTabPage.ResumeLayout(false);
            this.UpdatesTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpdateIntervalInput)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RowcountLimitInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage AuditTabPage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.CheckBox AuditingEnabled;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox AuditFormat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button AuditBrowse;
        private System.Windows.Forms.TabPage EditorTabPage;
        private System.Windows.Forms.CheckBox AutoQualifySchemaNamesCheck;
        private System.Windows.Forms.CheckBox AutoSaveOnCloseCheck;
        private System.Windows.Forms.CheckBox AutoDetectSchemaChangesCheck;
        private System.Windows.Forms.TabPage CleanTabPage;
        private System.Windows.Forms.NumericUpDown CleanAgeInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown CleanCheckInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button RunCleanerCheckButton;
        private System.Windows.Forms.CheckBox AutoCapitalizeKeywordsCheck;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox RememberOpenFilesCheck;
        private System.Windows.Forms.TabPage UpdatesTabPage;
        private System.Windows.Forms.Label UpdatesDisabledLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown UpdateIntervalInput;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox StableUpdatesCheck;
        private System.Windows.Forms.NumericUpDown RowcountLimitInput;
        private System.Windows.Forms.Label label11;
    }
}