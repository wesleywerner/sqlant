﻿namespace SQLAnt
{
    partial class StudioWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StudioWindow));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.AntMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.NewQueryButton = new System.Windows.Forms.ToolStripButton();
            this.OpenQueryButton = new System.Windows.Forms.ToolStripButton();
            this.SaveQueryButton = new System.Windows.Forms.ToolStripButton();
            this.OpenRecentButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ServerListDropdown = new System.Windows.Forms.ToolStripDropDownButton();
            this.DatabaseListDropdown = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ExecuteQueryButton = new System.Windows.Forms.ToolStripButton();
            this.StopExecutionButton = new System.Windows.Forms.ToolStripButton();
            this.AutoCleanerButton = new System.Windows.Forms.ToolStripButton();
            this.UpdateUrlButton = new System.Windows.Forms.ToolStripButton();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.LoadLiveSchemaThread = new System.ComponentModel.BackgroundWorker();
            this.EditorTabs = new System.Windows.Forms.TabControl();
            this.ServerConnectionThread = new System.ComponentModel.BackgroundWorker();
            this.InfoPanel = new System.Windows.Forms.Panel();
            this.InfoLabel = new System.Windows.Forms.Label();
            this.LoadCachedSchemaThread = new System.ComponentModel.BackgroundWorker();
            this.CheckDbForChanges = new System.Windows.Forms.Timer(this.components);
            this.AutoCleanerWorker = new System.ComponentModel.BackgroundWorker();
            this.DetectDbChangesThread = new System.ComponentModel.BackgroundWorker();
            this.UpdateCheckerThread = new System.ComponentModel.BackgroundWorker();
            this.UpdateCheckerTimer = new System.Windows.Forms.Timer(this.components);
            this.StudioWindowStatusBar = new System.Windows.Forms.StatusStrip();
            this.MemoryUsageStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.MemoryUsageTimer = new System.Windows.Forms.Timer(this.components);
            this.performanceCounter1 = new System.Diagnostics.PerformanceCounter();
            this.menuStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.InfoPanel.SuspendLayout();
            this.StudioWindowStatusBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.performanceCounter1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AntMenu,
            this.fileMenu,
            this.editMenu,
            this.viewMenu,
            this.databaseMenu});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(632, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // AntMenu
            // 
            this.AntMenu.Name = "AntMenu";
            this.AntMenu.Size = new System.Drawing.Size(38, 20);
            this.AntMenu.Text = "&Ant";
            // 
            // fileMenu
            // 
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // editMenu
            // 
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(39, 20);
            this.editMenu.Text = "&Edit";
            // 
            // viewMenu
            // 
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "&View";
            // 
            // databaseMenu
            // 
            this.databaseMenu.Name = "databaseMenu";
            this.databaseMenu.Size = new System.Drawing.Size(67, 20);
            this.databaseMenu.Text = "&Database";
            // 
            // toolStrip
            // 
            this.toolStrip.AutoSize = false;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NewQueryButton,
            this.OpenQueryButton,
            this.SaveQueryButton,
            this.OpenRecentButton,
            this.toolStripSeparator2,
            this.ServerListDropdown,
            this.DatabaseListDropdown,
            this.toolStripSeparator1,
            this.ExecuteQueryButton,
            this.StopExecutionButton,
            this.AutoCleanerButton,
            this.UpdateUrlButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(632, 32);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "ToolStrip";
            // 
            // NewQueryButton
            // 
            this.NewQueryButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.NewQueryButton.Name = "NewQueryButton";
            this.NewQueryButton.Size = new System.Drawing.Size(35, 29);
            this.NewQueryButton.Text = "&New";
            // 
            // OpenQueryButton
            // 
            this.OpenQueryButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OpenQueryButton.Name = "OpenQueryButton";
            this.OpenQueryButton.Size = new System.Drawing.Size(40, 29);
            this.OpenQueryButton.Text = "&Open";
            // 
            // SaveQueryButton
            // 
            this.SaveQueryButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.SaveQueryButton.Name = "SaveQueryButton";
            this.SaveQueryButton.Size = new System.Drawing.Size(35, 29);
            this.SaveQueryButton.Text = "&Save";
            // 
            // OpenRecentButton
            // 
            this.OpenRecentButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OpenRecentButton.Name = "OpenRecentButton";
            this.OpenRecentButton.Size = new System.Drawing.Size(56, 29);
            this.OpenRecentButton.Text = "Recent";
            this.OpenRecentButton.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.OpenRecentButton_DropDownItemClicked);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 32);
            // 
            // ServerListDropdown
            // 
            this.ServerListDropdown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ServerListDropdown.Name = "ServerListDropdown";
            this.ServerListDropdown.Size = new System.Drawing.Size(52, 29);
            this.ServerListDropdown.Text = "Server";
            this.ServerListDropdown.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ServerListDropdown_DropDownItemClicked);
            // 
            // DatabaseListDropdown
            // 
            this.DatabaseListDropdown.Enabled = false;
            this.DatabaseListDropdown.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.DatabaseListDropdown.Name = "DatabaseListDropdown";
            this.DatabaseListDropdown.Size = new System.Drawing.Size(68, 29);
            this.DatabaseListDropdown.Text = "Database";
            this.DatabaseListDropdown.DropDownItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.DatabaseListDropdown_DropDownItemClicked);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 32);
            // 
            // ExecuteQueryButton
            // 
            this.ExecuteQueryButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ExecuteQueryButton.Name = "ExecuteQueryButton";
            this.ExecuteQueryButton.Size = new System.Drawing.Size(74, 29);
            this.ExecuteQueryButton.Text = "Execute (F5)";
            this.ExecuteQueryButton.Click += new System.EventHandler(this.ExecuteQueryButton_Click);
            // 
            // StopExecutionButton
            // 
            this.StopExecutionButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StopExecutionButton.Name = "StopExecutionButton";
            this.StopExecutionButton.Size = new System.Drawing.Size(35, 29);
            this.StopExecutionButton.Text = "Stop";
            this.StopExecutionButton.Click += new System.EventHandler(this.StopExecutionButton_Click);
            // 
            // AutoCleanerButton
            // 
            this.AutoCleanerButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.AutoCleanerButton.BackColor = System.Drawing.SystemColors.Info;
            this.AutoCleanerButton.Image = ((System.Drawing.Image)(resources.GetObject("AutoCleanerButton.Image")));
            this.AutoCleanerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.AutoCleanerButton.Name = "AutoCleanerButton";
            this.AutoCleanerButton.Size = new System.Drawing.Size(86, 29);
            this.AutoCleanerButton.Text = "Auto Clean";
            this.AutoCleanerButton.ToolTipText = "Review and remove old files";
            this.AutoCleanerButton.Click += new System.EventHandler(this.AutoCleanerButton_Click);
            // 
            // UpdateUrlButton
            // 
            this.UpdateUrlButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.UpdateUrlButton.Image = ((System.Drawing.Image)(resources.GetObject("UpdateUrlButton.Image")));
            this.UpdateUrlButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.UpdateUrlButton.Name = "UpdateUrlButton";
            this.UpdateUrlButton.Size = new System.Drawing.Size(65, 29);
            this.UpdateUrlButton.Text = "Update";
            this.UpdateUrlButton.ToolTipText = "Update available online";
            this.UpdateUrlButton.Visible = false;
            this.UpdateUrlButton.Click += new System.EventHandler(this.UpdateUrlButton_Click);
            // 
            // LoadLiveSchemaThread
            // 
            this.LoadLiveSchemaThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.LoadLiveSchemaThread_DoWork);
            this.LoadLiveSchemaThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.LoadLiveSchemaThread_RunWorkerCompleted);
            // 
            // EditorTabs
            // 
            this.EditorTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.EditorTabs.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditorTabs.ItemSize = new System.Drawing.Size(71, 26);
            this.EditorTabs.Location = new System.Drawing.Point(0, 56);
            this.EditorTabs.Name = "EditorTabs";
            this.EditorTabs.SelectedIndex = 0;
            this.EditorTabs.Size = new System.Drawing.Size(632, 343);
            this.EditorTabs.TabIndex = 4;
            this.EditorTabs.SelectedIndexChanged += new System.EventHandler(this.EditorTabs_SelectedIndexChanged);
            // 
            // ServerConnectionThread
            // 
            this.ServerConnectionThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.ServerConnectionThread_DoWork);
            this.ServerConnectionThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.ServerConnectionThread_RunWorkerCompleted);
            // 
            // InfoPanel
            // 
            this.InfoPanel.BackColor = System.Drawing.Color.Orange;
            this.InfoPanel.Controls.Add(this.InfoLabel);
            this.InfoPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.InfoPanel.Location = new System.Drawing.Point(0, 399);
            this.InfoPanel.Name = "InfoPanel";
            this.InfoPanel.Padding = new System.Windows.Forms.Padding(20, 6, 0, 0);
            this.InfoPanel.Size = new System.Drawing.Size(632, 32);
            this.InfoPanel.TabIndex = 5;
            this.InfoPanel.Visible = false;
            // 
            // InfoLabel
            // 
            this.InfoLabel.AutoSize = true;
            this.InfoLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.InfoLabel.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoLabel.Location = new System.Drawing.Point(20, 6);
            this.InfoLabel.Name = "InfoLabel";
            this.InfoLabel.Size = new System.Drawing.Size(52, 16);
            this.InfoLabel.TabIndex = 0;
            this.InfoLabel.Text = "label1";
            // 
            // LoadCachedSchemaThread
            // 
            this.LoadCachedSchemaThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.LoadCachedSchemaThread_DoWork);
            this.LoadCachedSchemaThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.LoadCachedSchemaThread_RunWorkerCompleted);
            // 
            // CheckDbForChanges
            // 
            this.CheckDbForChanges.Tick += new System.EventHandler(this.CheckDbForChanges_Tick);
            // 
            // AutoCleanerWorker
            // 
            this.AutoCleanerWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.AutoCleanerWorker_DoWork);
            this.AutoCleanerWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.AutoCleanerWorker_RunWorkerCompleted);
            // 
            // DetectDbChangesThread
            // 
            this.DetectDbChangesThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.DetectDbChangesThread_DoWork);
            this.DetectDbChangesThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.DetectDbChangesThread_RunWorkerCompleted);
            // 
            // UpdateCheckerThread
            // 
            this.UpdateCheckerThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.UpdateCheckerThread_DoWork);
            this.UpdateCheckerThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.UpdateCheckerThread_RunWorkerCompleted);
            // 
            // UpdateCheckerTimer
            // 
            this.UpdateCheckerTimer.Tick += new System.EventHandler(this.UpdateCheckerTimer_Tick);
            // 
            // StudioWindowStatusBar
            // 
            this.StudioWindowStatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MemoryUsageStatusLabel});
            this.StudioWindowStatusBar.Location = new System.Drawing.Point(0, 431);
            this.StudioWindowStatusBar.Name = "StudioWindowStatusBar";
            this.StudioWindowStatusBar.Size = new System.Drawing.Size(632, 22);
            this.StudioWindowStatusBar.TabIndex = 6;
            this.StudioWindowStatusBar.Text = "statusStrip1";
            // 
            // MemoryUsageStatusLabel
            // 
            this.MemoryUsageStatusLabel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.MemoryUsageStatusLabel.Name = "MemoryUsageStatusLabel";
            this.MemoryUsageStatusLabel.Size = new System.Drawing.Size(94, 17);
            this.MemoryUsageStatusLabel.Text = "[memory usage]";
            // 
            // MemoryUsageTimer
            // 
            this.MemoryUsageTimer.Enabled = true;
            this.MemoryUsageTimer.Interval = 5000;
            this.MemoryUsageTimer.Tick += new System.EventHandler(this.MemoryUsageTimer_Tick);
            // 
            // performanceCounter1
            // 
            this.performanceCounter1.CategoryName = "Process";
            this.performanceCounter1.CounterName = "Working Set";
            // 
            // StudioWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(632, 453);
            this.Controls.Add(this.EditorTabs);
            this.Controls.Add(this.InfoPanel);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.StudioWindowStatusBar);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "StudioWindow";
            this.Text = "StudioMDI";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.StudioWindow_FormClosing);
            this.Load += new System.EventHandler(this.StudioWindow_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.StudioWindow_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.StudioWindow_KeyUp);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.InfoPanel.ResumeLayout(false);
            this.InfoPanel.PerformLayout();
            this.StudioWindowStatusBar.ResumeLayout(false);
            this.StudioWindowStatusBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.performanceCounter1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem databaseMenu;
        private System.Windows.Forms.ToolTip toolTip;
        private System.ComponentModel.BackgroundWorker LoadLiveSchemaThread;
        private System.Windows.Forms.ToolStripDropDownButton ServerListDropdown;
        private System.Windows.Forms.ToolStripDropDownButton DatabaseListDropdown;
        private System.Windows.Forms.TabControl EditorTabs;
        private System.ComponentModel.BackgroundWorker ServerConnectionThread;
        private System.Windows.Forms.Panel InfoPanel;
        private System.Windows.Forms.Label InfoLabel;
        private System.Windows.Forms.ToolStripDropDownButton OpenRecentButton;
        private System.ComponentModel.BackgroundWorker LoadCachedSchemaThread;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ExecuteQueryButton;
        private System.Windows.Forms.ToolStripButton NewQueryButton;
        private System.Windows.Forms.ToolStripButton OpenQueryButton;
        private System.Windows.Forms.ToolStripButton SaveQueryButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem AntMenu;
        private System.Windows.Forms.Timer CheckDbForChanges;
        private System.Windows.Forms.ToolStripButton StopExecutionButton;
        private System.ComponentModel.BackgroundWorker AutoCleanerWorker;
        private System.Windows.Forms.ToolStripButton AutoCleanerButton;
        private System.ComponentModel.BackgroundWorker DetectDbChangesThread;
        private System.ComponentModel.BackgroundWorker UpdateCheckerThread;
        private System.Windows.Forms.Timer UpdateCheckerTimer;
        private System.Windows.Forms.ToolStripButton UpdateUrlButton;
        private System.Windows.Forms.StatusStrip StudioWindowStatusBar;
        private System.Windows.Forms.ToolStripStatusLabel MemoryUsageStatusLabel;
        private System.Windows.Forms.Timer MemoryUsageTimer;
        private System.Diagnostics.PerformanceCounter performanceCounter1;
    }
}



