﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Microsoft.Data.ConnectionUI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SQLAnt
{
    public partial class ConnectionsEditorWindow : Form
    {

        private bool IgnoreChanges = false;

        public ConnectionsEditorWindow()
        {
            InitializeComponent();
            ConnectionList.View = View.Details;
            ConnectionList.Columns.Add("Connections", 200);
        }

        private void ConnectionsEditorWindow_Load(object sender, EventArgs e)
        {
            ConnectionList.Items.Clear();

            foreach (string key in Program.connections.Keys)
            {
                if (!key.StartsWith("#"))
                {
                    ConnectionList.Items.Add(key.ToString());
                }
            }

            if (Program.connections.Count == 0)
            {
                this.Show();
                Application.DoEvents();
                NewConnectionButton.PerformClick();
            }
        }

        private void SQLSecurityOption_CheckedChanged(object sender, EventArgs e)
        {
            panel1.Enabled = SQLSecurityOption.Checked;
        }

        private void ConnectionList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ConnectionList.SelectedItems.Count == 0) return;
            string server = ConnectionList.SelectedItems[0].Text;
            if (Program.connections.ContainsKey(server))
            {
                LoadConnectionDetails(Program.connections[server]);
            }
            if (Program.connectionColors.ContainsKey(server))
            {
                string color = Program.connectionColors[server];
                int icolor = 0;
                if (int.TryParse(color, out icolor))
                {
                    ConnectionColorButton.BackColor = Color.FromArgb(icolor);
                }
            }
        }

        private void LoadConnectionDetails(string connectionstring)
        {
            IgnoreChanges = true;
            var builder = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionstring);
            ServerText.Text = builder.DataSource;
            UserText.Text = builder.UserID;
            PasswordText.Text = builder.Password;
            if (builder.IntegratedSecurity)
            {
                IntegratedOption.Checked = true;
            }
            else
            {
                SQLSecurityOption.Checked = true;
            }
            IgnoreChanges = false;
        }

        private void SaveChanges()
        {
            if (IgnoreChanges) return;
            if (ConnectionList.SelectedItems.Count == 0) return;
            var builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
            builder.DataSource = ServerText.Text;
            builder.IntegratedSecurity = IntegratedOption.Checked;
            builder.UserID = UserText.Text;
            builder.Password = PasswordText.Text;
            string server = ServerText.Text;
            Program.connections[server] = builder.ToString();
            Program.connectionColors[server] = ConnectionColorButton.BackColor.ToArgb().ToString();
            ((StudioWindow)this.Owner).CreateServerConnectionDropdownMenu();
        }

        private void UserText_TextChanged(object sender, EventArgs e)
        {
            SaveChanges();
        }

        private void PasswordText_TextChanged(object sender, EventArgs e)
        {
            SaveChanges();
        }

        private void IntegratedOption_CheckedChanged(object sender, EventArgs e)
        {
            SaveChanges();
        }

        private void NewConnectionButton_Click(object sender, EventArgs e)
        {
            string connectionstring;
            if (TryGetDataConnectionStringFromUser(out connectionstring))
            {
                var csb = new System.Data.SqlClient.SqlConnectionStringBuilder(connectionstring);
                Program.connections[csb.DataSource] = connectionstring;
                ServerText.Text = csb.DataSource;
                var item = ConnectionList.Items.Add(ServerText.Text);
                IgnoreChanges = true;
                item.Selected = true;
                IgnoreChanges = false;
                SaveChanges();
            }
        }

        private void RemoveConnectionButton_Click(object sender, EventArgs e)
        {
            if (ConnectionList.SelectedItems.Count == 1)
            {
                if (MessageBox.Show("Remove the connection for " + ServerText.Text + "?", "Remove", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    Program.connections.Remove(ServerText.Text);
                    IgnoreChanges = true;
                    IntegratedOption.Checked = true;
                    ServerText.Text = "";
                    UserText.Text = "";
                    PasswordText.Text = "";
                    ConnectionList.Items.Remove(ConnectionList.SelectedItems[0]);
                    ((StudioWindow)this.Owner).CreateServerConnectionDropdownMenu();
                }
            }
        }

        private void ConnectionsEditorWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            ((StudioWindow)this.Owner).SaveServerConnections();
        }

        /// <summary>
        /// Set the connection color
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConnectionColorButton_Click(object sender, EventArgs e)
        {
            var dlg = new ColorDialog();
            if (dlg.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                ConnectionColorButton.BackColor = dlg.Color;
                SaveChanges();
            }
        }

        bool TryGetDataConnectionStringFromUser(out string outConnectionString)
        {
            using (var dialog = new DataConnectionDialog())
            {
                dialog.DataSources.Add(DataSource.SqlDataSource);
                dialog.SetSelectedDataProvider(DataSource.SqlDataSource, DataProvider.SqlDataProvider);
                DialogResult userChoice = DataConnectionDialog.Show(dialog, this);

                // Return the resulting connection string if a connection was selected:
                if (userChoice == DialogResult.OK)
                {
                    outConnectionString = dialog.ConnectionString;
                    return true;
                }
                else
                {
                    outConnectionString = null;
                    return false;
                }
            }
        }


    }
}
