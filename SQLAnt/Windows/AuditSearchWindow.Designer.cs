﻿namespace SQLAnt
{
    partial class AuditSearchWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.StopSearchButton = new System.Windows.Forms.Button();
            this.StartSearchButton = new System.Windows.Forms.Button();
            this.SearchResultsCheck = new System.Windows.Forms.CheckBox();
            this.SearchInput = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ResultFiles = new System.Windows.Forms.ListView();
            this.splitContextQueryAndData = new System.Windows.Forms.SplitContainer();
            this.QueryContextDisplay = new System.Windows.Forms.TextBox();
            this.DataContextDisplay = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.OpenAuditArchiveButton = new System.Windows.Forms.ToolStripButton();
            this.SearchThread = new System.ComponentModel.BackgroundWorker();
            this.SearchProgress = new System.Windows.Forms.ProgressBar();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContextQueryAndData)).BeginInit();
            this.splitContextQueryAndData.Panel1.SuspendLayout();
            this.splitContextQueryAndData.Panel2.SuspendLayout();
            this.splitContextQueryAndData.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.StopSearchButton);
            this.groupBox1.Controls.Add(this.StartSearchButton);
            this.groupBox1.Controls.Add(this.SearchResultsCheck);
            this.groupBox1.Controls.Add(this.SearchInput);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(660, 83);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search";
            // 
            // StopSearchButton
            // 
            this.StopSearchButton.Enabled = false;
            this.StopSearchButton.Image = global::SQLAnt.Properties.Resources.process_stop;
            this.StopSearchButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StopSearchButton.Location = new System.Drawing.Point(350, 17);
            this.StopSearchButton.Name = "StopSearchButton";
            this.StopSearchButton.Size = new System.Drawing.Size(75, 28);
            this.StopSearchButton.TabIndex = 5;
            this.StopSearchButton.Text = "&Stop";
            this.StopSearchButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.StopSearchButton.UseVisualStyleBackColor = true;
            this.StopSearchButton.Click += new System.EventHandler(this.StopSearchButton_Click);
            // 
            // StartSearchButton
            // 
            this.StartSearchButton.Image = global::SQLAnt.Properties.Resources.edit_find;
            this.StartSearchButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.StartSearchButton.Location = new System.Drawing.Point(269, 17);
            this.StartSearchButton.Name = "StartSearchButton";
            this.StartSearchButton.Size = new System.Drawing.Size(75, 28);
            this.StartSearchButton.TabIndex = 4;
            this.StartSearchButton.Text = "&Search";
            this.StartSearchButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.StartSearchButton.UseVisualStyleBackColor = true;
            this.StartSearchButton.Click += new System.EventHandler(this.StartSearchButton_Click);
            // 
            // SearchResultsCheck
            // 
            this.SearchResultsCheck.AutoSize = true;
            this.SearchResultsCheck.Location = new System.Drawing.Point(12, 51);
            this.SearchResultsCheck.Name = "SearchResultsCheck";
            this.SearchResultsCheck.Size = new System.Drawing.Size(143, 17);
            this.SearchResultsCheck.TabIndex = 3;
            this.SearchResultsCheck.Text = "Search within query data";
            this.toolTip1.SetToolTip(this.SearchResultsCheck, "Look at the query results (csv) for matches. Increases the search time.");
            this.SearchResultsCheck.UseVisualStyleBackColor = true;
            // 
            // SearchInput
            // 
            this.SearchInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchInput.Location = new System.Drawing.Point(12, 19);
            this.SearchInput.MaxLength = 250;
            this.SearchInput.Name = "SearchInput";
            this.SearchInput.Size = new System.Drawing.Size(251, 26);
            this.SearchInput.TabIndex = 0;
            this.SearchInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SearchInput_KeyDown);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.splitContainer1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(6, 89);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(660, 326);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Results";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 16);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ResultFiles);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContextQueryAndData);
            this.splitContainer1.Panel2.Controls.Add(this.toolStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(654, 307);
            this.splitContainer1.SplitterDistance = 260;
            this.splitContainer1.TabIndex = 0;
            // 
            // ResultFiles
            // 
            this.ResultFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ResultFiles.Location = new System.Drawing.Point(0, 0);
            this.ResultFiles.Name = "ResultFiles";
            this.ResultFiles.Size = new System.Drawing.Size(260, 307);
            this.ResultFiles.TabIndex = 0;
            this.ResultFiles.UseCompatibleStateImageBehavior = false;
            this.ResultFiles.View = System.Windows.Forms.View.Details;
            this.ResultFiles.SelectedIndexChanged += new System.EventHandler(this.ResultFiles_SelectedIndexChanged);
            // 
            // splitContextQueryAndData
            // 
            this.splitContextQueryAndData.Location = new System.Drawing.Point(90, 111);
            this.splitContextQueryAndData.Name = "splitContextQueryAndData";
            this.splitContextQueryAndData.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContextQueryAndData.Panel1
            // 
            this.splitContextQueryAndData.Panel1.Controls.Add(this.QueryContextDisplay);
            // 
            // splitContextQueryAndData.Panel2
            // 
            this.splitContextQueryAndData.Panel2.Controls.Add(this.DataContextDisplay);
            this.splitContextQueryAndData.Size = new System.Drawing.Size(208, 128);
            this.splitContextQueryAndData.SplitterDistance = 64;
            this.splitContextQueryAndData.TabIndex = 3;
            // 
            // QueryContextDisplay
            // 
            this.QueryContextDisplay.BackColor = System.Drawing.Color.White;
            this.QueryContextDisplay.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QueryContextDisplay.HideSelection = false;
            this.QueryContextDisplay.Location = new System.Drawing.Point(17, 3);
            this.QueryContextDisplay.Multiline = true;
            this.QueryContextDisplay.Name = "QueryContextDisplay";
            this.QueryContextDisplay.ReadOnly = true;
            this.QueryContextDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.QueryContextDisplay.Size = new System.Drawing.Size(132, 33);
            this.QueryContextDisplay.TabIndex = 0;
            // 
            // DataContextDisplay
            // 
            this.DataContextDisplay.BackColor = System.Drawing.Color.White;
            this.DataContextDisplay.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataContextDisplay.HideSelection = false;
            this.DataContextDisplay.Location = new System.Drawing.Point(17, 3);
            this.DataContextDisplay.Multiline = true;
            this.DataContextDisplay.Name = "DataContextDisplay";
            this.DataContextDisplay.ReadOnly = true;
            this.DataContextDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.DataContextDisplay.Size = new System.Drawing.Size(132, 34);
            this.DataContextDisplay.TabIndex = 2;
            this.DataContextDisplay.WordWrap = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenAuditArchiveButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(390, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // OpenAuditArchiveButton
            // 
            this.OpenAuditArchiveButton.Enabled = false;
            this.OpenAuditArchiveButton.Image = global::SQLAnt.Properties.Resources.document_open;
            this.OpenAuditArchiveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OpenAuditArchiveButton.Name = "OpenAuditArchiveButton";
            this.OpenAuditArchiveButton.Size = new System.Drawing.Size(119, 22);
            this.OpenAuditArchiveButton.Text = "Open this archive";
            this.OpenAuditArchiveButton.Click += new System.EventHandler(this.OpenAuditArchiveButton_Click);
            // 
            // SearchThread
            // 
            this.SearchThread.WorkerReportsProgress = true;
            this.SearchThread.WorkerSupportsCancellation = true;
            this.SearchThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.SearchThread_DoWork);
            this.SearchThread.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.SearchThread_ProgressChanged);
            this.SearchThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.SearchThread_RunWorkerCompleted);
            // 
            // SearchProgress
            // 
            this.SearchProgress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.SearchProgress.Location = new System.Drawing.Point(6, 415);
            this.SearchProgress.Name = "SearchProgress";
            this.SearchProgress.Size = new System.Drawing.Size(660, 23);
            this.SearchProgress.TabIndex = 4;
            // 
            // AuditSearchWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(672, 444);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.SearchProgress);
            this.Name = "AuditSearchWindow";
            this.Padding = new System.Windows.Forms.Padding(6);
            this.Text = "Audit Search";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContextQueryAndData.Panel1.ResumeLayout(false);
            this.splitContextQueryAndData.Panel1.PerformLayout();
            this.splitContextQueryAndData.Panel2.ResumeLayout(false);
            this.splitContextQueryAndData.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContextQueryAndData)).EndInit();
            this.splitContextQueryAndData.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox SearchInput;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView ResultFiles;
        private System.Windows.Forms.TextBox QueryContextDisplay;
        private System.ComponentModel.BackgroundWorker SearchThread;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton OpenAuditArchiveButton;
        private System.Windows.Forms.ProgressBar SearchProgress;
        private System.Windows.Forms.CheckBox SearchResultsCheck;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TextBox DataContextDisplay;
        private System.Windows.Forms.SplitContainer splitContextQueryAndData;
        private System.Windows.Forms.Button StartSearchButton;
        private System.Windows.Forms.Button StopSearchButton;
    }
}