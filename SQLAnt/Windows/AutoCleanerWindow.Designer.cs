﻿namespace SQLAnt.Windows
{
    partial class AutoCleanerWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FilesList = new System.Windows.Forms.ListView();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.ConfirmCheck = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BusyIndicator = new System.Windows.Forms.ProgressBar();
            this.CleanupWorker = new System.ComponentModel.BackgroundWorker();
            this.TrashCheck = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // FilesList
            // 
            this.FilesList.CheckBoxes = true;
            this.FilesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FilesList.FullRowSelect = true;
            this.FilesList.Location = new System.Drawing.Point(0, 0);
            this.FilesList.Name = "FilesList";
            this.FilesList.Size = new System.Drawing.Size(473, 215);
            this.FilesList.TabIndex = 0;
            this.FilesList.UseCompatibleStateImageBehavior = false;
            this.FilesList.View = System.Windows.Forms.View.Details;
            // 
            // DeleteButton
            // 
            this.DeleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.DeleteButton.BackColor = System.Drawing.Color.White;
            this.DeleteButton.Enabled = false;
            this.DeleteButton.Location = new System.Drawing.Point(344, 18);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(117, 33);
            this.DeleteButton.TabIndex = 3;
            this.DeleteButton.Text = "Delete selected";
            this.DeleteButton.UseVisualStyleBackColor = false;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // ConfirmCheck
            // 
            this.ConfirmCheck.AutoSize = true;
            this.ConfirmCheck.Location = new System.Drawing.Point(12, 27);
            this.ConfirmCheck.Name = "ConfirmCheck";
            this.ConfirmCheck.Size = new System.Drawing.Size(103, 17);
            this.ConfirmCheck.TabIndex = 4;
            this.ConfirmCheck.Text = "Confirm Deletion";
            this.ConfirmCheck.UseVisualStyleBackColor = true;
            this.ConfirmCheck.CheckedChanged += new System.EventHandler(this.ConfirmCheck_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.TrashCheck);
            this.panel1.Controls.Add(this.DeleteButton);
            this.panel1.Controls.Add(this.ConfirmCheck);
            this.panel1.Controls.Add(this.BusyIndicator);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 215);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(473, 65);
            this.panel1.TabIndex = 5;
            // 
            // BusyIndicator
            // 
            this.BusyIndicator.Location = new System.Drawing.Point(12, 18);
            this.BusyIndicator.Name = "BusyIndicator";
            this.BusyIndicator.Size = new System.Drawing.Size(449, 23);
            this.BusyIndicator.TabIndex = 5;
            this.BusyIndicator.Visible = false;
            // 
            // CleanupWorker
            // 
            this.CleanupWorker.WorkerReportsProgress = true;
            this.CleanupWorker.WorkerSupportsCancellation = true;
            this.CleanupWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.CleanupWorker_DoWork);
            this.CleanupWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.CleanupWorker_ProgressChanged);
            this.CleanupWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.CleanupWorker_RunWorkerCompleted);
            // 
            // TrashCheck
            // 
            this.TrashCheck.AutoSize = true;
            this.TrashCheck.Checked = true;
            this.TrashCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.TrashCheck.Location = new System.Drawing.Point(136, 27);
            this.TrashCheck.Name = "TrashCheck";
            this.TrashCheck.Size = new System.Drawing.Size(104, 17);
            this.TrashCheck.TabIndex = 6;
            this.TrashCheck.Text = "Use Recycle bin";
            this.TrashCheck.UseVisualStyleBackColor = true;
            // 
            // AutoCleanerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(473, 280);
            this.Controls.Add(this.FilesList);
            this.Controls.Add(this.panel1);
            this.Name = "AutoCleanerWindow";
            this.Text = "Cleaner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AutoCleanerWindow_FormClosing);
            this.Load += new System.EventHandler(this.AutoCleanerWindow_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView FilesList;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.CheckBox ConfirmCheck;
        private System.Windows.Forms.Panel panel1;
        private System.ComponentModel.BackgroundWorker CleanupWorker;
        private System.Windows.Forms.ProgressBar BusyIndicator;
        private System.Windows.Forms.CheckBox TrashCheck;
    }
}