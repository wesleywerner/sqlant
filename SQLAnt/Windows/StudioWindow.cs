﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Microsoft.SqlServer.Management.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ScintillaNET_FindReplaceDialog;
using System.Net;
using System.Diagnostics;

namespace SQLAnt
{
    public partial class StudioWindow : Form
    {

        /// <summary>
        /// Remember the last time the control modifier was pressed.
        /// </summary>
        private DateTime lastControlPressed = DateTime.Now;


        /// <summary>
        /// Reusable floating window
        /// </summary>
        private ConnectionsEditorWindow ConnectionEditor;


        /// <summary>
        /// References the active editor's word wrap state
        /// </summary>
        private ToolStripMenuItem WordWrapMenuItem;


        /// <summary>
        /// References the default database menu item
        /// </summary>
        private ToolStripMenuItem DefaultDbMenuItem;


        /// <summary>
        /// Caches the database list per server for the session only
        /// </summary>
        private Dictionary<string, List<string>> DatabaseList;


        /// <summary>
        /// Stored the time of the last schema change.
        /// Used for detecting new changes in a schema.
        /// </summary>
        private Dictionary<string, DateTime> LastSchemaChange;


        /// <summary>
        /// FindReplace dialog
        /// </summary>
        FindReplace MyFindReplace;


        /// <summary>
        /// Main window entry point.
        /// </summary>
        public StudioWindow()
        {
            InitializeComponent();
            this.Text = Application.ProductName;
            this.Icon = SQLAnt.Properties.Resources.ant;
            CreateMainMenus();
            CreateMainToolbars();
            Utilities.LogAction(new string('-', 80));
            Utilities.LogAction("SQLAnt startup sequence");
            Utilities.LogAction("Loading server list");
            this.CreateServerConnectionDropdownMenu();
            this.CreateRecentFilesMenu();
            EditorTabs.TabPages.Clear();
            this.HideInfo();
            Utilities.LogAction("Opening previous session queries");
            OpenKnownFiles();
            Utilities.LogAction("Opening auto saved queries");
            OpenAutoSavedFiles();

            CheckDbForChanges.Interval = (1000 * 15);
            CheckDbForChanges.Start();
            if (Program.rc.GetBool("auto refresh schema", true))
            {
                Utilities.LogAction("Monitoring for db changes every {0} seconds", Math.Floor(CheckDbForChanges.Interval / 1000M));
            }

            // trigger update check
            UpdateCheckerTimer.Interval = (1000 * 60 * 15);
            UpdateCheckerTimer.Start();
            Utilities.LogAction("Update checker initialized");
        }


        /// <summary>
        /// Main Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StudioWindow_Load(object sender, EventArgs e)
        {
            if (EditorTabs.TabCount == 0)
            {
                NewQueryMenuClick(null, null);
            }
            RunCleanupCheck();
        }


        /// <summary>
        /// Main window close handler.
        /// Prompts to save changed editors before closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StudioWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Remove previous auto save queries
            var quickies = Directory.GetFiles(Utilities.Storage("autosaved", ""));
            foreach (string quicksave in quickies)
            {
                File.Delete(quicksave);
            }

            // Clear the open files list
            Program.rc["open files"] = string.Empty;

            // Close all open tabs
            while (EditorTabs.TabCount > 0)
            {
                var editor = this.ActiveEditor();
                if (editor != null)
                {
                    editor.Focus();
                    if (!editor.CloseQuery(true))
                    {
                        e.Cancel = true;
                        break;
                    }
                }
            }
        }


        /// <summary>
        /// Handler for the main window key up events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StudioWindow_KeyUp(object sender, KeyEventArgs e)
        {
            // Allow a grace period for the control modifier. 
            // We do this because e.Control is false on the key up event (it is only true for the KeyDown event).
            // mainly because we expect the modifier to indicate truthfully, yet win forms has 
            // other ideas of how modifiers should be treated.
            bool recentControlPressed = (DateTime.Now - lastControlPressed).TotalMilliseconds < 500;
            //TODO if we return here, then the next line will ALWAYS pass false for recentControlPressed!
            if (recentControlPressed) return;
            SendKeysToCurrentEditor(recentControlPressed, e.Alt, e.Shift, e.KeyCode);
        }


        /// <summary>
        /// Handler for the main window key down events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StudioWindow_KeyDown(object sender, KeyEventArgs e)
        {
            // Remember when the control key was pressed
            lastControlPressed = (e.Control) ? DateTime.Now : lastControlPressed;
        }


        /// <summary>
        /// Creates the main window menu items.
        /// </summary>
        private void CreateMainMenus()
        {

            ToolStripMenuItem item = null;

            // Default state for controls
            StopExecutionButton.Enabled = false;

            // ---- Ant Menu
            
            // Settings
            item = (ToolStripMenuItem)AntMenu.DropDownItems.Add("Settings", null, ProgramSettingsMenuClick);
            
            // Help
            AntMenu.DropDownItems.Add("-");
            item = (ToolStripMenuItem)AntMenu.DropDownItems.Add("Online Wiki", null, WikiMenuClick);
            item.ShortcutKeys = Keys.F1;
            AntMenu.DropDownItems.Add("-");
           
            // About
            item = (ToolStripMenuItem)AntMenu.DropDownItems.Add("About", null, AboutMenuClick);

            // ---- File Menu
            
            // New
            item = (ToolStripMenuItem)fileMenu.DropDownItems.Add("&New", SQLAnt.Properties.Resources.document_new, NewQueryMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.N;

            // Open
            item = (ToolStripMenuItem)fileMenu.DropDownItems.Add("&Open", SQLAnt.Properties.Resources.document_open, OpenQueryMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.O;

            // Close
            item = (ToolStripMenuItem)fileMenu.DropDownItems.Add("Clos&e", null, CloseQueryMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.W;

            fileMenu.DropDownItems.Add("-");

            // Save
            item = (ToolStripMenuItem)fileMenu.DropDownItems.Add("&Save", SQLAnt.Properties.Resources.document_save, SaveQueryMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.S;

            // Save As
            item = (ToolStripMenuItem)fileMenu.DropDownItems.Add("Save &as", SQLAnt.Properties.Resources.document_save_as, SaveQueryAsMenuClick);

            // Save Snippet
            item = (ToolStripMenuItem)fileMenu.DropDownItems.Add("Save as Sni&ppet", null, SaveQueryAsSnippetMenuClick);

            // Quit
            item = (ToolStripMenuItem)fileMenu.DropDownItems.Add("&Quit", null, QuitMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.Q;

            // ---- Edit Menu
            // Undo
            item = (ToolStripMenuItem)editMenu.DropDownItems.Add("&Undo", SQLAnt.Properties.Resources.edit_undo, UndoMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.Z;

            // Redo
            item = (ToolStripMenuItem)editMenu.DropDownItems.Add("&Redo", SQLAnt.Properties.Resources.edit_redo, RedoMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.Y;

            editMenu.DropDownItems.Add("-");

            // Cut
            item = (ToolStripMenuItem)editMenu.DropDownItems.Add("Cu&t", SQLAnt.Properties.Resources.edit_cut, CutMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.X;

            // Copy
            item = (ToolStripMenuItem)editMenu.DropDownItems.Add("&Copy", SQLAnt.Properties.Resources.edit_copy, CopyMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.C;

            // Paste
            item = (ToolStripMenuItem)editMenu.DropDownItems.Add("&Paste", SQLAnt.Properties.Resources.edit_paste, PasteMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.V;

            editMenu.DropDownItems.Add("-");

            // Select All
            item = (ToolStripMenuItem)editMenu.DropDownItems.Add("Select &All", SQLAnt.Properties.Resources.edit_select_all, SelectAllMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.A;

            // Select block
            item = (ToolStripMenuItem)editMenu.DropDownItems.Add("Select Code &Block", SQLAnt.Properties.Resources.edit_select_all, SelectBlockMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.F5;

            editMenu.DropDownItems.Add("-");

            // Find
            item = (ToolStripMenuItem)editMenu.DropDownItems.Add("&Find", SQLAnt.Properties.Resources.edit_find, FindTextMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.F;
            
            // Replace
            item = (ToolStripMenuItem)editMenu.DropDownItems.Add("&Replace", SQLAnt.Properties.Resources.edit_find_replace, ReplaceTextMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.H;

            // ---- View Menu

            // Word Wrap
            WordWrapMenuItem = (ToolStripMenuItem)viewMenu.DropDownItems.Add("Word Wrap", null, WordWrapMenuClick);
            
            // Snippet auto complete
            item = (ToolStripMenuItem)viewMenu.DropDownItems.Add("Snippet Auto-complete", null, SnippetAutoCompleteMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.K;
            
            // Object auto complete
            item = (ToolStripMenuItem)viewMenu.DropDownItems.Add("Object Auto-complete", null, ObjectAutoCompleteMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.J;
            viewMenu.DropDownItems.Add("-");
            
            // Refresh Snippets
            item = (ToolStripMenuItem)viewMenu.DropDownItems.Add("Reload Snippets", null, RefreshSnippetsMenuClick);
            item = (ToolStripMenuItem)viewMenu.DropDownItems.Add("Show Snippets in folder", null, ShowSnippetsMenuClick);

            // ---- Database Menu
            
            // Focus database objects filter
            item = (ToolStripMenuItem)databaseMenu.DropDownItems.Add("Filter", null, FocusDatabaseObjectsFilterMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.Shift | Keys.F;
            
            // Script Tables
            item = (ToolStripMenuItem)databaseMenu.DropDownItems.Add("Generate Scripts", null, ScriptDataMenuClick);
            
            // Audit Search
            item = (ToolStripMenuItem)databaseMenu.DropDownItems.Add("Search Audit History", null, SearchAuditTrailMenuClick);
            databaseMenu.DropDownItems.Add("-");
            
            // Set Default
            DefaultDbMenuItem = (ToolStripMenuItem)databaseMenu.DropDownItems.Add("Use current database as default", null, SetDefaultDbMenuClick);
            databaseMenu.DropDownItems.Add("-");
            
            // Refresh databases
            item = (ToolStripMenuItem)databaseMenu.DropDownItems.Add("&Refresh Database List", SQLAnt.Properties.Resources.view_refresh, RefreshDatabasesMenuClick);
            
            // Refresh objects
            item = (ToolStripMenuItem)databaseMenu.DropDownItems.Add("&Refresh Objects", SQLAnt.Properties.Resources.view_refresh, RefreshObjectsMenuClick);
            item.ShortcutKeys = Keys.Control | Keys.Shift | Keys.R;

        }


        /// <summary>
        /// Assigns the main toolbar button images and click handlers.
        /// </summary>
        private void CreateMainToolbars()
        {
            NewQueryButton.Image = SQLAnt.Properties.Resources.document_new;
            OpenQueryButton.Image = SQLAnt.Properties.Resources.document_open;
            SaveQueryButton.Image = SQLAnt.Properties.Resources.document_save;
            OpenRecentButton.Image = SQLAnt.Properties.Resources.document_open;
            ServerListDropdown.Image = SQLAnt.Properties.Resources.network_server;
            DatabaseListDropdown.Image = SQLAnt.Properties.Resources.drive_harddisk;
            ExecuteQueryButton.Image = SQLAnt.Properties.Resources.media_playback_start;
            StopExecutionButton.Image = SQLAnt.Properties.Resources.process_stop;
            AutoCleanerButton.Image = SQLAnt.Properties.Resources.edit_clear;
            AutoCleanerButton.Visible = false;
            UpdateUrlButton.Image = SQLAnt.Properties.Resources.system_installer;

            this.SaveQueryButton.Click += new System.EventHandler(this.SaveQueryMenuClick);
            this.OpenQueryButton.Click += new System.EventHandler(this.OpenQueryMenuClick);
            this.NewQueryButton.Click += new System.EventHandler(this.NewQueryMenuClick);

            MemoryUsageStatusLabel.Text = string.Empty;
        }


        /// <summary>
        /// Creates the connection dropdown menu from the user configuration.
        /// </summary>
        internal void CreateServerConnectionDropdownMenu()
        {
            ServerListDropdown.DropDownItems.Clear();
            foreach (string key in Program.connections.Keys)
            {
                if (!key.StartsWith("#"))
                {
                    ServerListDropdown.DropDownItems.Add(key.ToString());
                }
            }
            if (ServerListDropdown.DropDownItems.Count > 0)
            {
                ServerListDropdown.DropDownItems.Add("-");
            }
            var editItem = ServerListDropdown.DropDownItems.Add("Edit Connections");
        }


        /// <summary>
        /// Saves the server connection configuration to file.
        /// </summary>
        internal void SaveServerConnections()
        {
            Utilities.WriteINIFile(Program.connections, Utilities.Storage("", "connections"));
            Utilities.WriteINIFile(Program.connectionColors, Utilities.Storage("", "connectioncolors"));
        }


        /// <summary>
        /// Gets the active code editor. Returns null if none are active.
        /// </summary>
        /// <returns></returns>
        internal CodeEditor ActiveEditor()
        {
            try
            {
                if (EditorTabs.SelectedTab != null)
                {
                    return (CodeEditor)EditorTabs.SelectedTab.Controls[0];
                }
                return null;
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
                return null;
            }
        }


        /// <summary>
        /// Shows a new code editor tab.
        /// </summary>
        /// <param name="filename">The path of an existing file to open, or string.empty for a blank editor.</param>
        internal CodeEditor ShowEditor(string filename, bool rememberRecentFile)
        {
            if (filename != string.Empty)
            {
                if (!File.Exists(filename))
                {
                    MessageBox.Show("File not found. Removing from recent files list.");
                    Program.recentFiles.Remove(filename);
                    this.CreateRecentFilesMenu();
                    return null;
                }
                else
                {
                    if (rememberRecentFile) AddRecentFile(filename);
                }
            }
            var newEditor = new CodeEditor(filename, this);
            var currentEditor = this.ActiveEditor();
            this.CreateEditorTab(newEditor);
            this.SetEditorConnection(currentEditor, newEditor);
            return newEditor;
        }


        /// <summary>
        /// Applies the connection of one editor to another, or the default connection if there is no active editor.
        /// This allows opening new editors that inherit the current connection.
        /// </summary>
        /// <param name="previousEditor">The editor to copy the connection from</param>
        /// <param name="newEditor">The editor to apply the connection to</param>
        private void SetEditorConnection(CodeEditor previousEditor, CodeEditor newEditor)
        {
            var blueprint = previousEditor ?? newEditor;
            bool useCurrentConnection = (blueprint != null && !string.IsNullOrEmpty(blueprint.Server) && !string.IsNullOrEmpty(blueprint.Database));

            // Apply the current connection to the new editor
            if (useCurrentConnection)
            {
                this.ConnectToServer(blueprint.Server, blueprint.Database);
            }
            else
            {
                // Set the default database for the editor
                if (Program.rc.ContainsKey("default server") && Program.rc.ContainsKey("default database"))
                {
                    this.ConnectToServer(Program.rc["default server"], Program.rc["default database"]);
                }
            }
        }


        /// <summary>
        /// Switches between editors by clicking on the tab control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditorTabs_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var editor = this.ActiveEditor();
                if (editor != null)
                {
                    // Toggle the word wrap button checked state for the current editor control
                    WordWrapMenuItem.Checked = editor != null && editor.scintilla1.WrapMode == ScintillaNET.WrapMode.Word;

                    // Match the server and database
                    this.ConnectToServer(editor.Server, editor.Database);
                    editor.Focus();
                    editor.scintilla1.Focus();

                    UpdateToolbarButtons(editor);

                    // set search target
                    if (MyFindReplace != null) MyFindReplace.Scintilla = editor.scintilla1;
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Update to toolbar execute and stop buttons to reflect the given editor's execution state.
        /// </summary>
        /// <param name="editor"></param>
        internal void UpdateToolbarButtons(CodeEditor editor)
        {
            if (editor == null) editor = this.ActiveEditor();
            if (editor != null)
            {
                // Toggle the execute stop buttons
                ExecuteQueryButton.Enabled = !editor.executeThread.IsBusy;
                StopExecutionButton.Enabled = editor.executeThread.IsBusy;
            }
        }


        /// <summary>
        /// Provides a method to code editors to request database objects asynchronously.
        /// The editor window will receive the result via their AcceptDatabaseObjectlist() call when the work completes.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="database"></param>
        /// <param name="forced"></param>
        /// <param name="target"></param>
        internal void RequestObjectList(string server, string database, bool forced, CodeEditor target)
        {
            if (server == null || database == null || database == string.Empty) return;

            while (LoadCachedSchemaThread.IsBusy) { Application.DoEvents(); }

            Utilities.LogAction("Requesting object list for {0}.{1}", server, database);

            var cacheRequest = new SmoQueryRequest()
            {
                Server = server,
                Database = database,
                Forced = forced,
                Target = target,
                StartTime = DateTime.Now
            };

            this.ShowInfo(string.Format("Loading {0}.{1}", cacheRequest.Server, cacheRequest.Database), InfoType.Info);
            LoadCachedSchemaThread.RunWorkerAsync(cacheRequest);
        }


        /// <summary>
        /// Worker thread that reads the cached database schema from disk.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadCachedSchemaThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var request = (SmoQueryRequest)e.Argument;
            var cachedFile = Utilities.Storage("cache", string.Format("{0} {1}", request.Server, request.Database));
            var meta = new DatabaseMetadata();
            meta.Read(cachedFile);
            request.Result = meta;
            e.Result = request;
        }


        /// <summary>
        /// Callback for when the worker thread completes reading the cached database schema from disk.
        /// If there is no cached schema available, this callback will initiate the worker that fetches the schema from the database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadCachedSchemaThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var cacheRequest = (SmoQueryRequest)e.Result;
            var cached = (DatabaseMetadata)cacheRequest.Result;
            if (cached.IsEmpty() || cacheRequest.Forced)
            {
                while (LoadLiveSchemaThread.IsBusy) { Application.DoEvents(); }
                if (LoadLiveSchemaThread.IsBusy)
                {
                    Utilities.LogAction("An object list request is already in progress");
                    return;
                }

                var request = new SmoQueryRequest()
                {
                    Server = cacheRequest.Server,
                    Database = cacheRequest.Database,
                    Forced = cacheRequest.Forced,
                    Target = cacheRequest.Target,
                    StartTime = DateTime.Now
                };
                LoadLiveSchemaThread.RunWorkerAsync(request);
            }
            else
            {
                // call the target method with the contents of the cached object list
                Utilities.LogAction("Using the cached object list");
                cacheRequest.Target.AcceptDatabaseObjectlist(cached);
                this.HideInfo();
            }
        }


        /// <summary>
        /// Worker thread that loads the database schema from the database engine.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadLiveSchemaThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var request = (SmoQueryRequest)e.Argument;
            var smo = new SmoController(this.SelectedConnection(), request.Database);
            request.Result = smo.GetDatabaseMetadata(request);
            request.Duration = (DateTime.Now - request.StartTime).TotalSeconds;
            e.Result = request;
        }


        /// <summary>
        /// Callback for when the worker thread completes loading the database schema from the database engine.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadLiveSchemaThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                this.HideInfo();

                // Bubble async errors up
                if (e.Error != null) throw e.Error;

                var request = (SmoQueryRequest)e.Result;
                var meta = (DatabaseMetadata)request.Result;
                Utilities.LogAction("Object list queried in {0} seconds", request.Duration);
                WriteSchemaToDisk(request.Server, request.Database, meta);

                // Update all editors that use this database and server
                foreach (TabPage tp in EditorTabs.TabPages)
                {
                    var editor = (CodeEditor)tp.Controls[0];
                    if (editor.Server == request.Server && editor.Database == request.Database)
                    {
                        editor.AcceptDatabaseObjectlist(meta);
                    }
                }
            }
            catch (ConnectionFailureException ex)
            {
                var message = Utilities.LogException(ex, true);
                message = message.Replace(Environment.NewLine, " ");
                ShowInfo(message, InfoType.Error);
                CheckDbForChanges.Start();
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Writes the database schema to disk. This is read at a later time instead of querying the database engine for the schema.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="database"></param>
        /// <param name="meta"></param>
        private void WriteSchemaToDisk(string server, string database, DatabaseMetadata meta)
        {
            try
            {
                if (meta == null) return;
                Utilities.LogAction("Caching object list for {0}.{1}", server, database);
                var cachedFile = Utilities.Storage("cache", string.Format("{0} {1}", server, database));
                meta.Save(cachedFile);
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Event handler for selecting an item from the connection dropdown list. It handles showing the connection editor window, and initiates the chosen connection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ServerListDropdown_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (e.ClickedItem.Text == "Edit Connections")
            {
                if (ConnectionEditor == null || ConnectionEditor.IsDisposed)
                {
                    ConnectionEditor = new ConnectionsEditorWindow();
                    ConnectionEditor.Owner = this;
                }
                ConnectionEditor.Show();
            }
            else
            {
                // auto select the default database on the default server
                string defaultDb = null;
                if (Program.rc.GetString("default server", null) == e.ClickedItem.Text)
                {
                    defaultDb = Program.rc.GetString("default database", null);
                }
                this.ConnectToServer(e.ClickedItem.Text, defaultDb);
            }
        }


        /// <summary>
        /// Initiates a connection by invoking the ServerConnectionThread.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="database"></param>
        private void ConnectToServer(string server, string database)
        {
            try
            {
                if (server == null || server == string.Empty)
                {
                    ServerListDropdown.Text = "Server";
                    return;
                }

                // Use the default database if it is empty, and the server matches the default
                if (Program.rc.ContainsKey("default database") && string.IsNullOrEmpty(database))
                {
                    if (server == Program.rc.GetString("default server", string.Empty))
                    {
                        database = Program.rc.GetString("default database", string.Empty);
                    }
                }

                ShowInfo("Connecting to " + server + "...", InfoType.Info);
                ServerListDropdown.Enabled = false;
                ServerListDropdown.Text = server;
                DatabaseListDropdown.Text = "Database";
                ServerListDropdown.HideDropDown();
                DatabaseListDropdown.Enabled = false;
                DatabaseListDropdown.DropDownItems.Clear();

                var editor = this.ActiveEditor();
                if (editor != null && editor.Server != server)
                {
                    editor.Server = server;
                    editor.Connectionstring = this.SelectedConnection();
                }

                // When a new editor tab is selected it selects the server.
                // New tabs already have a server connection opening, prevent multiple connections overlapping.
                if (!ServerConnectionThread.IsBusy)
                {
                    var request = new SmoQueryRequest()
                    {
                        Server = server,
                        Database = database
                    };
                    ServerConnectionThread.RunWorkerAsync(request);
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Worker thread that connects to a server and retrieves the list of databases.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ServerConnectionThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var request = (SmoQueryRequest)e.Argument;

            // Use the cached database list
            if (DatabaseList == null) DatabaseList = new Dictionary<string, List<string>>();
            if (DatabaseList.ContainsKey(request.Server) && DatabaseList[request.Server].Count > 0)
            {
                request.Result = DatabaseList[request.Server];
            }
            else
            {
                var smo = new SmoController(this.SelectedConnection(), request.Database);
                Utilities.LogAction("Requesting the database list from {0}", request.Server);
                List<string> dblist = smo.GetDatabaseList();
                Utilities.LogAction("Database list received");
                request.Result = dblist;

                // cache this result for future re-use
                DatabaseList[request.Server] = dblist;
            }
            e.Result = request;
        }


        /// <summary>
        /// Callback for when a connection to a server has completed. The result contains a list of all databases on that server, that gets populated into the database dropdown menu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ServerConnectionThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.HideInfo();
            ServerListDropdown.Enabled = true;

            if (e.Error is Microsoft.SqlServer.Management.Common.ConnectionFailureException)
            {
                this.ShowInfo("Failed to connect to the server " + ServerListDropdown.Text, InfoType.Error);
            }
            else if (e.Error != null)
            {
                Utilities.LogException(e.Error);
            }

            if (e.Error == null && e.Result != null)
            {
                var request = (SmoQueryRequest)e.Result;
                foreach (string dbname in (List<string>)request.Result)
                {
                    DatabaseListDropdown.DropDownItems.Add(dbname);
                }
                DatabaseListDropdown.Enabled = true;

                // auto select the database
                if (request.Database != null)
                {
                    this.ApplyDatabaseToActiveEditor(request.Database);
                }
            }
        }


        /// <summary>
        /// Returns the currently selected connection string, or string.empty if none are selected.
        /// </summary>
        /// <returns></returns>
        private string SelectedConnection()
        {
            if (Program.connections.ContainsKey(ServerListDropdown.Text))
            {
                return Program.connections[ServerListDropdown.Text];
            }
            else
            {
                return string.Empty;
            }
        }


        /// <summary>
        /// Apply the specified database to the active code editor.
        /// The editor will request the database object list from us when it's database property is changed.
        /// </summary>
        /// <param name="database"></param>
        private void ApplyDatabaseToActiveEditor(string database)
        {
            try
            {
                var editor = this.ActiveEditor();
                if (string.IsNullOrEmpty(database) || editor == null)
                {
                    DatabaseListDropdown.Text = "Database";
                    return;
                }

                DatabaseListDropdown.Text = database;

                // Set the active editor's database. 
                if (editor.Database != database)
                {
                    editor.Database = database;
                }
                else
                {
                    // The database has been set already, so hide the info panel
                    this.HideInfo();
                }
                // Check the "Set as default" menu if this is the default database
                DefaultDbMenuItem.Checked = (Program.rc.ContainsKey("default database") && database == Program.rc["default database"]);
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Creates a new tab for the given code editor.
        /// </summary>
        /// <param name="editor"></param>
        internal void CreateEditorTab(CodeEditor editor)
        {
            try
            {
                EditorTabs.ShowToolTips = true;
                var tab = new TabPage(editor.Text);
                tab.Controls.Add(editor);
                editor.Dock = DockStyle.Fill;
                tab.Tag = editor;
                EditorTabs.TabPages.Add(tab);
                EditorTabs.SelectedTab = tab;
                tab.ToolTipText = editor.filename;
                editor.Focus();
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Removes the tab for the given code editor. This method does not perform any save-changes checking. It is used internally after the code editor has done all it's saving routines.
        /// </summary>
        /// <param name="target"></param>
        internal void RemoveEditorTab(CodeEditor target)
        {
            int index = EditorTabs.SelectedIndex - 1;
            index = (index == -1) ? 0 : index;
            foreach (TabPage tp in EditorTabs.TabPages)
            {
                if (tp.Controls.Count > 0 && tp.Controls[0] == target)
                {
                    target.ClearResultsGrid();
                    EditorTabs.SelectedTab = EditorTabs.TabPages[index];
                    EditorTabs.TabPages.Remove(tp);
                    EditorTabs.SelectedIndex = index;
                    break;
                }
            }
        }


        /// <summary>
        /// Update the tab text for the specified editor. Used when an editor gets a new filename, or to indicate modified state.
        /// </summary>
        /// <param name="editor"></param>
        internal void UpdateEditorTabText(CodeEditor editor)
        {
            foreach (TabPage tp in EditorTabs.TabPages)
            {
                if (tp.Tag == editor)
                {
                    tp.Text = editor.Text;
                    break;
                }
            }
        }


        /// <summary>
        /// Shows the info panel with the given text.
        /// </summary>
        /// <param name="text">The text to display in the panel</param>
        /// <param name="type">INFO or ERROR</param>
        internal void ShowInfo(string text, InfoType type)
        {
            InfoLabel.Text = text;
            InfoPanel.Visible = true;
            if (type == InfoType.Info)
            {
                InfoLabel.ForeColor = SystemColors.HighlightText;
                InfoPanel.BackColor = SystemColors.Highlight;
            }
            if (type == InfoType.Error)
            {
                InfoLabel.ForeColor = SystemColors.WindowText;
                InfoPanel.BackColor = Color.Orange;
            }
        }


        /// <summary>
        /// Hides the info panel.
        /// </summary>
        internal void HideInfo()
        {
            InfoPanel.Visible = false;
            InfoLabel.Text = string.Empty;
        }


        /// <summary>
        /// Adds a path to the recent files list.
        /// </summary>
        /// <param name="filename"></param>
        internal void AddRecentFile(string filepath)
        {

            // Remove existing to move to the top
            if (Program.recentFiles.Contains(filepath))
            {
                Program.recentFiles.Remove(filepath);
            }

            // Add new item
            Program.recentFiles.Add(filepath);

            // Cull
            while (Program.recentFiles.Count > 20)
            {
                Program.recentFiles.RemoveAt(0);
            }

            // Save
            Utilities.WriteListFile(Utilities.Storage("", "recentfiles"), Program.recentFiles);

            // Rebuild
            this.CreateRecentFilesMenu();

        }


        /// <summary>
        /// Creates the recent files menu.
        /// </summary>
        private void CreateRecentFilesMenu()
        {
            OpenRecentButton.DropDownItems.Clear();
            var recent = new string[Program.recentFiles.Count];
            Program.recentFiles.CopyTo(recent);
            foreach (string file in recent.Reverse())
            {
                string title = Path.GetFileName(file);
                var item = OpenRecentButton.DropDownItems.Add(title);
                item.ToolTipText = file;
            }
        }


        /// <summary>
        /// Shows the generate scripts window.
        /// </summary>
        public void ShowGenerateScriptsWindow(Dictionary<string, string> preselected)
        {
            var cachedFile = Utilities.Storage("cache", string.Format("{0} {1}", ServerListDropdown.Text, DatabaseListDropdown.Text));
            var meta = new DatabaseMetadata();
            meta.Read(cachedFile);
            if (meta.IsEmpty())
            {
                MessageBox.Show("Select a database to script against and wait for the cache to finish loading.");
            }
            else
            {
                var wnd = new ScriptObjectsWindow(this.SelectedConnection(), DatabaseListDropdown.Text, meta, preselected);
                wnd.Owner = this;
                wnd.Show();
            }
        }


        /// <summary>
        /// Open the list of files saved from the previous session.
        /// </summary>
        private void OpenKnownFiles()
        {
            try
            {
                var knownFiles = Program.rc.GetString("open files", string.Empty).Split('|');
                foreach (string known in knownFiles)
                {
                    if (known != string.Empty)
                    {
                        var ed = this.ShowEditor(known, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Open the list of files saved from the previous session, that don't have filenames.
        /// </summary>
        private void OpenAutoSavedFiles()
        {
            try
            {
                var quickies = Directory.GetFiles(Utilities.Storage("autosaved", ""));
                foreach (string quicksave in quickies)
                {
                    var ed = this.ShowEditor(quicksave, false);
                    if (ed != null)
                    {
                        // Empty the editor filename so it prompts for a new filename on save
                        ed.filename = string.Empty;
                        ed.IsAutoSavedQuery = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Timer handler that initiates the schema changes check.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckDbForChanges_Tick(object sender, EventArgs e)
        {
            CheckDbForChanges.Stop();
            if (Program.rc.GetBool("auto refresh schema", true))
            {
                if (LastSchemaChange == null) LastSchemaChange = new Dictionary<string, DateTime>();

                // Use the active editor's connection
                var editor = this.ActiveEditor();
                if (editor == null) return;
                if (SelectedConnection() == string.Empty || editor.Database == null || editor.Database == string.Empty) return;

                var options = new SmoQueryRequest();
                options.Database = editor.Database;
                options.Server = SelectedConnection();
                options.StartTime = DateTime.MinValue;

                // Look up the last known modification time
                if (LastSchemaChange.ContainsKey(editor.Database))
                {
                    options.StartTime = LastSchemaChange[editor.Database];
                }

                // Query the schema for it's last know modification
                DetectDbChangesThread.RunWorkerAsync(options);
            }
        }


        /// <summary>
        /// Worker thread that checks if the database has recent changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DetectDbChangesThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var options = (SmoQueryRequest)e.Argument;
            var smo = new SmoController(options.Server, options.Database);
            options.Result = smo.LastSchemaChangedDate(options.StartTime);
            e.Result = options;
        }


        /// <summary>
        /// Callback for when done checking for recent database changes. If there is a newer modification time than what is recorded it requests a database objects refresh.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DetectDbChangesThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                // test the newest change is after the last known change
                var options = (SmoQueryRequest)e.Result;
                var newest = (DateTime)options.Result;
                if (options.StartTime > DateTime.MinValue && newest > options.StartTime)
                {
                    RefreshObjectsMenuClick(null, null);
                }
                // record the newest
                LastSchemaChange[options.Database] = newest;
                // restart the check timer
                CheckDbForChanges.Start();
            }
            else
            {
                if (e.Error is ConnectionFailureException)
                {
                    var message = Utilities.LogException(e.Error, true);
                    message = message.Replace(Environment.NewLine, " ");
                    ShowInfo(message, InfoType.Error);
                    CheckDbForChanges.Start();
                }
                else
                {
                    Utilities.LogException(e.Error);
                }
            }
        }


        /// <summary>
        /// Sends a key with flags (control, alt, shift) to the active code editor.
        /// This is used to pass along actions from the main window.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="alt"></param>
        /// <param name="shift"></param>
        /// <param name="keys"></param>
        private void SendKeysToCurrentEditor(bool control, bool alt, bool shift, Keys keys)
        {
            var editor = this.ActiveEditor();
            if (editor != null)
            {
                editor.ProcessKey(control, alt, shift, keys);
            }
        }


        #region Menu Click Event Handlers


        /// <summary>
        /// Opens the selected recent item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenRecentButton_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.ShowEditor(e.ClickedItem.ToolTipText, true);
        }


        /// <summary>
        /// A database is selected from the database list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DatabaseListDropdown_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            this.ApplyDatabaseToActiveEditor(e.ClickedItem.Text);
        }


        /// <summary>
        /// Open a new query tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NewQueryMenuClick(object sender, EventArgs e)
        {
            Utilities.LogAction("Open new editor");
            this.ShowEditor(string.Empty, false);
        }


        /// <summary>
        /// Show the open file dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenQueryMenuClick(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Program.rc.GetString("recentpath", string.Empty);
            openFileDialog.Filter = "SQL Queries (*.sql)|*.sql|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                Program.rc["recentpath"] = Path.GetDirectoryName(openFileDialog.FileName);
                string FileName = openFileDialog.FileName;
                Utilities.LogAction("Open file {0}", FileName);
                this.ShowEditor(FileName, true);
            }
        }

        /// <summary>
        /// Save menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveQueryAsMenuClick(object sender, EventArgs e)
        {
            var editor = this.ActiveEditor();
            if (editor == null) return;
            editor.SaveQueryAs();
        }

        /// <summary>
        /// Save menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveQueryAsSnippetMenuClick(object sender, EventArgs e)
        {
            var editor = this.ActiveEditor();
            if (editor == null) return;
            editor.SaveQueryAs(Utilities.Storage("snippets", ""));
        }

        /// <summary>
        /// Exit menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QuitMenuClick(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Cut menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CutMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.X);
        }

        /// <summary>
        /// Copy menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.C);
        }

        /// <summary>
        /// Paste menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PasteMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.V);
        }

        /// <summary>
        /// Undo menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UndoMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.Z);
        }


        /// <summary>
        /// Redo menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RedoMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.Y);
        }

        /// <summary>
        /// Select all menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectAllMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.A);
        }


        /// <summary>
        /// Select code block menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectBlockMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.F5);
        }


        /// <summary>
        /// Display the script window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScriptDataMenuClick(object sender, EventArgs e)
        {
            ShowGenerateScriptsWindow(null);
        }


        /// <summary>
        /// Send an Execute signal to the active query window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExecuteQueryButton_Click(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(false, false, false, Keys.F5);
        }


        /// <summary>
        /// Send a stop execution signal to the active query window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopExecutionButton_Click(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(false, true, false, Keys.Pause);
        }

        /// <summary>
        /// Show the snippet editor window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshSnippetsMenuClick(object sender, EventArgs e)
        {
            Program.ReadSnippets();
        }


        /// <summary>
        /// Show the snippets folder in explorer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowSnippetsMenuClick(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("explorer.exe", Utilities.Storage("snippets", ""));
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Show the auto complete snippets
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SnippetAutoCompleteMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.K);
        }


        /// <summary>
        /// Show the auto complete snippets
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectAutoCompleteMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.J);
        }


        /// <summary>
        /// Display the text highlight tool
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FindTextMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.F);
        }


        /// <summary>
        /// Display the text highlight tool
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReplaceTextMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, false, Keys.H);
        }


        /// <summary>
        /// Show the about dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AboutMenuClick(object sender, EventArgs e)
        {
            var abt = new AboutWindow();
            abt.ShowDialog();
        }


        /// <summary>
        /// Show the online wiki URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WikiMenuClick(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(@"https://bitbucket.org/wesleywerner/sqlant/wiki/");
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Save the current editor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveQueryMenuClick(object sender, EventArgs e)
        {
            var editor = this.ActiveEditor();
            if (editor != null) editor.SaveQuery();
        }


        /// <summary>
        /// Close the current editor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseQueryMenuClick(object sender, EventArgs e)
        {
            var editor = this.ActiveEditor();
            if (editor != null) editor.CloseQuery();
        }


        /// <summary>
        /// Toggle word wrap in the active editor.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WordWrapMenuClick(object sender, EventArgs e)
        {
            var editor = this.ActiveEditor();
            if (editor != null)
            {
                if (editor.scintilla1.WrapMode == ScintillaNET.WrapMode.None)
                {
                    editor.scintilla1.WrapMode = ScintillaNET.WrapMode.Word;
                    WordWrapMenuItem.Checked = true;
                }
                else
                {
                    editor.scintilla1.WrapMode = ScintillaNET.WrapMode.None;
                    WordWrapMenuItem.Checked = false;
                }
            }
        }


        /// <summary>
        /// Trigger the database object list refresh for the active window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshObjectsMenuClick(object sender, EventArgs e)
        {
            var editor = this.ActiveEditor();
            if (editor != null) editor.RequestObjectList(true);
        }


        /// <summary>
        /// Refresh the database list for the current server
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RefreshDatabasesMenuClick(object sender, EventArgs e)
        {
            if (DatabaseList == null) return;
            var server = ServerListDropdown.Text;
            if (DatabaseList.ContainsKey(server))
            {
                DatabaseList[server].Clear();
                ConnectToServer(server, null);
            }
        }


        /// <summary>
        /// Set the current server and database as the default for startup.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetDefaultDbMenuClick(object sender, EventArgs e)
        {
            var builder = new SqlConnectionStringBuilder(this.SelectedConnection());
            if (builder.DataSource != string.Empty)
            {
                Program.rc["default server"] = builder.DataSource;
                if (DatabaseListDropdown.Text != "Database")
                {
                    Program.rc["default database"] = DatabaseListDropdown.Text;
                }
                // Check the "Set as default" menu if this is the default database
                DefaultDbMenuItem.Checked = true;
            }
        }


        /// <summary>
        /// Show the audit trail search window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchAuditTrailMenuClick(object sender, EventArgs e)
        {
            var wnd = new AuditSearchWindow();
            wnd.Show();
        }


        /// <summary>
        /// Sends a keypress event to the active editor to focus the object tree filter input.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FocusDatabaseObjectsFilterMenuClick(object sender, EventArgs e)
        {
            SendKeysToCurrentEditor(true, false, true, Keys.F);
        }


        /// <summary>
        /// Show the program settings window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProgramSettingsMenuClick(object sender, EventArgs e)
        {
            var wnd = new SettingsWindow();
            wnd.ShowDialog(this);
        }


        #endregion


        /// <summary>
        /// Checks if it is about time for another cleanup
        /// </summary>
        /// <param name="forced">Run the check regardless when the last check was done</param>
        internal void RunCleanupCheck(bool forced = false)
        {
            DateTime dt = Program.rc.GetDate("clean checked", DateTime.MinValue);

            if (dt == DateTime.MinValue)
            {
                Program.rc["clean checked"] = DateTime.Today.ToLongDateString();
                return;
            }

            if (forced || (DateTime.Today - dt).TotalDays > Program.rc.GetInt("clean n days", 14))
            {
                AutoCleanerWorker.RunWorkerAsync();
            }

        }

        /// <summary>
        /// Analyse files to clean
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoCleanerWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var options = new Cleaner.CleanerOptions();
            options.Age = Program.rc.GetInt("clean age", 60);
            options.Paths = new string[] {
                Utilities.Storage("audits",""),
                Utilities.Storage("cache",""),
                Utilities.Storage("errors",""),
                Utilities.Storage("logs","")
            };
            var cleaner = new Cleaner.CleanerAnalysis(options);
            cleaner.StartAnalysis();
            e.Result = cleaner;
        }

        /// <summary>
        /// Cleaner analysis complete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoCleanerWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var cleaner = (Cleaner.CleanerAnalysis)e.Result;
            AutoCleanerButton.Visible = cleaner.Results.Count > 0;
            long size = cleaner.Results.Select(m => m.Length).Sum();
            AutoCleanerButton.ToolTipText = string.Format("There is {0} in {1} files that can be cleaned.", Formatters.FormatBytes(size), cleaner.Results.Count);
            AutoCleanerButton.Tag = cleaner;
        }


        /// <summary>
        /// Show the cleaner window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutoCleanerButton_Click(object sender, EventArgs e)
        {
            var cleanerWnd = new Windows.AutoCleanerWindow((Cleaner.CleanerAnalysis)((ToolStripButton)sender).Tag);
            cleanerWnd.Show(this);
            AutoCleanerButton.Visible = false;
        }

        /// <summary>
        /// Show the find dialog
        /// </summary>
        /// <param name="target"></param>
        public void ShowFindDialog(ScintillaNET.Scintilla target)
        {
            if (MyFindReplace == null) MyFindReplace = new FindReplace();
            MyFindReplace.Scintilla = target;
            MyFindReplace.ShowFind();
        }

        /// <summary>
        /// Show the replace dialog
        /// </summary>
        /// <param name="target"></param>
        public void ShowReplaceDialog(ScintillaNET.Scintilla target)
        {
            if (MyFindReplace == null) MyFindReplace = new FindReplace();
            MyFindReplace.Scintilla = target;
            MyFindReplace.ShowReplace();
        }

        /// <summary>
        /// Find the next word
        /// </summary>
        /// <param name="target"></param>
        public void FindNext(ScintillaNET.Scintilla target)
        {
            if (MyFindReplace == null) MyFindReplace = new FindReplace();
            MyFindReplace.Scintilla = target;
            MyFindReplace.Window.FindNext();
        }

        /// <summary>
        /// Start the update check, if it has not checked in the last n hours.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateCheckerTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                var checker = new UpdateChecker.UpdateChecker();
                if (checker.ShouldCheckForUpdates())
                {
                    Utilities.LogAction("Checking for updates online...");
                    UpdateCheckerThread.RunWorkerAsync();
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }

        /// <summary>
        /// Fetch the version information online.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateCheckerThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var checker = new UpdateChecker.UpdateChecker();
            e.Result = checker.GetOnlineVersionInfo();
        }

        /// <summary>
        /// Process the version information and decide what to do.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateCheckerThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {
                    // remember the time checked
                    Program.rc["update last checked"] = DateTime.Now.ToString();

                    var updateResult = (UpdateChecker.UpdateCheckResult)e.Result;
                    if (updateResult.UpdateAvailable())
                    {
                        Utilities.LogAction("There are updates available. Go to {0}", updateResult.SuggestedUpdateUrl());
                        // show update button
                        UpdateUrlButton.Tag = updateResult.SuggestedUpdateUrl();
                        UpdateUrlButton.Visible = true;
                    }
                    else
                    {
                        Utilities.LogAction("No updates available.");
                    }
                }
                else
                {
                    Utilities.LogException(e.Error, true);
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }

        /// <summary>
        /// Open the update URL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateUrlButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (UpdateUrlButton.Tag != null)
                {
                    System.Diagnostics.Process.Start(UpdateUrlButton.Tag.ToString());
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Timer event that updates the process memory usage status label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MemoryUsageTimer_Tick(object sender, EventArgs e)
        {
            if (performanceCounter1.InstanceName == string.Empty)
            {
                
                using (Process p = Process.GetCurrentProcess())
                {
                    performanceCounter1.InstanceName = p.ProcessName;
                    performanceCounter1.NextValue();
                }
            }
            string unit = "MB";
            var mem = Math.Round(performanceCounter1.NextValue() / 1000000);
            if (mem > 1000)
            {
                unit = "GB";
                mem = Math.Round(mem / 1000, 1);
            }
            MemoryUsageStatusLabel.Text = string.Format("memory: {0}{1}", mem, unit);
        }


    }
}
