﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SQLAnt
{
    public partial class ScriptObjectsWindow : Form
    {
        private DatabaseMetadata meta { get; set; }
        private string Connectionstring { get; set; }
        private string Database { get; set; }
        private Dictionary<string, string> selected { get; set; }
        public bool SelectedListLoading { get; private set; }

        /// <summary>
        /// Gets the containing form
        /// </summary>
        /// <returns></returns>
        private StudioWindow ParentFormCast()
        {
            return (StudioWindow)this.Owner;
        }

        public ScriptObjectsWindow(string connectionString, string database, DatabaseMetadata meta, Dictionary<string, string> preselected)
        {
            InitializeComponent();
            this.Icon = SQLAnt.Properties.Resources.ant;
            this.meta = meta;
            this.Connectionstring = connectionString;
            this.Database = database;
            this.selected = preselected ?? new Dictionary<string, string>();
            GenerateScriptButton.Image = SQLAnt.Properties.Resources.media_playback_start;
        }

        private void ScriptTableWindow_Load(object sender, EventArgs e)
        {
            ObjectFilterPanel.Dock = DockStyle.Top;
            ObjectFilterLabel.Dock = DockStyle.Left;
            ObjectTreeFilterInput.Dock = DockStyle.Fill;
            ObjectTree.Dock = DockStyle.Fill;
            ObjectTreeFilterInput.Dock = DockStyle.Fill;
            ObjectTree.Dock = DockStyle.Fill;
            ScriptSplitContainer.Dock = DockStyle.Fill;
            SelectedItemsList.Dock = DockStyle.Fill;

            SelectedItemsList.Columns.Add("Name", 250);
            SelectedItemsList.Columns.Add("Type", 80);

            RefreshObjectTree(string.Empty);
            DisplaySelectedDictionary();
        }

        private void GenerateScriptButton_Click(object sender, EventArgs e)
        {
            var request = new ScriptRequest();
            request.options = new ScriptingOptions()
            {
                ScriptData = ScriptDataCheck.Checked,
                ScriptSchema = ScriptSchemaCheck.Checked
            };

            foreach (var obj in selected)
            {
                if (obj.Value == "table")
                {
                    request.Tables.Add(obj.Key);
                }
                else if (obj.Value == "view")
                {
                    request.Views.Add(obj.Key);
                }
                else if (obj.Value == "sproc")
                {
                    request.Procedures.Add(obj.Key);
                }
            }

            int itemsToListInLog = 5;
            var combinedItems = request.Tables.Concat(request.Views).Concat(request.Procedures);
            int totalItems = combinedItems.Count();
            string unlistedItemSummary = totalItems > itemsToListInLog ? string.Format(" and {0} others", totalItems - itemsToListInLog) : "";
            string itemsList = string.Join(", ", combinedItems.Take(itemsToListInLog));
            Utilities.LogAction("Scripting {0}{1}.", itemsList, unlistedItemSummary);

            GenerateScriptButton.Enabled = false;
            BusyProgress.Style = ProgressBarStyle.Marquee;
            Application.DoEvents();
            ScriptThread.RunWorkerAsync(request);
        }

        private void ScriptThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var request = (ScriptRequest)e.Argument;
            var smo = new SmoController(this.Connectionstring, this.Database);
            foreach (string tablename in request.Tables)
            {
                request.Scripts.Add(smo.ScriptTable(tablename, request.options));
            }
            foreach (string viewname in request.Views)
            {
                request.Scripts.Add(smo.ScriptView(viewname, request.options));
            }
            foreach (string procname in request.Procedures)
            {
                request.Scripts.Add(smo.ScriptProcedure(procname, request.options));
            }
            e.Result = request;
        }

        private void ScriptThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            GenerateScriptButton.Enabled = true;
            BusyProgress.Style = ProgressBarStyle.Blocks;
            BusyProgress.Value = BusyProgress.Maximum;

            if (e.Error != null)
            {
                Utilities.LogException(e.Error);
            }
            else
            {
                var request = (ScriptRequest)e.Result;
                string tempfile = Path.GetTempFileName();
                var writer = File.AppendText(tempfile);
                foreach (StringBuilder sb in request.Scripts)
                {
                    writer.WriteLine(sb.ToString());
                }
                writer.Flush();
                writer.Close();
                this.ParentFormCast().ShowEditor(tempfile, false);
            }
        }

        private void ScriptTableWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing && ScriptThread.IsBusy)
            {
                e.Cancel = true;
            }
        }


        /// <summary>
        /// Apply the object tree filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectTreeFilterTimer_Tick(object sender, EventArgs e)
        {
            ObjectTreeFilterTimer.Stop();
            RefreshObjectTree(ObjectTreeFilterInput.Text);
        }


        /// <summary>
        /// Changes to the object tree filter triggers the filter action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectTreeFilterInput_TextChanged(object sender, EventArgs e)
        {
            ObjectTreeFilterTimer.Stop();
            ObjectTreeFilterTimer.Start();
        }

        /// <summary>
        /// Load database object into the navigation tree
        /// </summary>
        private void RefreshObjectTree(string filter)
        {
            if (this.meta == null) return;

            try
            {
                var hasfilter = filter != string.Empty;
                filter = filter.ToLower();
                this.ObjectTree.Nodes.Clear();

                // Tables
                var tablesParent = new TreeNode("Tables");
                tablesParent.ImageKey = "table.png";
                tablesParent.SelectedImageKey = "table.png";
                foreach (DataTable t in this.meta.Tables.Tables)
                {
                    var tableFilterMatch = false;
                    var tnode = new TreeNode(meta.QualifyFullName(t.TableName));
                    tnode.Tag = "table";
                    tnode.ImageKey = "table.png";
                    tnode.SelectedImageKey = "table.png";
                    tnode.Checked = selected.ContainsKey(tnode.Text);
                    tableFilterMatch = t.TableName.ToLower().Contains(filter);
                    if (hasfilter && tableFilterMatch) tnode.BackColor = Color.Yellow;
                    if (hasfilter == false || tableFilterMatch) tablesParent.Nodes.Add(tnode);
                }
                this.ObjectTree.Nodes.Add(tablesParent);
                tablesParent.Expand();

                // Views
                var viewsParent = new TreeNode("Views");
                viewsParent.ImageKey = "view.png";
                viewsParent.SelectedImageKey = "view.png";
                foreach (DataTable t in this.meta.Views.Tables)
                {
                    var tableFilterMatch = false;
                    var tnode = new TreeNode(meta.QualifyFullName(t.TableName));
                    tnode.Tag = "view";
                    tnode.ImageKey = "view.png";
                    tnode.SelectedImageKey = "view.png";
                    tnode.Checked = selected.ContainsKey(tnode.Text);
                    tableFilterMatch = t.TableName.ToLower().Contains(filter);
                    if (hasfilter && tableFilterMatch) tnode.BackColor = Color.Yellow;
                    if (hasfilter == false || tableFilterMatch) viewsParent.Nodes.Add(tnode);
                }
                this.ObjectTree.Nodes.Add(viewsParent);

                // Procedures
                var procParent = new TreeNode("Procedures");
                procParent.ImageKey = "procedure.png";
                procParent.SelectedImageKey = "procedure.png";
                foreach (DataTable t in this.meta.Procedures.Tables)
                {
                    var tableFilterMatch = false;
                    var tnode = new TreeNode(meta.QualifyFullName(t.TableName));
                    tnode.Tag = "sproc";
                    tnode.ImageKey = "procedure.png";
                    tnode.SelectedImageKey = "procedure.png";
                    tnode.Checked = selected.ContainsKey(tnode.Text);
                    tableFilterMatch = t.TableName.ToLower().Contains(filter);
                    if (hasfilter && tableFilterMatch) tnode.BackColor = Color.Yellow;
                    if (hasfilter == false || tableFilterMatch) procParent.Nodes.Add(tnode);
                }
                this.ObjectTree.Nodes.Add(procParent);

                // Functions
                var funcParent = new TreeNode("Functions");
                funcParent.ImageKey = "procedure.png";
                funcParent.SelectedImageKey = "procedure.png";
                foreach (DataTable t in this.meta.Functions.Tables)
                {
                    var tableFilterMatch = false;
                    var tnode = new TreeNode(meta.QualifyFullName(t.TableName));
                    tnode.Tag = "function";
                    tnode.ImageKey = "procedure.png";
                    tnode.SelectedImageKey = "procedure.png";
                    tnode.Checked = selected.ContainsKey(tnode.Text);
                    tableFilterMatch = t.TableName.ToLower().Contains(filter);
                    if (hasfilter && tableFilterMatch) tnode.BackColor = Color.Yellow;
                    if (hasfilter == false || tableFilterMatch) funcParent.Nodes.Add(tnode);
                }
                this.ObjectTree.Nodes.Add(funcParent);

                // auto expand on filter
                if (hasfilter)
                {
                    tablesParent.ExpandAll();
                    viewsParent.ExpandAll();
                    procParent.ExpandAll();
                    tablesParent.EnsureVisible();
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }

        /// <summary>
        /// manage the selected dictionary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectTree_AfterCheck(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag == null)
            {
                // Check/Uncheck all child nodes
                foreach (TreeNode child in e.Node.Nodes)
                {
                    child.Checked = e.Node.Checked;
                }
            }
            else
            {
                // Manage the selected objects list
                string key = e.Node.Text;
                if (!e.Node.Checked && selected.ContainsKey(key)) selected.Remove(key);
                if (e.Node.Checked && !selected.ContainsKey(key)) selected.Add(key, e.Node.Tag.ToString());
                SelectedObjectsDisplayTimer.Stop();
                SelectedObjectsDisplayTimer.Start();
            }
        }

        /// <summary>
        /// Trigger selected items display
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedObjectsDisplayTimer_Tick(object sender, EventArgs e)
        {
            SelectedObjectsDisplayTimer.Stop();
            DisplaySelectedDictionary();
        }

        /// <summary>
        /// List the selected items
        /// </summary>
        private void DisplaySelectedDictionary()
        {
            SelectedListLoading = true;
            SelectedItemsList.Items.Clear();
            foreach (var obj in selected)
            {
                var item = new ListViewItem(obj.Key);
                item.Name = obj.Key;
                item.SubItems.Add(obj.Value);
                item.Checked = true;
                SelectedItemsList.Items.Add(item);
            }
            SelectedListLoading = false;
        }

        /// <summary>
        /// Remove a selected item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectedItemsList_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!SelectedListLoading)
            {
                string key = e.Item.Name;
                // manage selected list
                if (selected.ContainsKey(key)) selected.Remove(key);
                // manage tree
                foreach (TreeNode topnode in ObjectTree.Nodes)
                {
                    foreach (TreeNode childnode in topnode.Nodes)
                    {
                        if (childnode.Text == key) childnode.Checked = false;
                    }
                }
            }
        }


    }
}
