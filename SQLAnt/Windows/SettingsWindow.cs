﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLAnt
{
    public partial class SettingsWindow : Form
    {
        public SettingsWindow()
        {
            InitializeComponent();
            this.Icon = SQLAnt.Properties.Resources.ant;

            AuditingEnabled.Checked = Program.rc.GetBool("audit enabled", true);
            AuditFormat.Text = Program.rc.GetString("audit format", "csv").ToUpper();
            AutoQualifySchemaNamesCheck.Checked = Program.rc.GetBool("auto complete schema names", true);
            AutoSaveOnCloseCheck.Checked = Program.rc.GetBool("auto save on exit", true);
            AutoDetectSchemaChangesCheck.Checked = Program.rc.GetBool("auto refresh schema", true);
            CleanCheckInput.Value = Program.rc.GetInt("clean n days", 14);
            CleanAgeInput.Value = Program.rc.GetInt("clean age", 60);
            AutoCapitalizeKeywordsCheck.Checked = Program.rc.GetBool("auto cap keywords", true);
            RememberOpenFilesCheck.Checked = Program.rc.GetBool("remember open files", true);
            UpdateIntervalInput.Value = Program.rc.GetInt("update check interval", 24);
            StableUpdatesCheck.Checked = Program.rc.GetBool("stable updates", true);
            RowcountLimitInput.Value = Program.rc.GetInt("rowcount", 100000);
        }

        private void AuditBrowse_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(Utilities.Storage("audits", ""));
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            Program.rc["audit format"] = AuditFormat.Text.ToLower();
            Program.rc["audit enabled"] = AuditingEnabled.Checked.ToString();
            Program.rc["auto complete schema names"] = AutoQualifySchemaNamesCheck.Checked.ToString();
            Program.rc["auto save on exit"] = AutoSaveOnCloseCheck.Checked.ToString();
            Program.rc["auto refresh schema"] = AutoDetectSchemaChangesCheck.Checked.ToString();
            Program.rc["clean n days"] = CleanCheckInput.Value.ToString();
            Program.rc["clean age"] = CleanAgeInput.Value.ToString();
            Program.rc["auto cap keywords"] = AutoCapitalizeKeywordsCheck.Checked.ToString();
            Program.rc["remember open files"] = RememberOpenFilesCheck.Checked.ToString();
            Program.rc["update check interval"] = UpdateIntervalInput.Value.ToString();
            Program.rc["stable updates"] = StableUpdatesCheck.Checked.ToString();
            Program.rc["rowcount"] = RowcountLimitInput.Value.ToString();
            Program.WriteConfiguration();
        }


        /// <summary>
        /// Manual cleaner run
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RunCleanerCheckButton_Click(object sender, EventArgs e)
        {
            ((StudioWindow)this.Owner).RunCleanupCheck(true);
        }


        /// <summary>
        /// Toggle the updates disabled label.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateIntervalInput_ValueChanged(object sender, EventArgs e)
        {
            UpdatesDisabledLabel.Visible = (UpdateIntervalInput.Value == 0);
        }

    }
}
