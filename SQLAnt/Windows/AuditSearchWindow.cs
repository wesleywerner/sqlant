﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;
using System.Text.RegularExpressions;

namespace SQLAnt
{
    public partial class AuditSearchWindow : Form
    {

        /// <summary>
        /// Tracks the position of which search result is listed during search progress.
        /// </summary>
        private int lastSearchDisplayPosition;

        public AuditSearchWindow()
        {
            InitializeComponent();
            this.Icon = SQLAnt.Properties.Resources.ant;
            this.WindowState = FormWindowState.Maximized;

            // Configure results list columns
            ResultFiles.FullRowSelect = true;
            ResultFiles.HideSelection = false;
            ResultFiles.Columns.Add("File");
            ResultFiles.Columns.Add("Date");
            ResultFiles.Columns.Add("Size");

            // Dock context displays
            splitContextQueryAndData.Dock = DockStyle.Fill;
            QueryContextDisplay.Dock = DockStyle.Fill;
            DataContextDisplay.Dock = DockStyle.Fill;
        }

        /// <summary>
        /// Start the search
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartSearchButton_Click(object sender, EventArgs e)
        {
            if (SearchThread.IsBusy) return;
            StartSearchButton.Enabled = false;
            StopSearchButton.Enabled = true;
            ResultFiles.Items.Clear();
            lastSearchDisplayPosition = 0;
            SearchProgress.Value = 0;
            SearchProgress.Style = ProgressBarStyle.Blocks;
            Application.DoEvents();
            ResultFiles.BeginUpdate();
            SearchThread.RunWorkerAsync(SearchInput.Text);
        }

        /// <summary>
        /// Searching audit archives thread.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchThread_DoWork(object sender, DoWorkEventArgs e)
        {
            var options = new AuditSearchOptions();
            options.Path = Utilities.Storage("audits", "");
            options.SearchData = SearchResultsCheck.Checked;
            options.Filter = e.Argument.ToString().ToLower();

            var audit = new SmoAuditing();
            var state = audit.InitializeSearch(options);
            
            while (!state.Complete && SearchThread.CancellationPending == false)
            {
                state = audit.SearchNext(state);
                SearchThread.ReportProgress((int)((state.Position / (decimal)(state.Total - 1)) * 100), state.Matches);
            }

            e.Result = state.Matches;
        }

        /// <summary>
        /// Reports search progress
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            SearchProgress.Value = e.ProgressPercentage;
            var matches = (List<AuditSearchInfo>)e.UserState;
            for (int i = lastSearchDisplayPosition; i < matches.Count; i++)
            {
                var info = matches[i];
                var item = new ListViewItem(Path.GetFileName(info.Filename));
                item.SubItems.Add(info.Date.ToString("yyyy-MM-dd HH:mm"));
                item.SubItems.Add(Formatters.FormatBytes(info.Size));
                item.Tag = info;
                ResultFiles.Items.Add(item);
            }
            lastSearchDisplayPosition = matches.Count;
        }

        /// <summary>
        /// Searching completed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StartSearchButton.Enabled = true;
            StopSearchButton.Enabled = false;
            ResultFiles.EndUpdate();
            ResultFiles.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);
        }

        /// <summary>
        /// Selecting a search result file populates the query text.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResultFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ResultFiles.SelectedItems.Count == 1)
            {
                var info = (AuditSearchInfo)ResultFiles.SelectedItems[0].Tag;
                QueryContextDisplay.Text = info.Query;
                DataContextDisplay.Text = info.Data;
                OpenAuditArchiveButton.Enabled = true;
                HighlightSearchWords();
            }
            else
            {
                OpenAuditArchiveButton.Enabled = false;
            }
        }

        /// <summary>
        /// Highlights the searched text in the query and data displays
        /// </summary>
        private void HighlightSearchWords()
        {
            var match = Regex.Match(QueryContextDisplay.Text, SearchInput.Text, RegexOptions.IgnoreCase);
            
            if (match.Success)
            {
                QueryContextDisplay.SelectionStart = match.Index;
                QueryContextDisplay.SelectionLength = match.Length;
                QueryContextDisplay.ScrollToCaret();
            }

            match = Regex.Match(DataContextDisplay.Text, SearchInput.Text, RegexOptions.IgnoreCase);
            
            if (match.Success)
            {
                DataContextDisplay.SelectionStart = match.Index;
                DataContextDisplay.SelectionLength = match.Length;
                DataContextDisplay.ScrollToCaret();
            }
        }

        /// <summary>
        /// Attempt to open the selected archive
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenAuditArchiveButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (ResultFiles.SelectedItems.Count == 1)
                {
                    var info = (AuditSearchInfo)ResultFiles.SelectedItems[0].Tag;
                    System.Diagnostics.Process.Start(info.Filename);
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }

        /// <summary>
        /// Start search when hitting enter on the search input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && StartSearchButton.Enabled)
            {
                StartSearchButton.PerformClick();
            }
        }

        /// <summary>
        /// Stop the search in progress
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StopSearchButton_Click(object sender, EventArgs e)
        {
            if (SearchThread.IsBusy && !SearchThread.CancellationPending)
            {
                SearchThread.CancelAsync();
            }
        }
    }
}
