﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Threading;

namespace SQLAnt
{
    internal static class Utilities
    {

        internal static void LogAction(string detail, params object[] args)
        {
            // prevent concurrent log file writes
            EventWaitHandle waitHandle = new EventWaitHandle(true, EventResetMode.AutoReset, "2517e8e7-844a-43d2-b651-7ff168c778b8");
            waitHandle.WaitOne();
            string logfile = Storage("logs", DateTime.Now.ToString("yyyy-MM-dd") + ".txt");
            string time = DateTime.Now.ToString("HH:mm:ss");
            File.AppendAllText(logfile, time + ": " + string.Format(detail, args) + Environment.NewLine);
            waitHandle.Set();
        }

        /// <summary>
        /// Logs exception to file and display the error dialog
        /// </summary>
        /// <param name="ex"></param>
        internal static string LogException(Exception ex, bool silent = false)
        {
            // Log exeption to an errors directory
            // Log a courtesy detail with LogAction
            string cleanMessage = ExceptionHelper.Clean(ex);
            string logfile = Storage("errors", DateTime.Now.ToString("yyyy-MM-dd") + ".txt");
            string time = DateTime.Now.ToString("HH:mm:ss");
            File.AppendAllText(logfile, time + ": " + cleanMessage + Environment.NewLine + ex.ToString() + Environment.NewLine);

            if (!silent)
            {
                var f = new ExceptionWindow(cleanMessage, logfile);
                f.Show();
            }
            return cleanMessage;
        }

        /// <summary>
        /// Create the local store path.
        /// </summary>
        internal static void InitStoragePath()
        {
        }

        /// <summary>
        /// Get the full storage path for a file.
        /// </summary>
        /// <param name="subdirectory"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        internal static string Storage(string subdirectory, string filename)
        {
            string directorypath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "SQL Ant", subdirectory);
            string fullpath = System.IO.Path.Combine(directorypath, CleanPath(filename));
            if (!Directory.Exists(directorypath))
            {
                Directory.CreateDirectory(directorypath);
            }
            return fullpath;
        }

        /// <summary>
        /// Remove invalid file characters from a path.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="replacementChar"></param>
        /// <returns></returns>
        internal static string CleanPath(string path, char replacementChar = ' ')
        {
            foreach (char invalidChar in System.IO.Path.GetInvalidFileNameChars())
            {
                path = path.Replace(invalidChar, replacementChar);
            }
            return path;
        }

        /// <summary>
        /// Writes key value pairs to file.
        /// </summary>
        /// <param name="KeyValuePairs"></param>
        /// <param name="INIFilePath"></param>
        internal static void WriteINIFile(Dictionary<string, string> KeyValuePairs, string INIFilePath)
        {
            var inifile = new System.IO.StreamWriter(INIFilePath);
            foreach (string key in KeyValuePairs.Keys)
            {
                string convertedvalue = Regex.Replace(KeyValuePairs[key], @"\r\n?|\n", "{NEWLINE}");
                inifile.WriteLine("{0}={1}", key, convertedvalue);
            }
            inifile.Flush();
            inifile.Close();
        }

        /// <summary>
        /// Reads key value pairs from file.
        /// </summary>
        /// <param name="INIFilePath"></param>
        /// <returns></returns>
        internal static Dictionary<string, string> ReadINIFile(string INIFilePath)
        {
            Dictionary<string, string> KeyValuePairs = new Dictionary<string, string>();
            if (!System.IO.File.Exists(INIFilePath)) { return KeyValuePairs; }
            var inifile = new System.IO.StreamReader(INIFilePath);
            var line = inifile.ReadLine();
            while (!string.IsNullOrEmpty(line))
            {
                int separator = line.IndexOf('=');
                if (separator > 0)
                {
                    string key = line.Slice(0, separator);
                    string value = line.Slice(separator + 1, line.Length);
                    string convertedvalue = value.Replace("{NEWLINE}", Environment.NewLine);
                    KeyValuePairs.Add(key, convertedvalue);
                }
                line = inifile.ReadLine();
            }
            inifile.Close();

            return KeyValuePairs;
        }


        /// <summary>
        /// Reads a list of strings from file.
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        internal static List<string> ReadListFile(string FilePath)
        {
            if (!System.IO.File.Exists(FilePath)) { return new List<string>(); }
            var inifile = new System.IO.StreamReader(FilePath);
            var lines = File.ReadAllLines(FilePath, System.Text.ASCIIEncoding.UTF8);
            return new List<string>(lines);
        }


        /// <summary>
        /// Writes a list of strings to file.
        /// </summary>
        /// <param name="FilePath"></param>
        /// <param name="lines"></param>
        internal static void WriteListFile(string FilePath, List<string> lines)
        {
            File.WriteAllLines(FilePath, lines, System.Text.ASCIIEncoding.UTF8);
        }

    }

    public static class Extensions
    {
        /// <summary>
        /// Get the string slice between the two indexes.
        /// Inclusive for start index, exclusive for end index.
        /// </summary>
        public static string Slice(this string source, int start, int end)
        {
            if (end < 0) // Keep this for negative end support
            {
                end = source.Length + end;
            }
            int len = end - start;               // Calculate length
            return source.Substring(start, len); // Return Substring of length
        }

        /// <summary>
        /// Get the array slice between the two indexes.
        /// ... Inclusive for start index, exclusive for end index.
        /// </summary>
        public static T[] Slice<T>(this T[] source, int start, int end)
        {
            // Handles negative ends.
            if (end < 0)
            {
                end = source.Length + end;
            }
            int len = end - start;

            // Return new array.
            T[] res = new T[len];
            for (int i = 0; i < len; i++)
            {
                res[i] = source[i + start];
            }
            return res;
        }

        public static string CsvEscape(this string value)
        {
            if (value.Contains(","))
            {
                return "\"" + value.Replace("\"", "\"\"") + "\"";
            }
            return value;
        }
    }
}
