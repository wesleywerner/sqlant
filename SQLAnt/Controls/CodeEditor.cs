﻿// SQLAnt query editor
// Copyright (C) 2016 Wesley Werner
// https://bitbucket.org/wesleywerner/sqlant
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using ScintillaNET;

namespace SQLAnt
{
    public partial class CodeEditor : UserControl
    {

        Form parent;

        /// <summary>
        /// List of auto complete icon values
        /// </summary>
        const string AUTO_ICON_TABLE = "?0";
        const string AUTO_ICON_SPROC = "?1";
        const string AUTO_ICON_VIEW = "?2";
        const string AUTO_ICON_SNIPPET = "?3";
        const string AUTO_ICON_ALIAS = "?4";

        /// <summary>
        /// Scan at most this many words before and after the cursor when building the auto complete list for tables or aliases to reference for column names.
        /// </summary>
        const int AUTO_COMPLETE_SCAN_RANGE = 500;

        /// <summary>
        /// Stores table aliases
        /// </summary>
        private Dictionary<string, string> TableAliases;


        /// <summary>
        /// Stores the list of database objects for auto completion
        /// </summary>
        private List<string> MetaWordlist = null;


        /// <summary>
        /// Provides database schema metadata
        /// </summary>
        private DatabaseMetadata meta;


        /// <summary>
        /// Defines the sql keywords for syntax higlighting, and for auto completion.
        /// Compiled from https://msdn.microsoft.com/en-us/library/ms189822.aspx and https://msdn.microsoft.com/en-us/library/ms187752.aspx
        /// </summary>
        private string SqlKeywords = "add all alter and any as asc authorization backup begin between bigint binary bit break browse bulk by cascade case char check checkpoint close clustered coalesce collate column commit compute constraint contains containstable continue convert count create cross current current_date current_time current_timestamp current_user cursor database date datetime datetime2 datetimeoffset dbcc deallocate decimal declare default delete deny desc disk distinct distributed double drop dump else end errlvl escape except exec execute exists exit external fetch file fillfactor float for foreign freetext freetexttable from full function go goto grant group having hierarchyid holdlock identity identitycol identity_insert if image in index inner insert int intersect into is join key kill left like lineno load merge money national nchar nocheck nonclustered not ntext null nullif numeric nvarchar object_id of off offsets on open opendatasource openquery openrowset openxml option or order outer over percent pivot plan precision primary print proc procedure public raiserror read readtext real reconfigure references replication restore restrict return revert revoke right rollback rowcount rowguidcol rule save schema securityaudit select semantickeyphrasetable semanticsimilaritydetailstable semanticsimilaritytable session_user set setuser shutdown smalldatetime smallint smallmoney some sql_variant statistics system_user table tablesample text textsize then time timestamp tinyint to top tran transaction trigger truncate try_convert tsequal union unique uniqueidentifier unpivot update updatetext use user values varbinary varchar varying view waitfor when where while with within writetext xml";


        /// <summary>
        /// Stores the authentication information for the connection.
        /// </summary>
        public string Connectionstring { get; set; }


        /// <summary>
        /// Remmebers the first line of a partial selection being executed
        /// </summary>
        private int PartialExecutionSelectionLine = 0;


        /// <summary>
        /// Remembers if this query was opened as an auto-saved file.
        /// </summary>
        internal bool IsAutoSavedQuery { get; set; }


        /// <summary>
        /// Reference to the results grid control for this code editor.
        /// </summary>
        private Controls.ResultsGrid ResultsGridControl;


        /// <summary>
        /// Provides the Server name property
        /// </summary>
        private string _server = string.Empty;
        internal string Server
        {
            get
            {
                return this._server;
            }
            set
            {
                this._server = value;
                this._database = null;
                // Set server color
                if (Program.connectionColors.ContainsKey(this._server))
                {
                    string color = Program.connectionColors[this._server];
                    int icolor = 0;
                    if (int.TryParse(color, out icolor))
                    {
                        statusStrip2.BackColor = Color.FromArgb(icolor);
                    }
                }
            }
        }


        /// <summary>
        /// Provides the Database name property
        /// </summary>
        private string _database = string.Empty;
        internal string Database
        {
            get
            {
                return this._database;
            }
            set
            {
                this._database = value;
                ClearMeta();
                ObjectTree.Nodes.Clear();
                this.RequestObjectList(false);
                DatabaseLabel.Text = string.Format(@"{0}:{1}", this.Server, this.Database);
            }
        }


        /// <summary>
        /// The current file being edited, or string.empty if it is a new file.
        /// </summary>
        internal string filename = string.Empty;


        /// <summary>
        /// Gets the containing form
        /// </summary>
        /// <returns></returns>
        private StudioWindow ParentFormCast()
        {
            return (StudioWindow)this.parent;
        }


        /// <summary>
        /// Entry Point
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="parent"></param>
        public CodeEditor(string filename, Form parent)
        {
            InitializeComponent();
            AddControls();
            this.parent = parent;
            this.filename = filename;
            SidebarTabs.Dock = DockStyle.Fill;
            splitContainerEditor.Dock = DockStyle.Fill;
            ObjectFilterPanel.Dock = DockStyle.Top;
            ObjectFilterLabel.Dock = DockStyle.Left;
            ObjectTreeFilterInput.Dock = DockStyle.Fill;
            ObjectTree.Dock = DockStyle.Fill;

            // Set toolbar icons
            ObjectInfoButton.Image = SQLAnt.Properties.Resources.dialog_information;
            ObjectTreeButton.Image = SQLAnt.Properties.Resources.object_tree;

            // Hook events
            this.ObjectTree.ItemDrag += ObjectTree_ItemDrag;
            this.ObjectTree.MouseDown += ObjectTree_MouseDown;
            this.scintilla1.DragOver += Scintilla1_DragOver;

            this.SetupScintillaSyntax();
            this.SetupAutoCompleteMenu();
            splitContainerEditor.Panel2Collapsed = true;

            if (this.filename == string.Empty)
            {
                scintilla1.EmptyUndoBuffer();
                scintilla1.SetSavePoint();
                scintilla1.GotoPosition(scintilla1.Text.Length);
                this.Text = "New Query";
            }
            else
            {
                try
                {
                    string query = System.IO.File.ReadAllText(this.filename);
                    scintilla1.Text = query;
                    this.Text = System.IO.Path.GetFileName(this.filename);
                    scintilla1.EmptyUndoBuffer();
                    scintilla1.SetSavePoint();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ExceptionHelper.Clean(ex));
                }
            }
            StatusLabel.Text = "Ready";
        }

        /// <summary>
        /// List of programatically created controls
        /// </summary>
        private Timer autocompleteTimer;

        /// <summary>
        /// Add controls programatically
        /// </summary>
        private void AddControls()
        {
            // Results grid control
            ResultsGridControl = new SQLAnt.Controls.ResultsGrid();
            ResultsGridControl.FocusCodeEditor += ResultsGridControl_FocusCodeEditor;
            ResultsGridControl.ShowResultsPanel += ResultsGridControl_ShowResultsPanel;
            ResultsGridControl.HideResultsPanel += ResultsGridControl_HideResultsPanel;
            ResultsGridControl.GridMessage += ResultsGridControl_GridMessage;

            splitContainerEditor.Panel2.Controls.Add(ResultsGridControl);
            ResultsGridControl.Dock = DockStyle.Fill;
            autocompleteTimer = new Timer();
            autocompleteTimer.Tick += AutocompleteTimer_Tick;
            autocompleteTimer.Interval = 250;
        }


        /// <summary>
        /// Save with a new filename
        /// </summary>
        /// <param name="newfilename"></param>
        /// <returns></returns>
        internal bool SaveQuery(string newfilename)
        {
            this.filename = newfilename;
            Program.rc["recentpath"] = Path.GetDirectoryName(this.filename);
            return this.SaveQuery();
        }

        /// <summary>
        /// Performs a save on the editor.
        /// </summary>
        internal bool SaveQuery()
        {

            if (this.filename == string.Empty)
            {
                var saveFileDialog = new SaveFileDialog();
                saveFileDialog.InitialDirectory = Program.rc.GetString("recentpath", string.Empty);
                saveFileDialog.Filter = "SQL Queries (*.sql)|*.sql|All Files (*.*)|*.*";
                if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
                {
                    this.filename = saveFileDialog.FileName;
                    Program.rc["recentpath"] = Path.GetDirectoryName(this.filename);
                }
                else
                {
                    // Filename selection cancelled, signal fail
                    return false;
                }

            }

            try
            {
                this.Text = System.IO.Path.GetFileName(this.filename);
                System.IO.File.WriteAllText(this.filename, scintilla1.Text);
                scintilla1.SetSavePoint();

                // reset the auto save flag
                IsAutoSavedQuery = false;

                // avoid placing autosaved files in the recent files list
                if (!Path.GetFileName(this.filename).StartsWith("Autosave-"))
                {
                    this.ParentFormCast().AddRecentFile(this.filename);
                }

                // set container tab tooltip
                var tab = (TabPage)this.Parent;
                tab.ToolTipText = this.filename;
                return true;
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
            return false;
        }


        /// <summary>
        /// Save a query as a different filename, or redirect to normal save if there is no filename.
        /// </summary>
        /// <returns></returns>
        internal bool SaveQueryAs(string suggestedPath)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = suggestedPath;
            saveFileDialog.FileName = Path.GetFileName(filename);
            saveFileDialog.Filter = "SQL Queries (*.sql)|*.sql|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                return SaveQuery(saveFileDialog.FileName);
            }
            else
            {
                return false;
            }
        }


        internal bool SaveQueryAs()
        {
            if (filename == string.Empty)
            {
                return SaveQuery();
            }
            else
            {
                return SaveQueryAs(Path.GetDirectoryName(filename));
            }
        }


        /// <summary>
        /// Set up the scintilla syntax higlighting code editor.
        /// </summary>
        private void SetupScintillaSyntax()
        {

            // #48 include dashes and word qualifiers as word characters
            scintilla1.WordChars = "abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-[]@#";
            scintilla1.Styles[ScintillaNET.Style.Default].Font = "Consolas";
            scintilla1.Styles[ScintillaNET.Style.Default].Size = 12;

            scintilla1.Lexer = ScintillaNET.Lexer.Sql;
            scintilla1.SetKeywords(0, SqlKeywords);

            scintilla1.Styles[ScintillaNET.Style.Sql.Number].ForeColor = Color.Red;
            scintilla1.Styles[ScintillaNET.Style.Sql.CommentLine].ForeColor = Color.Green;
            scintilla1.Styles[ScintillaNET.Style.Sql.Comment].ForeColor = Color.Green;
            scintilla1.Styles[ScintillaNET.Style.Sql.String].ForeColor = Color.Gray;
            scintilla1.Styles[ScintillaNET.Style.Sql.Word].ForeColor = Color.Blue;
            scintilla1.Styles[ScintillaNET.Style.Sql.Word2].ForeColor = Color.Magenta;

            // show line numbers
            scintilla1.Margins[0].Width = 40;

            // Hide the control border
            scintilla1.BorderStyle = BorderStyle.None;

            // Lower the threshold for drawing the horizontal scrollbar
            scintilla1.ScrollWidth = 500;

            // Enable multi selection paste
            scintilla1.MultipleSelection = true;
            scintilla1.MouseSelectionRectangularSwitch = true;
            scintilla1.AdditionalSelectionTyping = true;
            scintilla1.VirtualSpaceOptions = ScintillaNET.VirtualSpace.RectangularSelection;

            // Ignore control-functions
            scintilla1.ClearCmdKey(Keys.Control | Keys.F);
            scintilla1.ClearCmdKey(Keys.Control | Keys.H);
            scintilla1.ClearCmdKey(Keys.Control | Keys.J);
            scintilla1.ClearCmdKey(Keys.Control | Keys.K);
            scintilla1.ClearCmdKey(Keys.Control | Keys.Shift | Keys.F);

            scintilla1.AllowDrop = true;

            // Configure autocomplete icons
            scintilla1.RegisterRgbaImage(0, SQLAnt.Properties.Resources.table);
            scintilla1.RegisterRgbaImage(1, SQLAnt.Properties.Resources.procedure);
            scintilla1.RegisterRgbaImage(2, SQLAnt.Properties.Resources.view);
            scintilla1.RegisterRgbaImage(3, SQLAnt.Properties.Resources.emblem_favorite);
            scintilla1.RegisterRgbaImage(4, SQLAnt.Properties.Resources.table_alias);

            // Trigger a request to get the database object names
            this.RequestObjectList(false);

        }


        /// <summary>
        /// Request refreshing the object list of the current database.
        /// </summary>
        /// <param name="forced"></param>
        public void RequestObjectList(bool forced)
        {
            ParentFormCast().RequestObjectList(this.Server, this.Database, forced, this);
        }


        /// <summary>
        /// Setup the auto-complete popup menu.
        /// </summary>
        private void SetupAutoCompleteMenu()
        {
            //scintilla1.AutoCOrder = ScintillaNET.Order.PerformSort;
            scintilla1.AutoCMaxHeight = 20;
            //scintilla1.AutoCDropRestOfWord = true;
            scintilla1.AutoCIgnoreCase = true;
            scintilla1.AutoCCompleted += Scintilla1_AutoCCompleted;
        }

        private void Scintilla1_AutoCCompleted(object sender, AutoCSelectionEventArgs e)
        {
            if (AutoCompleteSnippet(e.Text)) return;

            string insertedText = e.Text;

            // auto qualify schema names
            if (Program.rc.GetBool("auto complete schema names", true))
            {
                if (this.meta != null)
                {
                    insertedText = this.meta.QualifyObjectName(insertedText);
                }
            }

            scintilla1.BeginUndoAction();
            var wordStart = scintilla1.WordStartPosition(scintilla1.CurrentPosition, true);
            var wordEnd = scintilla1.WordEndPosition(scintilla1.CurrentPosition, true);
            scintilla1.SetTargetRange(wordStart, wordEnd);
            scintilla1.ReplaceTarget(insertedText);
            scintilla1.GotoPosition(wordStart + insertedText.Length);
            scintilla1.EndUndoAction();
        }


        /// <summary>
        /// Auto complete the word under the cursor as a snippet
        /// </summary>
        private bool AutoCompleteSnippet(string word)
        {
            if (word == string.Empty)
            {
                word = scintilla1.GetWordFromPosition(scintilla1.CurrentPosition);
            }

            word = word.ToLower();

            // Handle auto-complete insert for code snippets
            var snippetKeys = GetSnippetKeys(false);
            foreach (string key in snippetKeys)
            {
                string snippet = GetSnippetText(key);
                if (word == key.ToLower())
                {
                    scintilla1.BeginUndoAction();
                    var start = scintilla1.WordStartPosition(scintilla1.CurrentPosition, true);
                    var end = scintilla1.WordEndPosition(scintilla1.CurrentPosition, true);
                    scintilla1.SetTargetRange(start, end);
                    scintilla1.ReplaceTarget(snippet);
                    scintilla1.GotoPosition(start + snippet.Length);
                    scintilla1.EndUndoAction();
                    return true;
                }
            }
            return false;
        }


        /// <summary>
        /// Indicate the modified state of the editor in the tab title.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scintilla1_TextChanged(object sender, EventArgs e)
        {
            ClearErrorIndicators();
            if (scintilla1.Modified)
            {
                if (!this.Text.Contains('*')) this.Text += "*";
            }
            else
            {
                this.Text = this.Text.Replace("*", "");
            }
        }

        
        /// <summary>
        /// Handle editor key down events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scintilla1_KeyDown(object sender, KeyEventArgs e)
        {
            // auto complete snippet
            if (e.KeyCode == Keys.Space)
            {
                AutoCompleteSnippet(string.Empty);
                //e.Handled = true;
            }
        }


        /// <summary>
        /// Refresh the caret position indicators
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scintilla1_UpdateUI(object sender, UpdateUIEventArgs e)
        {
            EditorLineIndicator.Text = scintilla1.LineFromPosition(scintilla1.CurrentPosition).ToString();
            EditorColIndicator.Text = scintilla1.GetColumn(scintilla1.CurrentPosition).ToString();
            HighlightWord(scintilla1.SelectedText, false);
        }




        /// <summary>
        /// Execute the query. If there is text selected, execute the selection.
        /// </summary>
        private void Execute()
        {
            try
            {
                if (executeThread.IsBusy) return;

                // Clear previous results
                ResultsGridControl.Clear();

                if (this.Server == null || this.Database == null || this.Server == string.Empty || this.Database == string.Empty)
                {
                    MessageBox.Show("Please choose the database to run against this query");
                    return;
                }

                // Remember if running a partial selection, to adjust error line numbers to highlight accordingly
                if (scintilla1.SelectedText != string.Empty)
                {
                    PartialExecutionSelectionLine = scintilla1.LineFromPosition(scintilla1.SelectionStart);
                }
                else
                {
                    PartialExecutionSelectionLine = 0;
                }

                var rowcount = Program.rc.GetInt("rowcount", 100000);

                var request = new SmoQueryRequest()
                {
                    Server = this.Server,
                    Database = this.Database,
                    Query = scintilla1.SelectedText != string.Empty ? scintilla1.SelectedText : scintilla1.Text,
                    Forced = false,
                    Target = this,
                    StartTime = DateTime.Now,
                    RowCountLimit = rowcount
                };

                request.AuditOptions = new SmoAuditOptions()
                {
                    Enabled = Program.rc.GetBool("audit enabled", true),
                    Path = Utilities.Storage("audits", ""),
                    Format = Program.rc.GetString("audit format", "csv"),
                    Filename = this.filename
                };



                Utilities.LogAction("Executing query...");
                this.SetStatus("Executing query...");
                ExecutingIndicator.Visible = true;
                executeThread.RunWorkerAsync(request);

            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
            finally
            {
                this.ParentFormCast().UpdateToolbarButtons(null);
            }
        }


        /// <summary>
        /// Execute object help on the selected text.
        /// </summary>
        private void ExecuteObjectHelp(string qualifiedObjectName)
        {
            try
            {
                qualifiedObjectName = qualifiedObjectName == string.Empty ? scintilla1.SelectedText : qualifiedObjectName;

                if (qualifiedObjectName == string.Empty)
                {
                    MessageBox.Show("Please highlight a table, view or procedure first.", "Object Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                // Clear previous results
                ResultsGridControl.Clear();

                if (this.Server == null || this.Database == null)
                {
                    MessageBox.Show("Please choose the database to run against this query");
                    return;
                }

                var queryobject = string.Format("EXEC SP_HELP '{0}'", qualifiedObjectName);

                var request = new SmoQueryRequest()
                {
                    Server = this.Server,
                    Database = this.Database,
                    Query = queryobject,
                    Forced = false,
                    Target = this,
                    StartTime = DateTime.Now
                };
                this.SetStatus("Retrieving object info...");
                ExecutingIndicator.Visible = true;
                Utilities.LogAction(queryobject);
                executeThread.RunWorkerAsync(request);
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Run the query execution under a thread
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void executeThread_DoWork(object sender, DoWorkEventArgs e)
        {
            Exception childException = null;
            var request = (SmoQueryRequest)e.Argument;
            var smo = new SmoController(this.Connectionstring, request.Database);
            smo.BeginTransaction();
            var thisWorker = sender as BackgroundWorker;
            var _child = new System.Threading.Thread(() =>
                                          {
                                              try
                                              {
                                                  smo.ProcessRequest(request);
                                                  smo.CommitTransaction();
                                                  e.Result = request;
                                              }
                                              catch (Exception ex)
                                              {
                                                  smo.RollbackTransaction();
                                                  if (ex is System.Threading.ThreadAbortException || ex.InnerException is System.Threading.ThreadAbortException)
                                                  {
                                                      Utilities.LogAction("User aborted query execution");
                                                  }
                                                  else
                                                  {
                                                      childException = ex;
                                                  }
                                              }
                                          });
            _child.Start();
            while (_child.IsAlive)
            {
                if (thisWorker.CancellationPending)
                {
                    _child.Abort();
                    e.Cancel = true;
                    break;
                }
                System.Threading.Thread.SpinWait(1);
            }

            if (childException != null) throw childException;

        }


        /// <summary>
        /// Receive executed query results
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void executeThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ExecutingIndicator.Visible = false;
            this.ParentFormCast().UpdateToolbarButtons(null);
            if (e.Cancelled) return;
            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    var query = (SmoQueryRequest)e.Result;
                    ResultType queryType = query.Query.StartsWith("EXEC SP_HELP") ? ResultType.SP_HELP : ResultType.Any;
                    if (query.QueryErrors != null && query.QueryErrors.Count > 0)
                    {
                        this.DisplayQueryError(query.QueryErrors);
                    }
                    else if (query.Result != null)
                    {
                        ResultsGridControl.DisplayResults((DataSet)query.Result, queryType);
                        // Clear error highlights on success
                        ClearErrorIndicators();
                    }
                }
            }
            if (e.Error != null)
            {
                this.DisplayQueryException(e.Error);
            }
        }


        /// <summary>
        /// Highlight a line with the error indicator
        /// </summary>
        /// <param name="line"></param>
        private void HighlightLine(int line)
        {
            const int NUM = 9;
            ClearErrorIndicators();

            // Update indicator appearance
            scintilla1.Indicators[NUM].Style = IndicatorStyle.StraightBox;
            scintilla1.Indicators[NUM].Under = true;
            scintilla1.Indicators[NUM].ForeColor = Color.Red;
            scintilla1.Indicators[NUM].OutlineAlpha = 50;
            scintilla1.Indicators[NUM].Alpha = 30;

            int start = scintilla1.Lines[line - 1].Position;
            int end = scintilla1.Lines[line - 1].EndPosition;
            scintilla1.IndicatorFillRange(start, end - start);

        }


        /// <summary>
        /// Clear error highlight indicators
        /// </summary>
        internal void ClearErrorIndicators()
        {
            const int NUM = 9;
            // Remove all uses of our indicator
            scintilla1.IndicatorCurrent = NUM;
            scintilla1.IndicatorClearRange(0, scintilla1.TextLength);
        }


        /// <summary>
        /// Display an error tab
        /// </summary>
        /// <param name="ex"></param>
        private void DisplayQueryException(Exception ex)
        {
            ResultsGridControl.DisplayQueryError(ex);
        }

        private void DisplayQueryError(List<SmoQueryError> queryErrors)
        {
            try
            {
                var errorBuilder = new StringBuilder();
                foreach (SmoQueryError error in queryErrors)
                {
                    errorBuilder.AppendLine(string.Format("Line {0}: {1}", PartialExecutionSelectionLine + error.Line, error.Message));
                    HighlightLine(PartialExecutionSelectionLine + error.Line);
                }

                // Display error details
                ResultsGridControl.DisplayQueryError(new Exception(errorBuilder.ToString()));

                // Update the UI so scintilla scrolling is accurate
                Application.DoEvents();

                // Focus first error
                int firstErrorPos = scintilla1.Lines[queryErrors[0].Line - 1].Position;
                scintilla1.GotoPosition(firstErrorPos);
                scintilla1.ScrollCaret();
            }
            catch (Exception ex2)
            {
                Utilities.LogException(ex2);
            }
        }



        /// <summary>
        /// Reveive the database object name list
        /// </summary>
        /// <param name="meta"></param>
        public void AcceptDatabaseObjectlist(DatabaseMetadata meta)
        {
            try
            {
                this.meta = meta;

                RefreshObjectTree(ObjectTreeFilterInput.Text);

                // Rebuild the meta wordlist
                MetaWordlist = new List<string>();
                MetaWordlist.AddRange(TransformWordlistWithIcon(meta.GetTableList(), AUTO_ICON_TABLE));
                MetaWordlist.AddRange(TransformWordlistWithIcon(meta.GetViewList(), AUTO_ICON_VIEW));
                MetaWordlist.AddRange(TransformWordlistWithIcon(meta.GetProcedureList(), AUTO_ICON_SPROC));
                MetaWordlist.AddRange(TransformWordlistWithIcon(meta.GetFunctionList(), AUTO_ICON_SPROC));
                MetaWordlist.Sort();
                // Syntax highlighting
                string wordlist = string.Join(" ", MetaWordlist.Distinct());
                scintilla1.SetKeywords(1, wordlist);
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Clears the meta information.
        /// </summary>
        public void ClearMeta()
        {
            meta = null;
            MetaWordlist = new List<string>();
        }


        /// <summary>
        /// Assign auto complete icon keys to word lists.
        /// </summary>
        /// <param name="wordlist"></param>
        /// <param name="iconkey"></param>
        /// <returns></returns>
        private List<string> TransformWordlistWithIcon(List<string> wordlist, string iconkey)
        {
            for (int i = 0; i < wordlist.Count; i++)
            {
                wordlist[i] = wordlist[i] + iconkey;
            }
            return wordlist;
        }

        /// <summary>
        /// Build a list of words around the cursor for contextual auto complete.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<string> BuildDynamicWordList()
        {
            // Limit the scan range
            var virtualScanSize = 1000;

            // Limit the text range to exclude the currently typed word.
            var currentPos = scintilla1.CurrentPosition;
            var wordStartPos = scintilla1.WordStartPosition(currentPos, true);
            var virtualPos = wordStartPos - virtualScanSize;
            if (virtualPos < 0)
            {
                virtualScanSize += virtualPos;
                virtualPos = 0;
            }

            // Regex match word boundaries
            var combined = scintilla1.GetTextRange(virtualPos, virtualScanSize);
            var words = new Dictionary<string, string>();
            foreach (Match m in Regex.Matches(combined, @"\b\w+\b"))
            {
                yield return m.Value;
            }

        }


        /// <summary>
        /// Update this editor tab's title
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CodeEditor_TextChanged(object sender, EventArgs e)
        {
            ParentFormCast().UpdateEditorTabText(this);
        }


        /// <summary>
        /// Clear the result grids and dispose of them properly to release memory.
        /// </summary>
        public void ClearResultsGrid()
        {
            if (ResultsGridControl != null) ResultsGridControl.Clear();
        }

        /// <summary>
        /// Prompts to save changes before closing the editor
        /// </summary>
        internal bool CloseQuery(bool applicationExiting = false)
        {
            // remember the list of open files
            if (applicationExiting && this.filename != string.Empty && Program.rc.GetBool("remember open files", true))
            {
                Program.rc["open files"] += this.filename + "|";
            }

            if (scintilla1.Modified || IsAutoSavedQuery)
            {
                // When the auto save on exit option is enabled
                if (applicationExiting && Program.rc.GetBool("auto save on exit", true))
                {
                    if (this.filename == string.Empty)
                    {
                        Program.autosavecounter++;
                        this.filename = Utilities.Storage("autosaved", "Autosave-" + Program.autosavecounter.ToString() + ".sql");
                    }
                    this.SaveQuery();
                    this.ParentFormCast().RemoveEditorTab(this);
                }
                else
                {
                    var option = MessageBox.Show(this, "Save Changes?", "Save Changes", MessageBoxButtons.YesNoCancel);

                    if (option == DialogResult.Cancel) return false;

                    if (option == DialogResult.Yes)
                    {
                        if (this.SaveQuery())
                        {
                            this.ParentFormCast().RemoveEditorTab(this);
                        }
                        else
                        {
                            // Save cancelled
                            return false;
                        }
                    }

                    if (option == DialogResult.No)
                    {
                        this.ParentFormCast().RemoveEditorTab(this);
                    }
                }
            }
            else
            {
                // No changes to save
                this.ParentFormCast().RemoveEditorTab(this);
            }
            return true;
        }


        /// <summary>
        /// Select the code block under the cursor, within blank lines as boundaries.
        /// </summary>
        private void SelectCurrentBlock()
        {
            try
            {

                // Store detected line breaks.
                int start = -1;
                int end = -1;

                // Match on more empty lines
                var matches = Regex.Matches(Environment.NewLine + scintilla1.Text, @"^\s{1,}$", RegexOptions.Multiline);

                // Find the first position before the cursor
                for (int i = matches.Count - 1; i >= 0; i--)
                {
                    if (matches[i].Index < scintilla1.CurrentPosition)
                    {
                        start = matches[i].Index;
                        break;
                    }
                }

                // Find the next position after the cursor
                for (int i = 0; i < matches.Count; i++)
                {
                    if (matches[i].Index > scintilla1.CurrentPosition)
                    {
                        end = matches[i].Index;
                        break;
                    }
                }

                if (start > -1)
                {
                    scintilla1.SelectionStart = start;
                    scintilla1.SelectionEnd = end > -1 ? end : scintilla1.TextLength;
                }

            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }

        }


        /// <summary>
        /// Process a key event given to us by our parent form.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="alt"></param>
        /// <param name="shift"></param>
        /// <param name="keys"></param>
        internal void ProcessKey(bool control, bool alt, bool shift, Keys keys)
        {
            if (scintilla1.CanUndo && control && keys == Keys.Z) UndoFromActiveControl();
            if (scintilla1.CanRedo && control && keys == Keys.Y) RedoFromActiveControl();
            if (control && keys == Keys.A) SelectAllFromActiveControl();
            if (control && keys == Keys.V) PasteToActiveControl();
            if (control && keys == Keys.X) CutFromActiveControl();
            if (control && keys == Keys.C) CopyFromActiveControl();
            if (keys == Keys.F3) ParentFormCast().FindNext(scintilla1);
            if (!control && keys == Keys.F5) this.Execute();
            if (control && keys == Keys.F5) SelectCurrentBlock();
            if (alt && keys == Keys.Pause && executeThread.IsBusy && !executeThread.CancellationPending) executeThread.CancelAsync();
            if (keys == Keys.F6) ResultsGridControl.FocusResultsTab();
            if (keys == Keys.F7 || keys == Keys.Escape) scintilla1.Focus();
            if (keys == Keys.F8) this.splitContainerTree.Panel1Collapsed = !this.splitContainerTree.Panel1Collapsed;
            if (control && keys == Keys.J) BuildAutoCompleteWords();
            if (control && keys == Keys.K) ShowAutocompleteSnippets();
            if (control && !shift && keys == Keys.F) FocusFindTab();
            if (control && !shift && keys == Keys.H) FocusReplaceTab();
            if (control && shift && keys == Keys.F) FocusObjectFilter();

            // Copy text or results
            if (control && keys == Keys.C)
            {
                if (this.splitContainerEditor.ActiveControl == scintilla1)
                {
                    scintilla1.CopyAllowLine(ScintillaNET.CopyFormat.Text);
                    this.SetStatus("Copied query to the clipboard");
                }
                else if (this.splitContainerEditor.ActiveControl is DataGridView)
                {
                    var cells = ResultsGridControl.CopyActiveGrid();
                    if (cells > 0)
                    {
                        SetStatus(string.Format("Copied {0} cells to the clipboard", cells));
                    }
                }
            }

            // Results tabs
            if (control && keys == Keys.PageUp) ResultsGridControl.SwitchTab(-1);
            if (control && keys == Keys.PageDown) ResultsGridControl.SwitchTab(1);

            // Object Help
            if (alt && keys == Keys.F1) ExecuteObjectHelp(string.Empty);

            // navigation keys cancels auto complete
            if (keys == Keys.Home || keys == Keys.End) scintilla1.AutoCCancel();

        }


        public void UndoFromActiveControl()
        {
            Control active = FindFocusedControl(this);
            if (active != null)
            {
                if (active is TextBox)
                {
                    ((TextBox)active).Undo();
                }
                else if (active is ScintillaNET.Scintilla)
                {
                    ((Scintilla)active).Undo();
                }
            }
        }

        public void RedoFromActiveControl()
        {
            Control active = FindFocusedControl(this);
            if (active != null)
            {
                if (active is TextBox)
                {
                    //((TextBox)active).();
                }
                else if (active is ScintillaNET.Scintilla)
                {
                    ((Scintilla)active).Redo();
                }
            }
        }


        /// <summary>
        /// Select all in the focussed control
        /// </summary>
        public void SelectAllFromActiveControl()
        {
            Control active = FindFocusedControl(this);
            if (active != null)
            {
                if (active is TextBox)
                {
                    ((TextBox)active).SelectAll();
                }
                else if (active is ScintillaNET.Scintilla)
                {
                    ((Scintilla)active).SelectAll();
                }
                else if (active is DataGridView)
                {
                    ((DataGridView)active).SelectAll();
                }
            }
        }


        /// <summary>
        /// Copy from the focussed control
        /// </summary>
        public void CopyFromActiveControl()
        {
            Control active = FindFocusedControl(this);
            if (active != null)
            {
                if (active is TextBox)
                {
                    ((TextBox)active).Copy();
                }
                else if (active is ScintillaNET.Scintilla)
                {
                    ((Scintilla)active).CopyAllowLine();
                }
                else if (active is DataGridView)
                {
                    ResultsGridControl.CopyActiveGrid();
                }
            }
        }


        /// <summary>
        /// Cut from the focussed control
        /// </summary>
        public void CutFromActiveControl()
        {
            Control active = FindFocusedControl(this);
            if (active != null)
            {
                if (active is TextBox)
                {
                    ((TextBox)active).Cut();
                }
                else if (active is ScintillaNET.Scintilla)
                {
                    ((Scintilla)active).Cut();
                }
            }
        }


        /// <summary>
        /// Paste the clipboard into the focussed control
        /// </summary>
        public void PasteToActiveControl()
        {
            Control active = FindFocusedControl(this);
            if (active != null)
            {
                if (active is TextBox)
                {
                    ((TextBox)active).Paste();
                }
                else if (active is ScintillaNET.Scintilla)
                {
                    ((Scintilla)active).Paste();
                }
            }
        }


        /// <summary>
        /// Returns the focussed control
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public Control FindFocusedControl(Control control)
        {
            var container = control as IContainerControl;
            while (container != null)
            {
                control = container.ActiveControl;
                container = control as IContainerControl;
            }
            return control;
        }


        /// <summary>
        /// Sets the statusbar text and starts the timer to clear it
        /// </summary>
        /// <param name="text"></param>
        private void SetStatus(string text)
        {
            StatusLabel.Text = text;
            ClearStatusTimer.Start();
        }


        /// <summary>
        /// Reset the status label
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearStatusTimer_Tick(object sender, EventArgs e)
        {
            ClearStatusTimer.Stop();
            StatusLabel.Text = string.Empty;
        }


        /// <summary>
        /// Handles editor edits
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scintilla1_CharAdded(object sender, ScintillaNET.CharAddedEventArgs e)
        {
            try
            {
                // The cursor is inside a comment
                var withinComment = InsideCommentBlock();

                if (e.Char != 13 && e.Char != 10)
                {
                    autocompleteTimer.Stop();
                    if (!withinComment) autocompleteTimer.Start();
                }

                // auto capitalize sql keywords, but only after the word when space is encountered
                if (Program.rc.GetBool("auto cap keywords", true) && e.Char == 32)
                {
                    // skip auto completion and auto caps when inside a comment
                    if (withinComment) return;

                    var currentPos = scintilla1.CurrentPosition - 1;
                    var word = scintilla1.GetWordFromPosition(currentPos);
                    // strip punctuation ([qualified] names can break the regex)
                    word = Regex.Replace(word, @"[^\w\s]", "");
                    // the current word is a sql keyword
                    if (word != string.Empty && Regex.IsMatch(SqlKeywords, string.Format(@"\b{0}\b", word.ToLower())))
                    {
                        // push undo state
                        scintilla1.BeginUndoAction();
                        // position the target selection around the current word
                        var a = scintilla1.WordStartPosition(currentPos, true);
                        var b = scintilla1.WordEndPosition(currentPos, true);
                        scintilla1.SetTargetRange(a, b);
                        scintilla1.ReplaceTarget(word.ToUpper());
                        // reposition cursor at the end of the word
                        scintilla1.SelectionStart = currentPos + 1;
                        scintilla1.SelectionEnd = currentPos + 1;
                        scintilla1.EndUndoAction();
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }

        private bool InsideCommentBlock()
        {
            const string commentregex = @"(--.*)|(((/\*)+?[\w\W]+?(\*/)+))";
            bool inside = false;
            int cursor = scintilla1.CurrentPosition;
            var matches = Regex.Matches(scintilla1.Text, commentregex);

            foreach (Match match in matches)
            {
                if (cursor > match.Index && cursor < match.Index + match.Length)
                {
                    inside = true;
                }
                if (inside) break;
            }

            return inside;
        }

        /// <summary>
        /// Show the auto complete menu with context awareness.
        /// </summary>
        private void BuildAutoCompleteWords()
        {
            try
            {
                // Find the word start
                var currentPos = scintilla1.CurrentPosition;
                var wordStartPos = scintilla1.WordStartPosition(currentPos, true);
                var lenEntered = currentPos - wordStartPos;

                // Only show for partially entered words
                if (lenEntered == 0) return;

                var wordlist = new List<string>();

                // Get the current word
                var curWord = scintilla1.GetWordFromPosition(currentPos).ToLower();

                // Do not show auto complete if the current word is a number.
                // Assists where the popup gets in the way writing where statements.
                int tempi = 0;
                if (int.TryParse(curWord, out tempi))
                {
                    return;
                }

                wordlist.AddRange(GetSnippetKeys(true).Where(n => n.ToLower().Contains(curWord)));
                wordlist.AddRange(SqlKeywords.ToUpper().Split(' ').Where(n => n.ToLower().Contains(curWord)));

                // Contextual
                if (MetaWordlist != null && meta != null)
                {

                    // Match tables to aliased names
                    this.DetectTableAliases();

                    // Find the first match before the current word
                    string nearestWord = MatchClosestTableOrAlias(AUTO_COMPLETE_SCAN_RANGE, false);

                    // Find the first match after the current word
                    if (nearestWord == string.Empty)
                    {
                        nearestWord = MatchClosestTableOrAlias(AUTO_COMPLETE_SCAN_RANGE, true);
                    }

                    // Include nearest table or alias columns
                    var columns = LookupAliasColumns(nearestWord);
                    if (columns.Any())
                    {
                        wordlist.AddRange(columns.Where(n => n.ToLower().Contains(curWord)));
                    }

                    if (lenEntered > 0)
                    {
                        // Include matching tables, views, sprocs
                        wordlist.AddRange(MetaWordlist.Where(n => n.ToLower().Contains(curWord)).Take(20));

                        // Dynamic word list from user typed text
                        // (this duplicates objects as those have icon identifiers attached)
                        //wordlist.AddRange(this.BuildDynamicWordList());

                        // Aliased objects
                        if (TableAliases != null)
                        {
                            wordlist.AddRange(TransformWordlistWithIcon(TableAliases.Keys.Where(n => n.ToLower().Contains(curWord)).ToList(), AUTO_ICON_ALIAS));
                        }
                    }

                }

                // The the auto complete list if the wordlist has no matches for the current word
                var hasListMatch = wordlist.Where(n => n.ToLower().StartsWith(curWord)).Any();
                wordlist.Sort();
                string wordstring = string.Join(" ", wordlist.Distinct());
                scintilla1.AutoCShow(hasListMatch ? lenEntered : 0, wordstring);
                scintilla1.AutoCIgnoreCase = true;
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Find the nearest table or alias to the cursor.
        /// </summary>
        /// <param name="maxWordsToScan">The number of words to scan before giving up</param>
        /// <param name="forwards">Scan forward from the cursor, otherwise scan backwards</param>
        /// <returns></returns>
        private string MatchClosestTableOrAlias(int maxWordsToScan, bool forwards)
        {
            int steps = 0;
            var direction = (forwards ? 1 : -1);
            int wordStartPos = 0;
            var nextWordStartPos = 0;

            // regex to strip non alpha-numeric characters
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");

            if (direction == -1)
            {
                wordStartPos = scintilla1.WordStartPosition(scintilla1.CurrentPosition, true);
                nextWordStartPos = scintilla1.WordStartPosition(wordStartPos + direction, true);
            }
            else
            {
                wordStartPos = scintilla1.WordEndPosition(scintilla1.CurrentPosition, true);
                nextWordStartPos = scintilla1.WordEndPosition(wordStartPos + direction, true);
            }

            string nextWord = string.Empty;
            while (true)
            {
                // limit steps
                steps++;
                if (steps > maxWordsToScan)
                {
                    nextWord = string.Empty;
                    break;
                }

                // stop at the beginning of the document
                if (nextWordStartPos == 0) break;

                // fetch the next word
                nextWord = rgx.Replace(scintilla1.GetWordFromPosition(nextWordStartPos).ToLower(), "");

                // skip blank words
                if (nextWord != string.Empty)
                {

                    bool IsAlias = TableAliases.ContainsKey(nextWord);
                    var sens = meta.GetTableOrViewInsentitiveLookup(nextWord);
                    bool IsTable = meta.ContainsTableOrView(sens);
                    if (IsAlias || IsTable)
                    {
                        // Found a valid alias or table
                        break;
                    }
                    else
                    {
                        // Not a word we want
                        nextWord = string.Empty;
                    }
                }

                // Move to the next word
                if (direction == -1)
                {
                    nextWordStartPos = scintilla1.WordStartPosition(nextWordStartPos + direction, true);
                }
                else
                {
                    nextWordStartPos = scintilla1.WordEndPosition(nextWordStartPos + direction, true);
                }
            }

            return nextWord;
        }


        /// <summary>
        /// Show auto complete for snippets only
        /// </summary>
        private void ShowAutocompleteSnippets()
        {
            try
            {
                var currentPos = scintilla1.CurrentPosition;
                var wordStartPos = scintilla1.WordStartPosition(currentPos, true);
                var lenEntered = currentPos - wordStartPos;
                string wordstring = string.Join(" ", GetSnippetKeys(true));
                scintilla1.AutoCShow(lenEntered, wordstring);
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Match tables with their aliased names
        /// </summary>
        private void DetectTableAliases()
        {
            if (meta == null) return;
            if (TableAliases == null) TableAliases = new Dictionary<string, string>();
            TableAliases.Clear();

            // Limit the scan range to this many characters around the cursor
            var scanSize = 10000;
            int scanStart = Math.Max(0, scintilla1.CurrentPosition - scanSize);
            int scanEnd = Math.Min(scintilla1.TextLength, scintilla1.CurrentPosition + scanSize);
            
            // #50 Exclude comments from query
            string cleanQuery = Regex.Replace(scintilla1.GetTextRange(scanStart, scanEnd - scanStart), @"(--.*)|(((/\*)+?[\w\W]+?(\*/)+))", "");
            
            // Remove blank lines
            cleanQuery = Regex.Replace(cleanQuery, @"(^\s*$)", "", RegexOptions.Multiline);
            
            // Split out words, preserving [qualified words]
            var words = Regex.Split(cleanQuery, @"[\s.,'()*](?=[^\]]*?(?:\[|$))");

            // Remove empties
            words = words.Where(n => n != string.Empty).ToArray();

            // a word-state is valid if it is a table or view
            // a word-state is invalid if it is a sql keyword
            // a word-state is free if it is neither valid nor invalid
            // for each word: 
            //  check word state of current word (a):
            //      check word state for the next word (b):
            //          if a is valid and b is free:
            //              remember the alias

            // regex to strip non alpha-numeric characters
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");

            for (int i = 0; i < words.Length - 1; i++)
            {
                // grab the next two words
                var word_a = words[i];
                var word_b = words[i + 1];

                // aliasing "AS" takes the next word
                if (word_b.ToUpper() == "AS")
                {
                    // if there is a next word to take
                    if (words.Length > i + 1)
                        word_b = words[i + 2];
                }

                // strip non alpha-numeric characters
                word_a = rgx.Replace(word_a.ToLower(), "");
                word_b = rgx.Replace(word_b.ToLower(), "");

                // meta lookups are case sensitive
                word_a = meta.GetTableOrViewInsentitiveLookup(word_a);

                // check validity
                var a = checkWordValiditiy(word_a);
                var b = checkWordValiditiy(word_b);

                // remember the alias
                if (a == WordState.Valid && b == WordState.Free)
                {
                    if (!TableAliases.ContainsKey(word_b))
                    {
                        TableAliases.Add(word_b, word_a);
                    }
                }
            }
            
        }

        /// <summary>
        /// Check the state of a word.
        /// a word is valid if it is a table or view.
        /// a word is invalid if it is a sql keyword.
        /// a word is free if it is neither valid nor invalid.
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        private WordState checkWordValiditiy(string word)
        {
            if (meta.ContainsTableOrView(word))
            {
                return WordState.Valid;
            }
            else if (word == string.Empty || Regex.IsMatch(SqlKeywords, string.Format(@"\b{0}\b", word)))
            {
                return WordState.Invalid;
            }
            else
            {
                return WordState.Free;
            }
        }


        /// <summary>
        /// Finds the alias table and looks up the columns
        /// </summary>
        /// <param name="alias"></param>
        /// <returns></returns>
        private List<string> LookupAliasColumns(string alias)
        {
            if (TableAliases == null) return new List<string>();
            alias = alias.ToLower();
            if (this.TableAliases.ContainsKey(alias))
            {
                return this.meta.GetColumnList(this.TableAliases[alias]);
            }
            else
            {
                // No match for an alias, this could be an alias-less query
                // and check if the word is a table name
                var sens = meta.GetTableOrViewInsentitiveLookup(alias);
                if (meta.ContainsTableOrView(sens))
                {
                    return this.meta.GetColumnList(sens);
                }
            }
            return new List<string>();
        }


        /// <summary>
        /// Find the given text 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="wholeWord"></param>
        private void FindNext(string text, bool wholeWord)
        {
            try
            {
                // Indicators 0-7 could be in use by a lexer
                // so we'll use indicator 8 to highlight words.
                const int NUM = 8;

                // Remove all uses of our indicator
                scintilla1.IndicatorCurrent = NUM;
                scintilla1.IndicatorClearRange(0, scintilla1.TextLength);

                if (text.Trim() == string.Empty) return;

                // Update indicator appearance
                scintilla1.Indicators[NUM].Style = IndicatorStyle.StraightBox;
                scintilla1.Indicators[NUM].Under = true;
                scintilla1.Indicators[NUM].ForeColor = Color.Green;
                scintilla1.Indicators[NUM].OutlineAlpha = 192;
                scintilla1.Indicators[NUM].Alpha = 128;

                // Search the document
                scintilla1.TargetStart += 1;
                scintilla1.TargetEnd = scintilla1.TextLength;

                if (wholeWord)
                {
                    scintilla1.SearchFlags = SearchFlags.WholeWord;
                }
                else
                {
                    scintilla1.SearchFlags = SearchFlags.None;
                }

                if (scintilla1.SearchInTarget(text) == -1)
                {
                    // Wrap to start
                    if (scintilla1.TargetStart > 1)
                    {
                        scintilla1.TargetStart = 0;
                        FindNext(text, wholeWord);
                    }
                }
                else
                {
                    // Mark the search results with the current indicator
                    scintilla1.IndicatorFillRange(scintilla1.TargetStart, scintilla1.TargetEnd - scintilla1.TargetStart);
                    scintilla1.ScrollRange(scintilla1.TargetStart, scintilla1.TargetEnd);
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Replace the given text
        /// </summary>
        /// <param name="findtext"></param>
        /// <param name="replacetext"></param>
        /// <param name="wholeWord"></param>
        private void ReplaceNext(string findtext, string replacetext, bool wholeWord)
        {
            try
            {
                // Indicators 0-7 could be in use by a lexer
                // so we'll use indicator 8 to highlight words.
                const int NUM = 8;

                // Remove all uses of our indicator
                scintilla1.IndicatorCurrent = NUM;
                scintilla1.IndicatorClearRange(0, scintilla1.TextLength);

                if (findtext.Trim() == string.Empty) return;

                // Update indicator appearance
                scintilla1.Indicators[NUM].Style = IndicatorStyle.StraightBox;
                scintilla1.Indicators[NUM].Under = true;
                scintilla1.Indicators[NUM].ForeColor = Color.Green;
                scintilla1.Indicators[NUM].OutlineAlpha = 192;
                scintilla1.Indicators[NUM].Alpha = 128;

                // Search the document
                scintilla1.TargetStart += 1;
                scintilla1.TargetEnd = scintilla1.TextLength;

                if (wholeWord)
                {
                    scintilla1.SearchFlags = SearchFlags.WholeWord;
                }
                else
                {
                    scintilla1.SearchFlags = SearchFlags.None;
                }

                if (scintilla1.SearchInTarget(findtext) == -1)
                {
                    // Wrap to start
                    if (scintilla1.TargetStart > 1)
                    {
                        scintilla1.TargetStart = 0;
                        FindNext(findtext, wholeWord);
                    }
                }
                else
                {
                    // Replace
                    scintilla1.ReplaceTarget(replacetext);
                    // Highlight next
                    FindNext(findtext, wholeWord);
                    scintilla1.TargetStart -= 1;
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Highlight the given text in the editor
        /// </summary>
        /// <param name="text"></param>
        /// <param name="scintilla"></param>
        private void HighlightWord(string text, bool scrollInView)
        {
            try
            {
                // Indicators 0-7 could be in use by a lexer
                // so we'll use indicator 8 to highlight words.
                const int NUM = 8;

                // Remove all uses of our indicator
                scintilla1.IndicatorCurrent = NUM;
                scintilla1.IndicatorClearRange(0, scintilla1.TextLength);

                // remove punctuation
                text = Regex.Replace(text, @"[^\w\s]", "");

                // ignore empty or text with newlines
                if (text.Trim() == string.Empty) return;
                if (text.Contains(Environment.NewLine)) return;

                // Update indicator appearance
                scintilla1.Indicators[NUM].Style = IndicatorStyle.StraightBox;
                scintilla1.Indicators[NUM].Under = true;
                scintilla1.Indicators[NUM].ForeColor = Color.Gold;
                scintilla1.Indicators[NUM].OutlineAlpha = 192;
                scintilla1.Indicators[NUM].Alpha = 128;

                // Search the document
                scintilla1.TargetStart = 0;
                scintilla1.TargetEnd = scintilla1.TextLength;
                scintilla1.SearchFlags = SearchFlags.None;

                while (scintilla1.SearchInTarget(text) != -1)
                {
                    // Mark the search results with the current indicator
                    scintilla1.IndicatorFillRange(scintilla1.TargetStart, scintilla1.TargetEnd - scintilla1.TargetStart);

                    if (scrollInView)
                        scintilla1.ScrollRange(scintilla1.TargetStart, scintilla1.TargetEnd);

                    // Search the remainder of the document
                    scintilla1.TargetStart = scintilla1.TargetEnd;
                    scintilla1.TargetEnd = scintilla1.TextLength;
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Object tree drag start operation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ObjectTree_ItemDrag(object sender, ItemDragEventArgs e)
        {
            this.ObjectTree.DoDragDrop(((TreeNode)e.Item).Text, DragDropEffects.Copy);
        }


        /// <summary>
        /// Object tree mouse intercept
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectTree_MouseDown(object sender, MouseEventArgs e)
        {
            // Right clicks will also select the node under the cursor
            if (e.Button == MouseButtons.Right)
            {
                var node = ObjectTree.GetNodeAt(e.X, e.Y);
                if (node != null) ObjectTree.SelectedNode = node;
            }
        }


        /// <summary>
        /// Load database object into the navigation tree
        /// </summary>
        private void RefreshObjectTree(string filter)
        {
            if (this.meta == null) return;

            try
            {
                var hasfilter = filter != string.Empty;
                filter = filter.ToLower();
                this.ObjectTree.Nodes.Clear();

                // Tables
                var tablesParent = new TreeNode("Tables");
                tablesParent.ImageKey = "table.png";
                tablesParent.SelectedImageKey = "table.png";
                foreach (DataTable t in this.meta.Tables.Tables)
                {
                    var columnFilterMatch = false;
                    var tableFilterMatch = false;
                    var tnode = new TreeNode(t.TableName);
                    tnode.Tag = "table";
                    tnode.ImageKey = "table.png";
                    tnode.SelectedImageKey = "table.png";
                    tableFilterMatch = t.TableName.ToLower().Contains(filter);
                    if (hasfilter && tableFilterMatch) tnode.BackColor = Color.Yellow;
                    var columns = meta.GetColumnList(t.TableName);
                    foreach (string c in columns)
                    {
                        var columnNode = tnode.Nodes.Add(c, c);
                        if (hasfilter)
                        {
                            if (c.ToLower().Contains(filter))
                            {
                                columnFilterMatch = true;
                                columnNode.BackColor = Color.Yellow;
                            }
                        }
                    }
                    if (hasfilter == false || tableFilterMatch || columnFilterMatch) tablesParent.Nodes.Add(tnode);
                }
                this.ObjectTree.Nodes.Add(tablesParent);
                tablesParent.Expand();

                // Views
                var viewsParent = new TreeNode("Views");
                viewsParent.ImageKey = "view.png";
                viewsParent.SelectedImageKey = "view.png";
                foreach (DataTable t in this.meta.Views.Tables)
                {
                    var columnFilterMatch = false;
                    var tableFilterMatch = false;
                    var tnode = new TreeNode(t.TableName);
                    tnode.Tag = "view";
                    tnode.ImageKey = "view.png";
                    tnode.SelectedImageKey = "view.png";
                    tableFilterMatch = t.TableName.ToLower().Contains(filter);
                    if (hasfilter && tableFilterMatch) tnode.BackColor = Color.Yellow;
                    var columns = meta.GetColumnList(t.TableName);
                    foreach (string c in columns)
                    {
                        var columnNode = tnode.Nodes.Add(c, c);
                        if (hasfilter)
                        {
                            if (c.ToLower().Contains(filter))
                            {
                                columnFilterMatch = true;
                                columnNode.BackColor = Color.Yellow;
                            }
                        }
                    }
                    if (hasfilter == false || tableFilterMatch || columnFilterMatch) viewsParent.Nodes.Add(tnode);
                }
                this.ObjectTree.Nodes.Add(viewsParent);

                // Procedures
                var procParent = new TreeNode("Procedures");
                procParent.ImageKey = "procedure.png";
                procParent.SelectedImageKey = "procedure.png";
                foreach (DataTable t in this.meta.Procedures.Tables)
                {
                    var columnFilterMatch = false;
                    var tableFilterMatch = false;
                    var tnode = new TreeNode(t.TableName);
                    tnode.Tag = "sproc";
                    tnode.ImageKey = "procedure.png";
                    tnode.SelectedImageKey = "procedure.png";
                    tableFilterMatch = t.TableName.ToLower().Contains(filter);
                    if (hasfilter && tableFilterMatch) tnode.BackColor = Color.Yellow;
                    var columns = meta.GetProcedureParameterList(t.TableName);
                    foreach (string c in columns)
                    {
                        var columnNode = tnode.Nodes.Add(c, c);
                        if (hasfilter)
                        {
                            if (c.ToLower().Contains(filter))
                            {
                                columnFilterMatch = true;
                                columnNode.BackColor = Color.Yellow;
                            }
                        }
                    }
                    if (hasfilter == false || tableFilterMatch || columnFilterMatch) procParent.Nodes.Add(tnode);
                }
                this.ObjectTree.Nodes.Add(procParent);

                // Functions
                var funcParent = new TreeNode("Functions");
                funcParent.ImageKey = "procedure.png";
                funcParent.SelectedImageKey = "procedure.png";
                foreach (DataTable t in this.meta.Functions.Tables)
                {
                    var columnFilterMatch = false;
                    var tableFilterMatch = false;
                    var tnode = new TreeNode(t.TableName);
                    tnode.Tag = "function";
                    tnode.ImageKey = "procedure.png";
                    tnode.SelectedImageKey = "procedure.png";
                    tableFilterMatch = t.TableName.ToLower().Contains(filter);
                    if (hasfilter && tableFilterMatch) tnode.BackColor = Color.Yellow;
                    var columns = meta.GetFunctionParameterList(t.TableName);
                    foreach (string c in columns)
                    {
                        var columnNode = tnode.Nodes.Add(c, c);
                        if (hasfilter)
                        {
                            if (c.ToLower().Contains(filter))
                            {
                                columnFilterMatch = true;
                                columnNode.BackColor = Color.Yellow;
                            }
                        }
                    }
                    if (hasfilter == false || tableFilterMatch || columnFilterMatch) funcParent.Nodes.Add(tnode);
                }
                this.ObjectTree.Nodes.Add(funcParent);

                // auto expand on filter
                if (hasfilter)
                {
                    //tablesParent.ExpandAll();
                    //viewsParent.ExpandAll();
                    //procParent.ExpandAll();
                    tablesParent.EnsureVisible();
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Allow drag drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scintilla1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(string))) e.Effect = DragDropEffects.Copy;
        }


        /// <summary>
        /// Allow drag drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void scintilla1_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                scintilla1.BeginUndoAction();
                string insertText = (string)e.Data.GetData(typeof(string));

                // Check the text being inserted for auto qualifying schema names
                if (this.meta != null && Program.rc.GetBool("auto complete schema names", true))
                {
                    insertText = this.meta.QualifyObjectName(insertText);
                }

                scintilla1.InsertText(scintilla1.CurrentPosition, insertText);
                scintilla1.GotoPosition(scintilla1.CurrentPosition + insertText.Length);
                scintilla1.EndUndoAction();
                scintilla1.Focus();
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }



        private void Scintilla1_DragOver(object sender, System.Windows.Forms.DragEventArgs e)
        {
            var pt = scintilla1.PointToClient(new Point(e.X, e.Y));
            var i = scintilla1.CharPositionFromPoint(pt.X, pt.Y);
            scintilla1.GotoPosition(i);
        }


        /// <summary>
        /// Focus the object filter
        /// </summary>
        private void FocusObjectFilter()
        {
            SidebarTabs.SelectedTab = ObjectTreeTab;
            ObjectTreeFilterInput.Focus();
            ObjectTreeFilterInput.SelectAll();
        }


        /// <summary>
        /// Focus the find and replace tab
        /// </summary>
        private void FocusFindTab()
        {
            try
            {
                this.ParentFormCast().ShowFindDialog(scintilla1);
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Focus the find and replace tab
        /// </summary>
        private void FocusReplaceTab()
        {
            try
            {
                this.ParentFormCast().ShowReplaceDialog(scintilla1);
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Returns all snippet keys for auto complete lists
        /// </summary>
        /// <param name="withIcons"></param>
        /// <returns></returns>
        private List<string> GetSnippetKeys(bool withIcons)
        {
            if (withIcons)
            {
                var clone = new List<string>();
                foreach (string key in Program.codeSnippets.Keys)
                {
                    clone.Add(key + AUTO_ICON_SNIPPET);
                }
                return clone;
            }
            else
            {
                return Program.codeSnippets.Keys.ToList();
            }
        }


        /// <summary>
        /// Gets the snippet text
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetSnippetText(string key)
        {
            try
            {
                return Program.codeSnippets.GetString(key, string.Empty);
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
                return string.Empty;
            }
        }


        /// <summary>
        /// Display the script object window for the selected item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScriptObjectMenu_Click(object sender, EventArgs e)
        {
            try
            {
                if (ObjectTree.SelectedNode != null && ObjectTree.SelectedNode.Tag != null)
                {
                    var dict = new Dictionary<string, string>();
                    dict.Add(meta.QualifyFullName(ObjectTree.SelectedNode.Text), ObjectTree.SelectedNode.Tag.ToString());
                    ParentFormCast().ShowGenerateScriptsWindow(dict);
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Show information on the selected tree item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeObjectInformationMenu_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.meta == null) return;
                var node = ObjectTree.SelectedNode;
                if (node != null)
                {
                    if (node.Tag == null) return;
                    var nodeType = node.Tag.ToString();
                    if (nodeType == "table" || nodeType == "view" || nodeType == "sproc" || nodeType == "function")
                    {
                        ExecuteObjectHelp(this.meta.QualifyObjectName(node.Text));
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Apply the object tree filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectTreeFilterTimer_Tick(object sender, EventArgs e)
        {
            ObjectTreeFilterTimer.Stop();
            RefreshObjectTree(ObjectTreeFilterInput.Text);
        }


        /// <summary>
        /// Changes to the object tree filter triggers the filter action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectTreeFilterInput_TextChanged(object sender, EventArgs e)
        {
            ObjectTreeFilterTimer.Stop();
            ObjectTreeFilterTimer.Start();
        }


        /// <summary>
        /// Expand all top-level nodes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExpandTreeNodesMenu_Click(object sender, EventArgs e)
        {
            foreach (TreeNode categorynode in ObjectTree.Nodes)
            {
                foreach (TreeNode objectnode in categorynode.Nodes)
                {
                    objectnode.Expand();
                }
            }
        }


        /// <summary>
        /// Collapse all top-level nodes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CollapseTreeNodesMenu_Click(object sender, EventArgs e)
        {
            foreach (TreeNode categorynode in ObjectTree.Nodes)
            {
                foreach (TreeNode objectnode in categorynode.Nodes)
                {
                    objectnode.Collapse();
                }
            }
        }


        /// <summary>
        /// show selection object info
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectInfoButton_Click(object sender, EventArgs e)
        {
           ProcessKey(false, true, false, Keys.F1);
        }


        /// <summary>
        /// toggle the object tree
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObjectTreeButton_Click(object sender, EventArgs e)
        {
           ProcessKey(false, false, false, Keys.F8);
        }


        /// <summary>
        /// Close the current editor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CloseEditorButton_Click(object sender, EventArgs e)
        {
            CloseQuery();
        }

        /// <summary>
        /// Handles receiving messages from the results grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResultsGridControl_GridMessage(object sender, Controls.ResultsGrid.ResultGridMessage e)
        {
            this.SetStatus(e.Message);
        }


        /// <summary>
        /// Handles a request from the results grid to show the results panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResultsGridControl_ShowResultsPanel(object sender, EventArgs e)
        {
            splitContainerEditor.Panel2Collapsed = false;
        }


        /// <summary>
        /// Handles a request from the results grid to hide the results panel.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResultsGridControl_HideResultsPanel(object sender, EventArgs e)
        {
            splitContainerEditor.Panel2Collapsed = true;
        }


        /// <summary>
        /// Handles a request from the results grid to focus the code editor.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResultsGridControl_FocusCodeEditor(object sender, EventArgs e)
        {
            scintilla1.Focus();
        }


        /// <summary>
        /// Trigger auto completion list building
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AutocompleteTimer_Tick(object sender, EventArgs e)
        {
            autocompleteTimer.Stop();
            BuildAutoCompleteWords();
        }


    }
}
