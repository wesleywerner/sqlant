﻿namespace SQLAnt.Controls
{
    partial class ResultsGrid
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripSeparator ResultsGridContextSep1;
            this.ResultsTabs = new System.Windows.Forms.TabControl();
            this.ResultsGridContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CopyDataWithHeadersAction = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyDataAsInList = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyDataAsCSVAction = new System.Windows.Forms.ToolStripMenuItem();
            this.CopyHeadersAction = new System.Windows.Forms.ToolStripMenuItem();
            ResultsGridContextSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.ResultsGridContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // ResultsGridContextSep1
            // 
            ResultsGridContextSep1.Name = "ResultsGridContextSep1";
            ResultsGridContextSep1.Size = new System.Drawing.Size(148, 6);
            // 
            // ResultsTabs
            // 
            this.ResultsTabs.Location = new System.Drawing.Point(17, 18);
            this.ResultsTabs.Name = "ResultsTabs";
            this.ResultsTabs.SelectedIndex = 0;
            this.ResultsTabs.Size = new System.Drawing.Size(200, 100);
            this.ResultsTabs.TabIndex = 0;
            // 
            // ResultsGridContext
            // 
            this.ResultsGridContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyDataWithHeadersAction,
            this.CopyDataAsInList,
            this.CopyDataAsCSVAction,
            ResultsGridContextSep1,
            this.CopyHeadersAction});
            this.ResultsGridContext.Name = "ResultsGridContext";
            this.ResultsGridContext.Size = new System.Drawing.Size(152, 98);
            // 
            // CopyDataWithHeadersAction
            // 
            this.CopyDataWithHeadersAction.Name = "CopyDataWithHeadersAction";
            this.CopyDataWithHeadersAction.Size = new System.Drawing.Size(151, 22);
            this.CopyDataWithHeadersAction.Text = "Copy";
            this.CopyDataWithHeadersAction.Click += new System.EventHandler(this.CopyDataWithHeadersAction_Click);
            // 
            // CopyDataAsInList
            // 
            this.CopyDataAsInList.Name = "CopyDataAsInList";
            this.CopyDataAsInList.Size = new System.Drawing.Size(151, 22);
            this.CopyDataAsInList.Text = "Copy as IN-list";
            this.CopyDataAsInList.Click += new System.EventHandler(this.CopyDataAsCSVAction_Click);
            // 
            // CopyDataAsCSVAction
            // 
            this.CopyDataAsCSVAction.Name = "CopyDataAsCSVAction";
            this.CopyDataAsCSVAction.Size = new System.Drawing.Size(151, 22);
            this.CopyDataAsCSVAction.Text = "Copy as CSV";
            this.CopyDataAsCSVAction.Click += new System.EventHandler(this.CopyDataAsCSVAction_Click);
            // 
            // CopyHeadersAction
            // 
            this.CopyHeadersAction.Name = "CopyHeadersAction";
            this.CopyHeadersAction.Size = new System.Drawing.Size(151, 22);
            this.CopyHeadersAction.Text = "Copy headers";
            this.CopyHeadersAction.Click += new System.EventHandler(this.CopyHeadersAction_Click);
            // 
            // ResultsGrid
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ResultsTabs);
            this.Name = "ResultsGrid";
            this.Size = new System.Drawing.Size(260, 174);
            this.ResultsGridContext.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl ResultsTabs;
        private System.Windows.Forms.ContextMenuStrip ResultsGridContext;
        private System.Windows.Forms.ToolStripMenuItem CopyDataWithHeadersAction;
        private System.Windows.Forms.ToolStripMenuItem CopyDataAsInList;
        private System.Windows.Forms.ToolStripMenuItem CopyDataAsCSVAction;
        private System.Windows.Forms.ToolStripMenuItem CopyHeadersAction;
    }
}
