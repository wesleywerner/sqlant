﻿namespace SQLAnt
{
    partial class CodeEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CodeEditor));
            this.splitContainerEditor = new System.Windows.Forms.SplitContainer();
            this.scintilla1 = new ScintillaNET.Scintilla();
            this.executeThread = new System.ComponentModel.BackgroundWorker();
            this.ClearStatusTimer = new System.Windows.Forms.Timer(this.components);
            this.splitContainerTree = new System.Windows.Forms.SplitContainer();
            this.SidebarTabs = new System.Windows.Forms.TabControl();
            this.ObjectTreeTab = new System.Windows.Forms.TabPage();
            this.ObjectTree = new System.Windows.Forms.TreeView();
            this.ObjectTreeContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TreeObjectInformationMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ScriptObjectMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ExpandTreeNodesMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.CollapseTreeNodesMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.ObjectTreeIcons = new System.Windows.Forms.ImageList(this.components);
            this.ObjectFilterPanel = new System.Windows.Forms.Panel();
            this.ObjectTreeFilterInput = new System.Windows.Forms.TextBox();
            this.ObjectFilterLabel = new System.Windows.Forms.Label();
            this.EditorToolstrip = new System.Windows.Forms.ToolStrip();
            this.ObjectTreeButton = new System.Windows.Forms.ToolStripButton();
            this.ObjectInfoButton = new System.Windows.Forms.ToolStripButton();
            this.CloseEditorButton = new System.Windows.Forms.ToolStripButton();
            this.statusStrip2 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ExecutingIndicator = new System.Windows.Forms.ToolStripProgressBar();
            this.DatabaseLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.EditorLineIndicator = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.EditorColIndicator = new System.Windows.Forms.ToolStripStatusLabel();
            this.ObjectTreeFilterTimer = new System.Windows.Forms.Timer(this.components);
            toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerEditor)).BeginInit();
            this.splitContainerEditor.Panel1.SuspendLayout();
            this.splitContainerEditor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerTree)).BeginInit();
            this.splitContainerTree.Panel1.SuspendLayout();
            this.splitContainerTree.Panel2.SuspendLayout();
            this.splitContainerTree.SuspendLayout();
            this.SidebarTabs.SuspendLayout();
            this.ObjectTreeTab.SuspendLayout();
            this.ObjectTreeContext.SuspendLayout();
            this.ObjectFilterPanel.SuspendLayout();
            this.EditorToolstrip.SuspendLayout();
            this.statusStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripSeparator1
            // 
            toolStripSeparator1.Name = "toolStripSeparator1";
            toolStripSeparator1.Size = new System.Drawing.Size(139, 6);
            // 
            // splitContainerEditor
            // 
            this.splitContainerEditor.Location = new System.Drawing.Point(52, 81);
            this.splitContainerEditor.Name = "splitContainerEditor";
            this.splitContainerEditor.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainerEditor.Panel1
            // 
            this.splitContainerEditor.Panel1.Controls.Add(this.scintilla1);
            this.splitContainerEditor.Size = new System.Drawing.Size(181, 282);
            this.splitContainerEditor.SplitterDistance = 140;
            this.splitContainerEditor.TabIndex = 0;
            // 
            // scintilla1
            // 
            this.scintilla1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scintilla1.Location = new System.Drawing.Point(0, 0);
            this.scintilla1.Name = "scintilla1";
            this.scintilla1.Size = new System.Drawing.Size(181, 140);
            this.scintilla1.TabIndex = 0;
            this.scintilla1.CharAdded += new System.EventHandler<ScintillaNET.CharAddedEventArgs>(this.scintilla1_CharAdded);
            this.scintilla1.UpdateUI += new System.EventHandler<ScintillaNET.UpdateUIEventArgs>(this.scintilla1_UpdateUI);
            this.scintilla1.TextChanged += new System.EventHandler(this.scintilla1_TextChanged);
            this.scintilla1.DragDrop += new System.Windows.Forms.DragEventHandler(this.scintilla1_DragDrop);
            this.scintilla1.DragEnter += new System.Windows.Forms.DragEventHandler(this.scintilla1_DragEnter);
            this.scintilla1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.scintilla1_KeyDown);
            // 
            // executeThread
            // 
            this.executeThread.WorkerSupportsCancellation = true;
            this.executeThread.DoWork += new System.ComponentModel.DoWorkEventHandler(this.executeThread_DoWork);
            this.executeThread.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.executeThread_RunWorkerCompleted);
            // 
            // ClearStatusTimer
            // 
            this.ClearStatusTimer.Interval = 2000;
            this.ClearStatusTimer.Tick += new System.EventHandler(this.ClearStatusTimer_Tick);
            // 
            // splitContainerTree
            // 
            this.splitContainerTree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerTree.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainerTree.Location = new System.Drawing.Point(0, 0);
            this.splitContainerTree.Name = "splitContainerTree";
            // 
            // splitContainerTree.Panel1
            // 
            this.splitContainerTree.Panel1.Controls.Add(this.SidebarTabs);
            // 
            // splitContainerTree.Panel2
            // 
            this.splitContainerTree.Panel2.Controls.Add(this.splitContainerEditor);
            this.splitContainerTree.Panel2.Controls.Add(this.EditorToolstrip);
            this.splitContainerTree.Panel2MinSize = 220;
            this.splitContainerTree.Size = new System.Drawing.Size(621, 407);
            this.splitContainerTree.SplitterDistance = 300;
            this.splitContainerTree.TabIndex = 0;
            this.splitContainerTree.TabStop = false;
            // 
            // SidebarTabs
            // 
            this.SidebarTabs.Controls.Add(this.ObjectTreeTab);
            this.SidebarTabs.Location = new System.Drawing.Point(14, 27);
            this.SidebarTabs.Name = "SidebarTabs";
            this.SidebarTabs.SelectedIndex = 0;
            this.SidebarTabs.Size = new System.Drawing.Size(200, 288);
            this.SidebarTabs.TabIndex = 0;
            // 
            // ObjectTreeTab
            // 
            this.ObjectTreeTab.Controls.Add(this.ObjectTree);
            this.ObjectTreeTab.Controls.Add(this.ObjectFilterPanel);
            this.ObjectTreeTab.Location = new System.Drawing.Point(4, 22);
            this.ObjectTreeTab.Name = "ObjectTreeTab";
            this.ObjectTreeTab.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.ObjectTreeTab.Size = new System.Drawing.Size(192, 262);
            this.ObjectTreeTab.TabIndex = 0;
            this.ObjectTreeTab.Text = "Objects";
            this.ObjectTreeTab.UseVisualStyleBackColor = true;
            // 
            // ObjectTree
            // 
            this.ObjectTree.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ObjectTree.ContextMenuStrip = this.ObjectTreeContext;
            this.ObjectTree.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ObjectTree.ImageIndex = 0;
            this.ObjectTree.ImageList = this.ObjectTreeIcons;
            this.ObjectTree.Location = new System.Drawing.Point(0, 119);
            this.ObjectTree.Name = "ObjectTree";
            this.ObjectTree.SelectedImageIndex = 0;
            this.ObjectTree.Size = new System.Drawing.Size(192, 143);
            this.ObjectTree.TabIndex = 1;
            // 
            // ObjectTreeContext
            // 
            this.ObjectTreeContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TreeObjectInformationMenu,
            this.ScriptObjectMenu,
            toolStripSeparator1,
            this.ExpandTreeNodesMenu,
            this.CollapseTreeNodesMenu});
            this.ObjectTreeContext.Name = "ObjectTreeContext";
            this.ObjectTreeContext.Size = new System.Drawing.Size(143, 98);
            // 
            // TreeObjectInformationMenu
            // 
            this.TreeObjectInformationMenu.Name = "TreeObjectInformationMenu";
            this.TreeObjectInformationMenu.Size = new System.Drawing.Size(142, 22);
            this.TreeObjectInformationMenu.Text = "&Information";
            this.TreeObjectInformationMenu.Click += new System.EventHandler(this.TreeObjectInformationMenu_Click);
            // 
            // ScriptObjectMenu
            // 
            this.ScriptObjectMenu.Name = "ScriptObjectMenu";
            this.ScriptObjectMenu.Size = new System.Drawing.Size(142, 22);
            this.ScriptObjectMenu.Text = "Script Object";
            this.ScriptObjectMenu.Click += new System.EventHandler(this.ScriptObjectMenu_Click);
            // 
            // ExpandTreeNodesMenu
            // 
            this.ExpandTreeNodesMenu.Name = "ExpandTreeNodesMenu";
            this.ExpandTreeNodesMenu.Size = new System.Drawing.Size(142, 22);
            this.ExpandTreeNodesMenu.Text = "Expand";
            this.ExpandTreeNodesMenu.Click += new System.EventHandler(this.ExpandTreeNodesMenu_Click);
            // 
            // CollapseTreeNodesMenu
            // 
            this.CollapseTreeNodesMenu.Name = "CollapseTreeNodesMenu";
            this.CollapseTreeNodesMenu.Size = new System.Drawing.Size(142, 22);
            this.CollapseTreeNodesMenu.Text = "Collapse";
            this.CollapseTreeNodesMenu.Click += new System.EventHandler(this.CollapseTreeNodesMenu_Click);
            // 
            // ObjectTreeIcons
            // 
            this.ObjectTreeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ObjectTreeIcons.ImageStream")));
            this.ObjectTreeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ObjectTreeIcons.Images.SetKeyName(0, "treedefault.png");
            this.ObjectTreeIcons.Images.SetKeyName(1, "table.png");
            this.ObjectTreeIcons.Images.SetKeyName(2, "view.png");
            this.ObjectTreeIcons.Images.SetKeyName(3, "procedure.png");
            // 
            // ObjectFilterPanel
            // 
            this.ObjectFilterPanel.Controls.Add(this.ObjectTreeFilterInput);
            this.ObjectFilterPanel.Controls.Add(this.ObjectFilterLabel);
            this.ObjectFilterPanel.Location = new System.Drawing.Point(0, 10);
            this.ObjectFilterPanel.Name = "ObjectFilterPanel";
            this.ObjectFilterPanel.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.ObjectFilterPanel.Size = new System.Drawing.Size(192, 30);
            this.ObjectFilterPanel.TabIndex = 0;
            // 
            // ObjectTreeFilterInput
            // 
            this.ObjectTreeFilterInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ObjectTreeFilterInput.ForeColor = System.Drawing.Color.Blue;
            this.ObjectTreeFilterInput.Location = new System.Drawing.Point(86, 0);
            this.ObjectTreeFilterInput.Name = "ObjectTreeFilterInput";
            this.ObjectTreeFilterInput.Size = new System.Drawing.Size(106, 22);
            this.ObjectTreeFilterInput.TabIndex = 1;
            this.ObjectTreeFilterInput.TextChanged += new System.EventHandler(this.ObjectTreeFilterInput_TextChanged);
            // 
            // ObjectFilterLabel
            // 
            this.ObjectFilterLabel.AutoSize = true;
            this.ObjectFilterLabel.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.ObjectFilterLabel.Location = new System.Drawing.Point(0, 0);
            this.ObjectFilterLabel.Name = "ObjectFilterLabel";
            this.ObjectFilterLabel.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.ObjectFilterLabel.Size = new System.Drawing.Size(86, 16);
            this.ObjectFilterLabel.TabIndex = 0;
            this.ObjectFilterLabel.Text = "Filter (Ctrl-Shift-F)";
            // 
            // EditorToolstrip
            // 
            this.EditorToolstrip.AutoSize = false;
            this.EditorToolstrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ObjectTreeButton,
            this.ObjectInfoButton,
            this.CloseEditorButton});
            this.EditorToolstrip.Location = new System.Drawing.Point(0, 0);
            this.EditorToolstrip.Name = "EditorToolstrip";
            this.EditorToolstrip.Size = new System.Drawing.Size(317, 25);
            this.EditorToolstrip.TabIndex = 1;
            this.EditorToolstrip.Text = "toolStrip1";
            // 
            // ObjectTreeButton
            // 
            this.ObjectTreeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ObjectTreeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ObjectTreeButton.Name = "ObjectTreeButton";
            this.ObjectTreeButton.Size = new System.Drawing.Size(23, 22);
            this.ObjectTreeButton.Text = "Object Tree";
            this.ObjectTreeButton.ToolTipText = "Object Tree (F8)";
            this.ObjectTreeButton.Click += new System.EventHandler(this.ObjectTreeButton_Click);
            // 
            // ObjectInfoButton
            // 
            this.ObjectInfoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ObjectInfoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ObjectInfoButton.Name = "ObjectInfoButton";
            this.ObjectInfoButton.Size = new System.Drawing.Size(23, 22);
            this.ObjectInfoButton.Text = "Object Information";
            this.ObjectInfoButton.ToolTipText = "Selected Object Information (Alt-F1)";
            this.ObjectInfoButton.Click += new System.EventHandler(this.ObjectInfoButton_Click);
            // 
            // CloseEditorButton
            // 
            this.CloseEditorButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.CloseEditorButton.BackColor = System.Drawing.Color.Red;
            this.CloseEditorButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.CloseEditorButton.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseEditorButton.ForeColor = System.Drawing.Color.White;
            this.CloseEditorButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.CloseEditorButton.Name = "CloseEditorButton";
            this.CloseEditorButton.Size = new System.Drawing.Size(23, 22);
            this.CloseEditorButton.Text = "X";
            this.CloseEditorButton.Click += new System.EventHandler(this.CloseEditorButton_Click);
            // 
            // statusStrip2
            // 
            this.statusStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel,
            this.ExecutingIndicator,
            this.DatabaseLabel,
            this.toolStripStatusLabel1,
            this.EditorLineIndicator,
            this.toolStripStatusLabel3,
            this.EditorColIndicator});
            this.statusStrip2.Location = new System.Drawing.Point(0, 407);
            this.statusStrip2.Name = "statusStrip2";
            this.statusStrip2.Size = new System.Drawing.Size(621, 22);
            this.statusStrip2.TabIndex = 4;
            this.statusStrip2.Text = "statusStrip2";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(448, 17);
            this.StatusLabel.Spring = true;
            this.StatusLabel.Text = "[status]";
            this.StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ExecutingIndicator
            // 
            this.ExecutingIndicator.Name = "ExecutingIndicator";
            this.ExecutingIndicator.Size = new System.Drawing.Size(100, 16);
            this.ExecutingIndicator.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.ExecutingIndicator.Visible = false;
            // 
            // DatabaseLabel
            // 
            this.DatabaseLabel.Image = global::SQLAnt.Properties.Resources.network_workgroup;
            this.DatabaseLabel.Name = "DatabaseLabel";
            this.DatabaseLabel.Size = new System.Drawing.Size(78, 17);
            this.DatabaseLabel.Text = "[database]";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(29, 17);
            this.toolStripStatusLabel1.Text = "Line";
            // 
            // EditorLineIndicator
            // 
            this.EditorLineIndicator.Name = "EditorLineIndicator";
            this.EditorLineIndicator.Size = new System.Drawing.Size(13, 17);
            this.EditorLineIndicator.Text = "0";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(25, 17);
            this.toolStripStatusLabel3.Text = "Col";
            // 
            // EditorColIndicator
            // 
            this.EditorColIndicator.Name = "EditorColIndicator";
            this.EditorColIndicator.Size = new System.Drawing.Size(13, 17);
            this.EditorColIndicator.Text = "0";
            // 
            // ObjectTreeFilterTimer
            // 
            this.ObjectTreeFilterTimer.Interval = 450;
            this.ObjectTreeFilterTimer.Tick += new System.EventHandler(this.ObjectTreeFilterTimer_Tick);
            // 
            // CodeEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerTree);
            this.Controls.Add(this.statusStrip2);
            this.Name = "CodeEditor";
            this.Size = new System.Drawing.Size(621, 429);
            this.TextChanged += new System.EventHandler(this.CodeEditor_TextChanged);
            this.splitContainerEditor.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerEditor)).EndInit();
            this.splitContainerEditor.ResumeLayout(false);
            this.splitContainerTree.Panel1.ResumeLayout(false);
            this.splitContainerTree.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerTree)).EndInit();
            this.splitContainerTree.ResumeLayout(false);
            this.SidebarTabs.ResumeLayout(false);
            this.ObjectTreeTab.ResumeLayout(false);
            this.ObjectTreeContext.ResumeLayout(false);
            this.ObjectFilterPanel.ResumeLayout(false);
            this.ObjectFilterPanel.PerformLayout();
            this.EditorToolstrip.ResumeLayout(false);
            this.EditorToolstrip.PerformLayout();
            this.statusStrip2.ResumeLayout(false);
            this.statusStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainerEditor;
        internal ScintillaNET.Scintilla scintilla1;
        private System.Windows.Forms.Timer ClearStatusTimer;
        private System.Windows.Forms.SplitContainer splitContainerTree;
        private System.Windows.Forms.ImageList ObjectTreeIcons;
        private System.Windows.Forms.TabControl SidebarTabs;
        private System.Windows.Forms.TabPage ObjectTreeTab;
        private System.Windows.Forms.TreeView ObjectTree;
        private System.Windows.Forms.Label ObjectFilterLabel;
        private System.Windows.Forms.TextBox ObjectTreeFilterInput;
        private System.Windows.Forms.Panel ObjectFilterPanel;
        private System.Windows.Forms.ContextMenuStrip ObjectTreeContext;
        private System.Windows.Forms.ToolStripMenuItem ScriptObjectMenu;
        private System.Windows.Forms.StatusStrip statusStrip2;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel DatabaseLabel;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel EditorLineIndicator;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel EditorColIndicator;
        private System.Windows.Forms.ToolStripProgressBar ExecutingIndicator;
        private System.Windows.Forms.Timer ObjectTreeFilterTimer;
        private System.Windows.Forms.ToolStripMenuItem TreeObjectInformationMenu;
        internal System.ComponentModel.BackgroundWorker executeThread;
        private System.Windows.Forms.ToolStripMenuItem ExpandTreeNodesMenu;
        private System.Windows.Forms.ToolStripMenuItem CollapseTreeNodesMenu;
        private System.Windows.Forms.ToolStrip EditorToolstrip;
        private System.Windows.Forms.ToolStripButton ObjectTreeButton;
        private System.Windows.Forms.ToolStripButton ObjectInfoButton;
        private System.Windows.Forms.ToolStripButton CloseEditorButton;
    }
}
