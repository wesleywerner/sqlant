﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQLAnt.Controls
{
    public partial class ResultsGrid : UserControl
    {


        /// <summary>
        /// Defines the tab names for SP_HELP queries
        /// </summary>
        private static string[] SP_HELP_TABS = { "", "Object", "Columns", "Identities", "RowGuids", "File Groups", "Indexes", "Constraints", "References", "", "", "", "", "", "", "" };


        /// <summary>
        /// Event to focus the code editor in the parent.
        /// </summary>
        public event EventHandler FocusCodeEditor;


        /// <summary>
        /// Event to show the result panel in the parent.
        /// </summary>
        public event EventHandler ShowResultsPanel;


        /// <summary>
        /// Event to hide the result panel in the parent.
        /// </summary>
        public event EventHandler HideResultsPanel;


        /// <summary>
        /// Event to send messages to the parent.
        /// </summary>
        public event EventHandler<ResultGridMessage> GridMessage;


        /// <summary>
        /// Result grid message argument.
        /// </summary>
        public class ResultGridMessage : EventArgs
        {
            public string Message { get; set; }
            public ResultGridMessage(string message)
            {
                Message = message;
            }
        }


        /// <summary>
        /// Main entry
        /// </summary>
        public ResultsGrid()
        {
            InitializeComponent();
            ResultsTabs.Dock = DockStyle.Fill;
        }


        /// <summary>
        /// Display a dataset as grids in mutliple tabs.
        /// </summary>
        /// <param name="data">The dataset to render</param>
        /// <param name="queryType">The query type that determines the appearance of the grid</param>
        public void DisplayResults(DataSet data, ResultType queryType)
        {
            try
            {
                Clear();
                if (data != null)
                {
                    int tableIndex = 0;
                    foreach (DataTable dataTable in data.Tables)
                    {
                        tableIndex++;
                        // tab page
                        var tab = new TabPage();
                        tab.Text = string.Format("{0} ({1:#,0} rows)", dataTable.TableName, dataTable.Rows.Count);
                        ResultsTabs.TabPages.Add(tab);
                        // data grid
                        var grid = new DataGridView();
                        grid.Parent = tab;
                        tab.Controls.Add(grid);
                        grid.Dock = DockStyle.Fill;
                        grid.AllowUserToAddRows = false;
                        grid.AllowUserToDeleteRows = false;
                        grid.AllowUserToOrderColumns = false;
                        grid.AllowUserToResizeColumns = true;
                        grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                        grid.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                        grid.AllowUserToResizeRows = false;
                        grid.StandardTab = true;
                        grid.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
                        grid.EditMode = DataGridViewEditMode.EditProgrammatically;
                        grid.BringToFront();

                        // set up virtual mode for the data grid view:
                        // store the data table in the grid.
                        foreach (DataColumn c in dataTable.Columns)
                        {
                            grid.Columns.Add(c.ColumnName, c.ColumnName);
                        }
                        grid.Tag = dataTable;
                        grid.VirtualMode = true;
                        grid.CellValueNeeded += Grid_CellValueNeeded;
                        grid.RowCount = dataTable.Rows.Count;
                        grid.Enter += new System.EventHandler(grid_Enter);
                        grid.Leave += new System.EventHandler(grid_Leave);
                        grid.ContextMenuStrip = ResultsGridContext;
                        grid.BackgroundColor = SystemColors.Control;
                        grid.DataError += grid_DataError;
                        grid.CellMouseDown += Grid_CellMouseDown;

                        Program.DoubleBuffered(grid, true);

                        // Disable sorting on grid column click
                        foreach (DataGridViewColumn column in grid.Columns)
                        {
                            column.MinimumWidth = 130;
                            column.SortMode = DataGridViewColumnSortMode.NotSortable;
                        }

                        // Allow selecting on columns
                        grid.SelectionMode = DataGridViewSelectionMode.ColumnHeaderSelect;

                        // Customize the tab name
                        if (queryType == ResultType.SP_HELP)
                        {
                            tab.Text = SP_HELP_TABS[tableIndex];
                            // Auto select columns tab
                            if (tableIndex == 2)
                            {
                                ResultsTabs.SelectedTab = tab;
                            }
                        }

                    }

                    // Show a run confirmation when there are no results to display
                    if (data.Tables.Count == 0)
                    {
                        // tab page
                        var tab = new TabPage();
                        tab.Text = "Execution";
                        ResultsTabs.TabPages.Add(tab);
                        var messageBox = new TextBox();
                        messageBox.Multiline = true;
                        messageBox.Dock = DockStyle.Fill;
                        messageBox.Font = new Font("Consolas", 12);
                        messageBox.Text = "Query Executed";
                        messageBox.SelectionStart = messageBox.Text.Length;
                        tab.Controls.Add(messageBox);
                    }

                }


            }
            catch (Exception ex)
            {
                DisplayQueryError(ex);
            }
            finally
            {
                // Auto focus
                if (ResultsTabs.TabCount > 0)
                {
                    this.FocusCodeEditor(this, new EventArgs());
                    this.ShowResultsPanel(this, new EventArgs());
                }
            }
        }


        /// <summary>
        /// Switch to another result tab in the given direction
        /// </summary>
        /// <param name="direction"></param>
        public void SwitchTab(int direction)
        {
            int idx = ResultsTabs.TabIndex + direction;
            if (idx == -1)
                idx = ResultsTabs.TabCount - 1;
            else if (idx >= ResultsTabs.TabCount)
                idx = 0;
            ResultsTabs.TabIndex = idx;
            // Auto focus
            ResultsTabs.TabPages[idx].Controls[0].Focus();
        }


        /// <summary>
        /// Place focus on the results tab
        /// </summary>
        public void FocusResultsTab()
        {
            if (ResultsTabs.TabCount == 0) return;
            var tab = ResultsTabs.TabPages[ResultsTabs.TabIndex];
            if (tab.Controls.Count > 0)
            {
                tab.Controls[0].Focus();
            }
        }


        /// <summary>
        /// Clear the result grids and dispose of them properly to release memory.
        /// </summary>
        public void Clear()
        {
            this.HideResultsPanel(this, new EventArgs());
            foreach (TabPage tab in ResultsTabs.TabPages)
            {
                // Remove the tab page
                ResultsTabs.TabPages.Remove(tab);

                // Dispose data sets
                foreach (Control ctrl in tab.Controls)
                {
                    if (ctrl is DataGridView)
                    {
                        if (ctrl.Tag is DataTable)
                        {
                            var n = ((DataTable)ctrl.Tag).DataSet;
                            ctrl.Tag = null;
                            n.Dispose();
                        }
                    }
                }
                tab.Dispose();
            }
        }


        /// <summary>
        /// Provide result grid enter and leave focus indication
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grid_Enter(object sender, EventArgs e)
        {
            var grid = (DataGridView)sender;
            foreach (DataGridViewRow row in grid.Rows)
            {
                row.DefaultCellStyle.SelectionBackColor = SystemColors.Highlight;
            }
        }


        /// <summary>
        /// Provide result grid enter and leave focus indication
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void grid_Leave(object sender, EventArgs e)
        {
            var grid = (DataGridView)sender;
            foreach (DataGridViewRow row in grid.Rows)
            {
                row.DefaultCellStyle.SelectionBackColor = SystemColors.GrayText;
            }
        }


        /// <summary>
        /// Gives the requested result grid value from the data source.
        /// This method enables the virtual data source for the result grid.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            var grid = (DataGridView)sender;
            var table = (DataTable)grid.Tag;
            e.Value = table.Rows[e.RowIndex][e.ColumnIndex];
        }


        /// <summary>
        /// Handle result grid mouse clicks which toggles full columns/row selection.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right) return;
            var grid = (DataGridView)sender;
            if (e.ColumnIndex == -1)
                grid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            else if (e.RowIndex == -1)
                grid.SelectionMode = DataGridViewSelectionMode.FullColumnSelect;
            else
                grid.SelectionMode = DataGridViewSelectionMode.CellSelect;
        }


        /// <summary>
        /// Handle results grid errors
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void grid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            e.ThrowException = false;
        }


        /// <summary>
        /// Display an exception message in the results control.
        /// </summary>
        /// <param name="ex"></param>
        public void DisplayQueryError(Exception ex)
        {
            try
            {
                var tab = new TabPage();
                tab.Text = "Query Error";
                ResultsTabs.TabPages.Add(tab);
                var errorBox = new TextBox();
                errorBox.Multiline = true;
                errorBox.Dock = DockStyle.Fill;
                errorBox.ForeColor = Color.Red;
                errorBox.Font = new Font("Consolas", 12);
                errorBox.Text = ExceptionHelper.Clean(ex);
                errorBox.SelectionStart = errorBox.Text.Length;
                tab.Controls.Add(errorBox);
                this.ShowResultsPanel(this, new EventArgs());
            }
            catch (Exception ex2)
            {
                Utilities.LogException(ex2);
            }
        }


        /// <summary>
        /// Gets the active result grid.
        /// </summary>
        /// <returns></returns>
        public DataGridView ActiveGrid()
        {
            if (ResultsTabs.TabCount > 0 && ResultsTabs.SelectedTab != null)
            {
                foreach (Control c in ResultsTabs.SelectedTab.Controls)
                {
                    if (c is DataGridView)
                    {
                        return (DataGridView)c;
                    }
                }
            }
            return null;
        }


        /// <summary>
        /// Copy the active grid to the clipboard.
        /// </summary>
        /// <returns></returns>
        public int CopyActiveGrid()
        {
            var grid = ActiveGrid();
            if (grid == null)
            {
                return 0;
            }
            else
            {
                Clipboard.SetDataObject(grid.GetClipboardContent());
                GridMessage(this, new ResultGridMessage(string.Format("Copied {0} cells to the clipboard", grid.SelectedCells.Count)));
                return grid.SelectedCells.Count;
            }
        }


        /// <summary>
        /// Copy result grid data with column headers.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyDataWithHeadersAction_Click(object sender, EventArgs e)
        {
            var grid = ActiveGrid();
            if (grid != null)
            {
                var data = grid.GetClipboardContent();
                if (data == null) return;
                var headers = GetDataHeaders("\t", false);
                if (headers == string.Empty)
                {
                    Clipboard.SetDataObject(data);
                }
                else
                {
                    Clipboard.SetDataObject(headers + Environment.NewLine + data.GetText());
                }
                GridMessage(this, new ResultGridMessage(string.Format("Copied {0} cells to the clipboard", grid.SelectedCells.Count)));
            }
        }


        /// <summary>
        /// Copy result grid data as a CSV list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyDataAsCSVAction_Click(object sender, EventArgs e)
        {
            try
            {
                bool specialInList = ((ToolStripMenuItem)sender).Name == CopyDataAsInList.Name;

                var grid = ActiveGrid();
                if (grid != null)
                {
                    // Do not copy empty cells
                    if (grid.SelectedCells.Count == 0) return;

                    var data = new List<string>();

                    // Include header names
                    if (!specialInList)
                    {
                        var headers = GetDataHeaders(",", true);
                        if (headers != string.Empty)
                        {
                            data.Add(headers);
                        }
                    }

                    foreach (DataGridViewRow row in grid.Rows)
                    {
                        var rowlist = new List<string>();
                        foreach (DataGridViewCell cell in row.Cells)
                        {
                            if (cell.Selected)
                            {
                                var value = cell.Value.ToString();
                                if (specialInList)
                                {
                                    rowlist.Add(string.Format("'{0}'", value));
                                }
                                else
                                {
                                    // A (double) quote character in a field must be represented by two (double) quote characters.
                                    // https://tools.ietf.org/html/rfc4180
                                    var hasQuote = value.IndexOf("\"") > -1;
                                    if (hasQuote) value = value.Replace("\"", "\"\"");
                                    rowlist.Add(string.Format("\"{0}\"", value));
                                }
                            }
                        }
                        if (rowlist.Count > 0)
                        {
                            data.Add(string.Join(",", rowlist));
                        }
                    }

                    string sep = (specialInList ? "," : string.Empty) + Environment.NewLine;
                    string cliptext = string.Join(sep, data);

                    if (specialInList)
                    {
                        cliptext = "IN (" + Environment.NewLine + cliptext + Environment.NewLine + ")";
                    }

                    Clipboard.SetText(cliptext);
                    GridMessage(this, new ResultGridMessage(string.Format("Copied {0} cells to the clipboard as CSV", grid.SelectedCells.Count)));
                }
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
            }
        }


        /// <summary>
        /// Copy selected column headers to the clipboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CopyHeadersAction_Click(object sender, EventArgs e)
        {
            var headers = GetDataHeaders("\t", false);
            if (headers != string.Empty)
            {
                Clipboard.SetText(headers);
                GridMessage(this, new ResultGridMessage("Copied headers"));
            }
        }


        /// <summary>
        /// Return a list of the highlighted data column names
        /// </summary>
        /// <returns></returns>
        private string GetDataHeaders(string seperator, bool quoted)
        {
            try
            {
                var grid = ActiveGrid();
                if (grid != null)
                {
                    List<String> columns = null;

                    // allow copying headers by selected columns, or selected cells
                    if (grid.SelectedColumns.Count > 0)
                    {
                        columns = (from c in grid.SelectedColumns.Cast<DataGridViewColumn>() select c)
                            .OrderBy(n => n.Index)
                            .Select(n => n.Name)
                            .ToList();
                    }
                    else if (grid.SelectedCells.Count > 0)
                    {
                        columns = (from c in grid.SelectedCells.Cast<DataGridViewCell>() select c)
                            .OrderBy(n => n.ColumnIndex)
                            .Select(n => grid.Columns[n.ColumnIndex].Name)
                            .ToList();
                    }

                    if (columns != null)
                    {
                        string joined = string.Join(seperator, columns.Distinct());
                        if (quoted)
                        {
                            joined = "\"" + string.Join("\"" + seperator + "\"", columns.Distinct()) + "\"";
                        }
                        return joined;
                    }
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                Utilities.LogException(ex);
                return string.Empty;
            }
        }


    }
}
